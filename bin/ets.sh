#!/bin/bash
RETVAL=0
prog="ets"
prog_python="python"

if [ "$PROG_PYTHON" = '' ]; then
    export PROG_PYTHON=$(which $prog_python)
fi
prog_python=$PROG_PYTHON

prog_dir=$(cd $(dirname $0)/..; pwd) # 程序主目录
prog_py="$prog_dir/bin/$prog.py" # python主程序文件
prog_sh="$prog_dir/bin/$prog.sh"
app_id=$(basename $prog_dir)
link_dir=""
if [ "$UD_SERVERLOGPATH" = '' ]; then
    link_dir=$prog_dir
else
    link_dir="$UD_SERVERLOGPATH/$app_id"
    if [ ! -x "$prog_dir/var/log" -a -x "$link_dir/var/log" ]; then
        ln -s "$link_dir/var/log" "$prog_dir/var/log"
    fi
fi

prog_pid="$prog_dir/var/run/$prog.$1.pid" # python主程序的pid文件
prog_loop="$prog_dir/var/run/$prog.$1.loop" # loop shell的pid文件
prog_output="$link_dir/var/log/$prog.$1.output" # 终端输出重定向文件

mkdir -p $(dirname $prog_pid)
mkdir -p $(dirname $prog_output)

# Source function library.
. /etc/rc.d/init.d/functions

loop() {
    echo -n $$ > $prog_loop
    while [ -f "$prog_loop" ]; do
        cd "$prog_dir/bin"
        $prog_python $prog_py $1 start
        RETVAL=$?
        sleep 2
    done
    return $RETVAL
}

start() {
    if [ -f "$prog_loop" ]; then
        status -p $prog_loop
        [ $? -eq 0 ] && return 1
    fi
    if [ -f "$prog_pid" ]; then
        status -p $prog_pid
        [ $? -eq 0 ] && return 1
    fi
	$prog_python $prog_py $1 test
	[ $? -ne 0 ] && return 1
    echo -n "Starting $prog: "

    nohup "$prog_dir/bin/$prog.sh" $1 loop >> $prog_output 2>&1 &
    RETVAL=$?
    sleep 2
    [ -f "$prog_pid" ] && success || failure
    echo
    return $RETVAL
}

stop() {
    status -p $prog_pid
    RETVAL=$?
    echo -n "Stopping $prog: "
    if [ $RETVAL -ne 0 ]; then
        failure; rm -f $prog_loop
    else
        $prog_python $prog_py $1 stop
        RETVAL=$?
        [ $RETVAL -eq 0 ] && (success; rm -f $prog_loop) || failure
    fi
    echo
    return $RETVAL
}

normalkill() {
    status -p $prog_pid
    RETVAL=$?
    echo -n "killing $prog: "
    if [ $RETVAL -ne 0 ]; then
        failure; rm -f $prog_loop
    else
        $prog_python $prog_py $1 kill
        RETVAL=$?
        [ $RETVAL -eq 0 ] && (success; rm -f $prog_loop) || failure
        
    fi
    echo
    return $RETVAL
}

superkill() {
    status -p $prog_pid    
    RETVAL=$?
        
    pid1=`ps axu | grep "$prog_py" | grep -v grep | grep -w $1|awk '{print $2}'|head -1`
    pid2=`ps axu | grep "$prog_sh" | grep -v grep | grep -w $1|awk '{print $2}'`
    if [[ $pid1 -ne 0 ]];then
        `ps axu | grep "$prog_py" | grep -v grep | grep -w $1|awk '{print $2}'|xargs kill -9`
    else
        echo "进程不存在"
    fi
    
    if [[ $pid2 -ne 0 ]];then
        `ps axu | grep "$prog_sh" | grep -v grep | grep -w $1|awk '{print $2}'|xargs kill -9`
    else
        echo "进程不存在"
    fi
    echo -n "killing $prog: "
    RETVAL=$?
    [ $RETVAL -eq 0 ] && (success; rm -f $prog_loop) || failure
            
    echo 
    return $RETVAL
}


allstart() {
    MOD_LIST=`$prog_python $prog_py mods`
    for mod in $MOD_LIST
    do
        echo "[$mod]"
        sh $0 $mod start
    done
}

allstop() {
    MOD_LIST=`$prog_python $prog_py mods`
    for mod in $MOD_LIST
    do
        echo "[$mod]"
        sh $0 $mod stop
    done
}

allstatus() {
    MOD_LIST=`$prog_python $prog_py mods`
    for mod in $MOD_LIST
    do
        echo "[$mod]"
        sh $0 $mod status
    done
}

allrestart() {
    MOD_LIST=`$prog_python $prog_py mods`
    for mod in $MOD_LIST
    do
        echo "[$mod]"
        sh $0 $mod restart
    done
}

choicename() {
    MOD_LIST=`$prog_python $prog_py mods`
    for mod in $MOD_LIST
    do
        if [ $mod = $1 ]
        then
            cn=1
            return $cn
        else
            cn=0
        fi
    done
    return $cn
}


# shell程序入口
if [ $# -eq 1 ]
then
    case "$1" in
        allstart)
            allstart
            exit
            ;;
        allstop)
            allstop
            exit
            ;;
        allstatus)
            allstatus
            exit
            ;;
        allrestart)
            allrestart
            exit
            ;;
        *)
            echo "Usage: $0 {allstart|allstop|allrestart|allstatus}"
    esac
    exit
else
    choicename $1
    cn=$?
    if [ $cn -eq 1 ]
    then
        case "$2" in
            loop)
                loop $1
                RETVAL=$?
                ;;
            start)
                start $1
                ;;
            stop)
                stop $1
                ;;
            restart)
                stop $1
                RETVAL=$?
                [ $RETVAL -eq 0 ] && (sleep 2; start $1)
                ;;
            normalkill)
                normalkill $1
                RETVAL=$?
                #[ $RETVAL -eq 0 ] && (sleep 1; start $1)
                ;;
            superkill)
                superkill $1
                RETVAL=$?
                ;;  
            status)
                status -p $prog_loop
                status -p $prog_pid
                RETVAL=$?
                ;;
            *)
                echo "Usage: $0 <section id> {start|stop|restart|status}"
                RETVAL=1
        esac
    else
        failure
        echo "Invalid process name"
        exit $?
    fi
    exit $RETVAL
fi
