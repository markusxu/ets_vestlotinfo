#coding=utf-8
import os
import sys
import imp
import time
import urllib
import signal
import logging
import traceback
import thread
import threading
import __main__
import multiprocessing.pool

import tornado.ioloop
import tornado.web

if not os.environ.has_key('_BASIC_PATH_'):
    _BASIC_PATH_ = os.path.abspath(__file__)
    _BASIC_PATH_ = _BASIC_PATH_[:_BASIC_PATH_.rfind('/bin/')]
    os.environ['_BASIC_PATH_'] = _BASIC_PATH_
if sys.path.count(os.environ['_BASIC_PATH_'] + '/lib') == 0:
    sys.path.append(os.environ['_BASIC_PATH_'] + '/lib')
    sys.path.append(os.environ['_BASIC_PATH_'] + '/lib/common')
    sys.path.append(os.environ['_BASIC_PATH_'] + '/mod')
    sys.path.append(os.environ['_BASIC_PATH_'] + '/lib/esunlib')

import XmlConfig
if sys.path.count(os.environ['_BASIC_PATH_'] + '/etc') == 0:
    sys.path.append(os.environ['_BASIC_PATH_'] + '/etc')

XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + "/etc/service.xml")

import Common
import LogUtil
import MiniXml
import JsonUtil
import WatchDog

from Ets import Plugin
from Ets import ServerUtil
from Ets import Module

_APP_ID_ = sys.argv[1]

class EtsConf(object):
    def __init__(self, name, filepath):
        super(EtsConf, self).__init__()
        self.name = name
        self.xfile = filepath
        self.logLevel = None
        self.listen = None
        self.worker_threads = None
        self.mods = dict()
        self.plugins = None

    def load(self):
        try:
            xparser = MiniXml.MiniXml()
            xparser.loadFile(self.xfile).parseXml()
            conf = xparser.get('/ets/applications/%s' % self.name)
            if not conf:
                return None
            self.logLevel = conf.get('logLevel', 'DEBUG')
            self.listen = int(conf['listen'])
            self.worker_threads = int(conf['worker_threads'])
            self.plugins = conf.get('plugin', '')
            self.rotateType = conf.get('rotateType', 'TIME')

            #log file init
            stdout_file = "%s/var/log/%s.output" % (os.environ['_BASIC_PATH_'], self.name)
            if 'SIZE' == self.rotateType:
                LogUtil.addRotatingFileHandler(
                    '%s/var/log/ets.%s.log' % (_BASIC_PATH_, self.name),
                    logLevel = self.logLevel
                )
            else:
                LogUtil.addTimedRotatingFileHandler(
                    '%s/var/log/ets.%s.log' % (_BASIC_PATH_, self.name),
                    logLevel = self.logLevel
                )

            parserMods = xparser.get('/ets/applications/%s/mod' % self.name)
            for mod in parserMods:
                logging.debug(mod)
                if type(mod) != dict:
                    self.mods[parserMods['prefix']] = parserMods['path']
                    break
                else:
                    self.mods[mod['prefix']] = mod['path']

        except:
            logging.error(traceback.format_exc())

        logging.debug(self.mods)
        return self

class EtsHandler(tornado.web.RequestHandler):
    def initialize(self, conf, modules, workers):
        logging.debug("Get Request")
        self.config = conf
        self.modules = modules
        self.workers = workers

    def prepare(self):
        # 获得正确的客户端ip
        self.host_ip = self.request.remote_ip  # host
        ip = self.request.headers.get("X-Real-Ip", self.request.remote_ip)
        logging.debug('**********X-Real-Ip:%s', ip)
        ip = self.request.headers.get("X-Forwarded-For", ip)
        logging.debug('**********X-Forwarded-For:%s', ip)
        ip = ip.split(',')[0].strip()
        self.request.remote_ip = ip

    @tornado.web.asynchronous
    def post(self, cmdId):
        params = { }
        logging.debug(self.request.arguments)
        logging.debug(self.request.headers)
        #logging.debug(self.request.body)

        # http post方式
        # urllib.unquote(self.request.body) 不做url解码

        if self.request.body:
            # 表单提交+文件上传,直接解析self.request.body_arguments
            if self.request.headers.get('Content-Type', '').find("application/x-www-form-urlencoded") >= 0 \
                or self.request.headers.get('Content-Type', '').find("multipart/form-data") >= 0:
                params = {}

            # 非表单
            else:
                try:

                    params = JsonUtil.read(self.request.body)
                except:
                    logging.error(traceback.format_exc())
                    params = { }

        if not params:
            # 表单参数如body内容为: a=1&b=2
            # url参数
            if self.request.arguments:
                args = dict((k, v[-1]) for k, v in self.request.arguments.items())
                params.update(args)

        if not params or type(params)!=dict:
            params = {}

        logging.debug(params)

        # dispatch it to worker and wait for callback
        def _callback(result):
            tornado.ioloop.IOLoop.instance().add_callback(lambda: self.invoke_done_callback(result))
        self.workers.apply_async(self.invoke, (cmdId, params), {}, _callback)

    @tornado.web.asynchronous
    def get(self, cmdId):
        params={}

        logging.debug(self.request.arguments)
        logging.debug(self.request.headers)

        try:
            args = self.request.arguments
            for a in args:
                params[a] = args[a][len(args[a])-1]

        except:
            logging.error(traceback.format_exc())
            result = JsonUtil.write({'CODE':'-10001', 'MSG':'parament is not format', 'info':'', 'list':''})
            self.invoke_done_callback(result)
            return

        logging.debug(params)

        # dispatch it to worker and wait for callback
        def _callback(result):
            tornado.ioloop.IOLoop.instance().add_callback(lambda: self.invoke_done_callback(result))
        self.workers.apply_async(self.invoke, (cmdId, params), {}, _callback)

    @tornado.web.asynchronous
    def invoke(self, cmdId, params, res=None, current=None):
        try:
            watchman=WatchDog.autoFeeder( thread.get_ident(), cmdId )
            logging.info(cmdId)
            if not self.modules.interfaces.has_key(cmdId): #方法不存在
                err = self._error('invoke', 'Method('+cmdId+' can not found!)')
                logging.info('[ID:%s][return]:%s', thread.get_ident(), err)
                return err

            #附属数据
            remote_ip = self.request.remote_ip
            params['remote_addr'] = self.request.remote_ip
            if not res:
                res = {
                    'from'          : self.request.headers.get('from',''),
                    'remote_addr'   : self.request.headers.get('remote_addr',remote_ip),
                    'useragent'     : self.request.headers.get('User-Agent',''),
                    'source'        : self.request.headers.get('source',''),
                    'referer'       : self.request.headers.get('Referer',''),
                }

            result = {}
            Plugin.do_action('invoke_begin', cmdId,
                req=params,
                res=res,
                current=current,
                resp=result,
            )
            if int(result.get('direct_ret',0)) == 1:
                return JsonUtil.write(result.get('ret',{'CODE':'-10000', 'MSG':'Invoke baned', 'info':{}, 'list':[]}))

            # add to thread local
            self.params = params
            Common.threadSpace().set('request', self)

            startime = time.time()
            result = self.modules.invoke(cmdId, params)

            #处理输出的result
            result = self._dealResult(result)

            Plugin.do_action('invoke_end', cmdId,
                req=params,
                res=res,
                current=current,
                resp=result,
            )
            costtime = time.time() - startime

            #只输出list的长度
            outputResult = result
            try:
                outputResult = JsonUtil.read(outputResult)
                if type(outputResult) == dict and outputResult.has_key('list'):
                    outputResult['list'] = '[LEN_OF_LIST:%d]'%len(outputResult.get('list',[]))
            except:
                pass

            outputResult_byte = outputResult if isinstance(outputResult, basestring) else JsonUtil.write(outputResult)
            logging.info('''%s 耗时 %s 毫秒 [IP:%s] [ID:%s] 入参[%s] 附属数据[%s] 出参[%s]''' % (str(cmdId), str(costtime * 1000), remote_ip, str(cmdId), JsonUtil.write(params), JsonUtil.write(res), outputResult_byte))
        except:
            logging.error(traceback.format_exc())
            result = JsonUtil.write({'CODE':'-10000', 'MSG':'Invoke illegal', 'info':{}, 'list':[]})

        return result

    #@tornado.web.asynchronous
    def invoke_done_callback(self, result):
        if result:
            #if result is dict, make sure that the version of json support dumps
            self.write(result)
        self.finish()

    #处理result
    def _dealResult(self, result):
        try:
            format_type = result.get('format','ets')
            if format_type not in ['ets', 'http']:
                format_type = 'ets'

            ret = ''
            format_flag = True

            if format_type == 'http':
                if not result.has_key('resp'):
                    format_flag = False
            else:
                if type(result) != dict or  not result.has_key('CODE') or not result.has_key('MSG') or not result.has_key('list') or not result.has_key('info'):
                    format_flag = False

            if not format_flag:
                ret = JsonUtil.write({'CODE':'-10000', 'MSG':'Return data illegal', 'info':'', 'list':''})
                logging.error('[result:%s]', result)
            elif format_type=='http':
                ret = result['resp']
            else:
                ret = JsonUtil.write(result)
        except:
            ret = JsonUtil.write({'CODE':'-10000', 'MSG':'Return data except', 'info':'', 'list':''})
            logging.error(traceback.format_exc())

        return ret

    def _getParaments(self):
        i = {}
        args = self.request.arguments
        for a in args:
            i[a] = self.get_argument(a)
        return i

    def _error(self, func, msg):
        '''出错处理'''
        ret = (-1, {'MSG':msg}, [])
        logging.error('【error:%s】:return:%s', func, JsonUtil.write(ret))
        return JsonUtil.write(ret)

class Server(object):
    def __init__(self, id):
        self.id = id
        self.svc = ServerUtil.ServiceX(self.id)

    def start(self):
        if self.svc.status() > 0:
            sys.exit(1)

        self.svc.start()

        self.init()
        signal.signal(signal.SIGTERM, self.term)
        logging.info("server start...")
        self.run()

        logging.info("server stop...")
        sys.exit()

    def stop(self):
        if self.svc.stop() == 0:
            self.svc.clear()
            sys.exit(0)
        else:
            sys.exit(1)

    def init(self):
        appname = self.id[4:]
        try:
            #获取基本配置
            self.conf = EtsConf(appname, os.environ['_BASIC_PATH_'] + '/etc/ets.xml')
            if not self.conf.load():
                logging.error("config %s not found" % appname)
                sys.exit(1)

            #加载插件
            self._load_plugin()

            #初始化服务
            global application
            application = tornado.web.Application()
            handlers = list()
            logging.debug('threads num:' + str(self.conf.worker_threads))
            self.workers = multiprocessing.pool.ThreadPool(self.conf.worker_threads)

            for pattern, mpath in self.conf.mods.items():
                mods = Module.Modules()
                mods.load_module(mpath)
                handlers.append((r"/%s/(.*)" % pattern, EtsHandler, {'conf': self.conf, 'modules': mods, 'workers': self.workers}))
            application.add_handlers(".*$", handlers)

            #加入监控
            self.tidList=list()
            for tid in self.workers._pool:
                WatchDog.addTID(tid.ident)
                self.tidList.append(tid.ident)

            #服务初始化hook
            Plugin.do_action('ets_start', __main__, self)
        except:
            logging.error(traceback.format_exc())

    def _load_plugin(self):
        '''加载插件方法'''
        if not self.conf.plugins:
            logging.info("no plugins to load")
            return None

        for p in self.conf.plugins.split(';'):
            try:
                logging.info("load plugin: "+ p)
                pm = imp.load_source('Ets_Plugin_'+p, _BASIC_PATH_+'/lib/Ets/Plugin/'+p+'.py')
                if hasattr(pm, 'setup'):
                    getattr(pm, 'setup')(self.conf.name)
            except:
                logging.error('some error in _load_plugin:%s', traceback.format_exc())

    def run(self):
        try:
            WatchDogInterval = "10"
            WatchDogTimeout = "10"
            WatchDogClose  = "0"
            WatchDogWarnTopic  = "TradeLogicWarn"

            # 初始化线程监控
            if "1" != WatchDogClose:
                WatchDog.init(WatchDogInterval, WatchDogTimeout, WatchDogClose, WatchDogWarnTopic)
                t_WatchDog = threading.Thread(target=WatchDog.run)
                t_WatchDog.setDaemon(True)       # 主进程退出，子线程也退出
                t_WatchDog.start()

            application.listen(self.conf.listen)
            tornado.ioloop.IOLoop.instance().start()

            #服务结束
            for tid in self.tidList:
                WatchDog.delTID(tid)
            Plugin.do_action('ets_end', __main__, self)

            tornado.ioloop.IOLoop.instance().close()

            return 0
        except:
            logging.error(traceback.format_exc())
            return 1

    def term(self, signo, frame):
        logging.info("catch SIGTERM!!")

        #等待其他工作线程结束后在关闭
        self.workers.close()
        self.workers.join()

        tornado.ioloop.IOLoop.instance().add_callback(tornado.ioloop.IOLoop.instance().stop)

############################################################
def usage(args):
    print 'Usage: %s <app> {start|stop}' % args[0]
    sys.exit(0)

def show_mods():
    _INTERFACE_ = MiniXml.parseFile(_BASIC_PATH_ + '/etc/ets.xml')
    mods = _INTERFACE_.list( '/ets/applications/' )
    for k, v in mods.items():
        if -1 == k.find('/'):
            print k
    sys.exit(0)

def main(args):
    if len(args) not in (2,3):
        usage(args)
    if args[1] == 'mods':
        show_mods()

    appname = args[1]
    appname = 'ets.'+appname
    server  = Server(appname)
    if args[2] == 'start':
        server.start()
    elif args[2] == 'stop':
        server.stop()
    else:
        usage(args)

if __name__ == "__main__":
    application = None
    main(sys.argv)
