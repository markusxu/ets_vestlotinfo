# Ets框架部署
## 服务端部署
1. Ets框架下包含目录bin、etc、lib、libs、mod、var
2. bin目录包含基本的Ets框架代码`ets.py`和服务启动脚本 `ets.sh`
- 服务启动方式，在bin目录：
	- `sh ets.sh module start`
	- `sh ets.sh module stop`
	- `sh ets.sh module restart`
3. Ets的lib、libs使用了eas框架的lib、libs库，但是不使用eas的插件目录，插件要放入lib/Ets目录里，通常eas的插件需要做一定修改才能移植到Ets中
4. var目录下，run是框架生成的进程相关的文件，log是日志和其他输出文件，与eas相同
5. mod下是主要的代码结构（参考demo的方式）
- 在mod层部署文件，也可以新增目录，在配置的时候，有目录的需要增加目录的路径
	其中`mod/demoa/__init__.py`有效，`mod/__init__.py`无效（TODO）
6. etc的基本配置ets.xml
- 参考demo的配置如下：
	
```
<demo logLevel="DEBUG" listen="10088" worker_threads="20" plugin="">
	<mod prefix="demo" path="demo"/>
	<mod prefix="demoa" path="demoa.demoa"/>
</demo>
```


- logLevel为日志级别，分为DEBUG、INFO、ERROR
- listen
- worker_threads为线程数
- plugin
- - 插件文件py放在lib/Ets/Plugin里，用分号隔开，如"OriginalEas;Monitor"
prefix为访问url的路径前缀，path为对应文件路径，一个文件对应一行
- - - 示例中是mod/demo.py和mod/demoa/demoa的配置，访问里面的接口的方式分别如`http://192.168.0.235:10088/demo/say_hello`,`http://192.168.0.235:10088/demoa/say_hello` 注意 prefix不能重复，否则会取最后一个


## 客户端调用
1. 客户端调用的参数，推荐采用gbk编码
    ets框架本身不对编码进行转换，在服务中处理

2. get调用示例
```
http://192.168.0.235:10088/demoa/say_hello?fromname=test&toname=what
```

3. post调用示例
```
http://192.168.0.235:10088/demoa/say_hello
post参数{'fromname':'test','toname':'what'}
参数本身是string类型，里面的json串的key和value也是string类型
```

4. 接口的参数在调用中必须一一对应，可以有多余的参数，可以在接口参数表里用*args, **kwargs来处理多余的参数

5. 出参，有两种格式
- 默认是{'CODE': '', 'MSG': str(''), 'info': dict(), 'list': list()}的字典形式，参数不能少且字母大小写
- 以json串的方式返回，支持{'format': 'http', 'resp': ''}的形式，这样会将resp的值直接返回