#encoding=utf-8

import os
import sys
import logging

import XmlConfig

#加入包路径
sys.path.append(os.environ['_BASIC_PATH_'] + '/libs')

# 添加配置路径
XmlConfig.loadFile( os.environ['_BASIC_PATH_'] + '/etc/service.xml')
