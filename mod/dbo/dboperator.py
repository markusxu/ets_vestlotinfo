#coding=utf-8

import time
import logging
import traceback

import Common
import esunmysql
import baseconfig

def get_popular_lottery(gametypeid,gametypeidinset,limit=100 ):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select gametypes.name,gametypes.currentjackpot,gametypes.gametypeid,gametypes.currency gamecurrency,gametypes.slug,
          UNIX_TIMESTAMP(date_sub(gametypes.nextdraw,interval gametypes.cutoffhours HOUR))-UNIX_TIMESTAMP(UTC_TIMESTAMP()) remaintime
		     	from gametypes
		     	where onhold = 0 and isplayable = 1 and gametypes.gametypeid in %(gametypeid)s 
		     	order by find_in_set(gametypeid,%(gametypeidinset)s)
		     	limit 0,%(limit)s
          '''
    args = {'limit':limit}

    args['gametypeid'] = gametypeid
    args['gametypeidinset'] = gametypeidinset
    ret =  Common.CursorToDict(db.query(sql,args))
    return ret

def get_mustplay_lottery(gametypeid,gametypeidinset,limit=100 ):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select gametypes.name,gametypes.currentjackpot,gametypes.gametypeid,gametypes.currency gamecurrency,gametypes.slug,
          UNIX_TIMESTAMP(date_sub(gametypes.nextdraw,interval gametypes.cutoffhours HOUR))-UNIX_TIMESTAMP(UTC_TIMESTAMP()) remaintime
		     	from gametypes
		     	where onhold = 0 and isplayable = 1 and gametypes.gametypeid in %(gametypeid)s 
		     	order by find_in_set(gametypeid,%(gametypeidinset)s)
		     	limit 0,%(limit)s
          '''
    args = {'limit':limit}

    args['gametypeid'] = gametypeid
    args['gametypeidinset'] = gametypeidinset
    ret =  Common.CursorToDict(db.query(sql,args))
    return ret

def get_latest_jackpot(gametypeid):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select jackpotsize jackpot from games where gametypeid=%(gametypeid)s and drawdate< now() order by drawid desc limit 1
          '''
    args = {'gametypeid':gametypeid}
    ret =  Common.CursorToDict(db.query(sql,args))
    return ret[0]

def get_biggest_lottery( gametypeid='',jurisdictionid='2',version='2.0.0',limit=100 ):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select gametypes.*,
          numbersdrawn+1 systemnumbermin,
           UNIX_TIMESTAMP(date_sub(gametypes.nextdraw,interval gametypes.cutoffhours HOUR))-UNIX_TIMESTAMP(UTC_TIMESTAMP()) remaintime,
          currencies.currencysymbol, currencies.symbollocation

                        from gametypes
                        inner join currencies on gametypes.currency = currencies.currency
                        where onhold = 0 and gametypes.gametypeid not in [gamelist] and isplayable = 1 [gametypeid] limit 0,%(limit)s
          '''
    args = {'limit':limit}
    if version > '2.0.0':
        #sql = sql.replace('[gamelist]','(select gametype_id from gametypes_jurisdictions where jurisdiction_id=%(jurisdictionid)s and is_playable=0)')
	#args['jurisdictionid'] = jurisdictionid
        if 3 == int(jurisdictionid):
            sql = sql.replace('[gamelist]','(64,65,1)')
        elif 2 == int(jurisdictionid):
            sql = sql.replace('[gamelist]','(62)')
        elif 4 == int(jurisdictionid):
            sql = sql.replace('[gamelist]','(64,62)')
        else:
            sql = sql.replace('[gamelist]','(62)')
    else:
        if 3 == int(jurisdictionid):
            sql = sql.replace('[gamelist]','(62,17,20,63,65,1)')
        else:
            sql = sql.replace('[gamelist]','(62,17,20,63,65)')

    if not gametypeid:
        sql = sql.replace('[gametypeid]','')
    else:
        sql = sql.replace('[gametypeid]',' and gametypes.gametypeid = %(gametypeid)s')
        args['gametypeid'] = gametypeid
    ret =  Common.CursorToDict(db.query(sql,args))
    return ret
 
def get_pc_lottery( gametypeid='',jurisdictionid='5',limit=100 ):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select gametypes.name,gametypes.currentjackpot,gametypes.gametypeid,gametypes.currency gamecurrency,gametypes.slug,
          UNIX_TIMESTAMP(date_sub(gametypes.nextdraw,interval gametypes.cutoffhours HOUR))-UNIX_TIMESTAMP(UTC_TIMESTAMP()) remaintime
		     	from gametypes
		     	where onhold = 0 and isplayable = 1 and gametypes.gametypeid not in [gamelist] [gametypeid] limit 0,%(limit)s
          '''
    args = {'limit':limit}
    if 3 == int(jurisdictionid):
        sql = sql.replace('[gamelist]','(64,65,1)')
    elif 2 == int(jurisdictionid):
        sql = sql.replace('[gamelist]','(62)')
    elif 4 == int(jurisdictionid):
        sql = sql.replace('[gamelist]','(64,62)')
    else:
        sql = sql.replace('[gamelist]','(62)')
        
    if not gametypeid:
        sql = sql.replace('[gametypeid]','')
    else :
        sql = sql.replace('[gametypeid]',' and gametypes.gametypeid = %(gametypeid)s')
        args['gametypeid'] = gametypeid
    ret =  Common.CursorToDict(db.query(sql,args))
    return ret

def get_groom_lottery( gametypeid='',gametypeidstr='',jurisdictionid='5',limit=100 ):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select gametypes.name,gametypes.currentjackpot,gametypes.gametypeid,gametypes.currency currency,gametypes.slug,
          UNIX_TIMESTAMP(date_sub(gametypes.nextdraw,interval gametypes.cutoffhours HOUR))-UNIX_TIMESTAMP(UTC_TIMESTAMP()) remaintime,
           gametypes.nextdraw
		     	from gametypes
		     	where onhold = 0 and isplayable = 1 and gametypes.gametypeid not in [gamelist] and gametypes.gametypeid in %(gametypeid)s order by find_in_set(gametypeid,%(gametypeidstr)s) limit 0,%(limit)s
          '''
    args = {'limit':limit,'gametypeid':gametypeid,'gametypeidstr':gametypeidstr}

    if 3 == int(jurisdictionid):
        sql = sql.replace('[gamelist]','(64,65,1)')
    elif 2 == int(jurisdictionid):
        sql = sql.replace('[gamelist]','(62)')
    elif 4 == int(jurisdictionid):
        sql = sql.replace('[gamelist]','(64,62)')
    else:
        sql = sql.replace('[gamelist]','(62)')
 
    ret =  Common.CursorToDict(db.query(sql,args))
    return ret

def get_fixed_lottery( gametypeid='',limit=100 ):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         select gametypes.name,gametypes.currentjackpot,gametypes.gametypeid,gametypes.currency gamecurrency,gametypes.slug,
          UNIX_TIMESTAMP(date_sub(gametypes.nextdraw,interval gametypes.cutoffhours HOUR))-UNIX_TIMESTAMP(UTC_TIMESTAMP()) remaintime
		     	from gametypes
		     	where onhold = 0 and isplayable = 1 and gametypes.gametypeid in %(gametypeid)s order by find_in_set(gametypeid,'9,64,1,65') limit 0,%(limit)s
          '''
    args = {'limit':limit,'gametypeid':gametypeid}

    ret =  Common.CursorToDict(db.query(sql,args))
    return ret

def get_gametypes_id():
	
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
           select gametypeid,slug from gametypes order by gametypeid
          '''
    ret =  Common.CursorToDict(db.query(sql))
    return ret
    
def credit_limit_remian(userid):
    ''' if reached cc limit?'''
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
            select 
            creditlimitday -(
                select coalesce(sum(amount), 0)
                from creditcardtransactions as cc
                where cc.userid = u.userid and date(cc.createdate) >= curdate() and cc.status = 200
            ) as remain_today_amount,
            creditlimitweek-(
                select coalesce(sum(amount), 0)
                from creditcardtransactions as cc
                where cc.userid = u.userid and date(cc.createdate) >= date_sub(curdate(), interval 1 week) and cc.status = 200
            ) as remain_week_amount,
            creditlimitmonth-(
                select coalesce(sum(amount), 0)
                from creditcardtransactions as cc
                where cc.userid = u.userid and date(cc.createdate) >= date_sub(curdate(), interval 1 month) and cc.status = 200
            ) as remain_month_amount
            from users as u
            where u.userid = %(userid)s
            group by u.userid, creditlimitday, creditlimitweek, creditlimitmonth
        '''
        args = {
            'userid'    :   userid
        }
        rows = Common.CursorToDict(db.query(sql, args))
        return rows

    except:
        logging.error(traceback.format_exc())
    return None

 
def get_user_totalinfo(userid):
    '''get user's total info'''

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    
    sql = ''' 
        SELECT *
        FROM users
        WHERE userid = %(userid)s
        LIMIT 1
        '''

    args = {'userid':userid}
    ret  = Common.CursorToDict( db.query(sql, args) )
    logging.info(ret)

    if len(ret) > 0:
        return ret[0]


    return {}

def get_drawing_schedule(gametypeid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)

    sql = '''
        select count(*) drawcount from drawingschedules where gametypeid = %(gametypeid)s
        '''

    args = {'gametypeid':gametypeid}
    ret  = Common.CursorToDict( db.query(sql, args) )
    logging.info(ret)

    if len(ret) > 0:
        return ret[0]['drawcount']
    return 0

def get_games( siteid,jurisdictionid,casinologo,casinourl,real,gameslug,selecttype=0 ):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select cs.*, CONCAT(%(casinologo)s,cs.logo) gamelogo,
         CONCAT(%(casinourl)s,cs.slug,(CASE WHEN %(real)s=1 THEN '/real' ELSE '/' END)) url,cp.name casinoprovidername  from casinogames cs 
               LEFT JOIN  casinogames_sortorder cso ON cso.casinogameid = cs.casinogameid AND cso.siteid = %(siteid)s
	       JOIN casinogames_jurisdictions cj on  cj.casinogameid = cs.casinogameid
	       JOIN casinoproviders cp on cp.casinoproviderid = cs.casinoproviderid
							 WHERE cs.active = 1 and cs.ismobile=1 and cj.jurisdictionid =%(jurisdictionid)s [slug] [casinogameid]
							 ORDER BY -cso.sortorder DESC, cs.casinogameid DESC
          '''

    if int(selecttype) == 0:
        sql = sql.replace('[slug]', 'and cs.slug in %(gameslug)s')
        sql = sql.replace('[casinogameid]', ' ')
    else:
        sql = sql.replace('[casinogameid]', 'and cs.casinogameid in %(gameslug)s')
        sql = sql.replace('[slug]', ' ')

    ret = Common.CursorToDict(db.query(sql,{'siteid':siteid,'casinologo':casinologo,'casinourl':casinourl,'real':real,'gameslug':gameslug,'jurisdictionid':jurisdictionid}))

    return ret

def get_pcgames( siteid,jurisdictionid,ismobile,casinologo,casinourl,real,gameslug,selecttype=0,feature='',typeid='' ):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select cs.*,(CASE WHEN %(feature)s = '' then CONCAT(%(casinologo)s,cs.logo) else CONCAT(%(casinologo)s,%(feature)s) END) gamelogo,
          CONCAT(%(casinourl)s,cs.slug) url from casinogames cs 
               LEFT JOIN  casinogames_sortorder cso ON cso.casinogameid = cs.casinogameid AND cso.siteid = %(siteid)s
	       JOIN casinogames_jurisdictions cj on  cj.casinogameid = cs.casinogameid
							 WHERE cs.active = 1 and cs.ismobile =%(ismobile)s  and cj.jurisdictionid =%(jurisdictionid)s  [typeid] [slug] [casinogameid] 
							 ORDER BY -cso.sortorder DESC, cs.casinogameid DESC
          '''
    args = {'siteid':siteid,'casinologo':casinologo,'casinourl':casinourl,'jurisdictionid':jurisdictionid,'ismobile':ismobile}
    #if feature:
    args['feature'] = feature
    if gameslug:
        if type(gameslug) == tuple:

            args['gameslug'] = gameslug
            if int(selecttype) == 0:
                sql = sql.replace('[slug]', 'and cs.slug in %(gameslug)s')
                sql = sql.replace('[casinogameid]', ' ')
            else:
                sql = sql.replace('[casinogameid]', 'and cs.casinogameid in %(gameslug)s')
                sql = sql.replace('[slug]', ' ')
        else:
            if '(' in gameslug:
	        args['gameslug'] = eval(gameslug)
	    else:
	        args['gameslug'] = gameslug
	    if int(selecttype) == 0:
                sql = sql.replace('[slug]', 'and cs.slug = %(gameslug)s')
                sql = sql.replace('[casinogameid]' ,' ')
            else:
                sql = sql.replace('[casinogameid]', 'and cs.casinogameid = %(gameslug)s')
                sql = sql.replace('[slug]' ,' ')
    else:
        sql = sql.replace('[slug]', ' ').replace('[casinogameid]',' ')
    if typeid:
        sql = sql.replace('[typeid]','and casinogametypeid = %(typeid)s')
        args['typeid']=typeid
    else:
        sql = sql.replace('[typeid]','')
        
    ret = Common.CursorToDict(db.query(sql,args))
    
    return ret

def get_pcgames_bytypeid( siteid,jurisdictionid,ismobile,casinologo,casinourl,real,gameslug,selecttype=0,feature='',typeid='' ):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
             SELECT c.*,(CASE WHEN %(feature)s = '' then CONCAT(%(casinologo)s,c.logo) else CONCAT(%(casinologo)s,%(feature)s) END) gamelogo,
          CONCAT(%(casinourl)s,c.slug) url FROM casinogames c 
	     join casinogames_jurisdictions cj on c.casinogameid =cj.casinogameid WHERE c.casinogametypeid = %(typeid)s 
	     and cj.jurisdictionid=%(jurisdictionid)s AND c.active = 1 and c.ismobile =%(ismobile)s ORDER BY c.sortorder ASC, c.casinogameid DESC
          '''
    args = {'casinologo':casinologo,'casinourl':casinourl,'jurisdictionid':jurisdictionid,'ismobile':ismobile,'typeid':typeid}
    #if feature:
    args['feature'] = feature
    if gameslug:
        if type(gameslug) == tuple:
            args['gameslug'] = gameslug
            if int(selecttype) == 0:
                sql = sql.replace('[slug]', 'and cs.slug in %(gameslug)s')
                sql = sql.replace('[casinogameid]', ' ')
            else:
                sql = sql.replace('[casinogameid]', 'and cs.casinogameid in %(gameslug)s')
                sql = sql.replace('[slug]', ' ')
        else:
            args['gameslug'] = eval(gameslug)
            sql = sql.replace('[slug]', 'and cs.slug = %(gameslug)s')
            sql = sql.replace('[casinogameid]' ,'or cs.casinogameid = %(gameslug)s')
    else:
        sql = sql.replace('[slug]', ' ').replace('[casinogameid]',' ')

    ret = Common.CursorToDict(db.query(sql,args))
    
    return ret


def get_new_games(siteid,newkey):
    
    logging.info(siteid)
    logging.info(newkey)
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
         SELECT value FROM sites_settings WHERE siteid = %(siteid)s AND `key` = %(newkey)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'siteid':siteid,'newkey':newkey}))
    return ret

def get_setting( key,siteid ):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
           select value from sites_settings where siteid=%(siteid)s and `key`=%(key)s
          '''
    ret =  Common.CursorToDict(db.query(sql,{'siteid':siteid,'key':key}) )
    logging.info(ret)
    return ret
 
def get_articles(language_id,siteid):
    logging.info(language_id)
    logging.info(siteid)
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select A.articleid
				  FROM articles A
				  INNER JOIN articles_translations AT ON AT.articleid = A.articleid AND AT.languageid = %(language_id)s
				  WHERE AT.active = 1 AND AT.deleted = 0 AND A.publishdate < NOW() AND A.articleid IN (
				  	SELECT articles_sites.articleid
				  	FROM articles_sites
				  	WHERE articles_sites.articleid = A.articleid AND siteid = %(siteid)s
				  )
				  ORDER BY A.publishdate DESC 
          '''
    ret = Common.CursorToDict(db.query(sql,{'language_id':language_id,'siteid':siteid}))
    return ret

def get_article_byid(articleid,language_id,articlecontenturl):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
           SELECT A.author, A.articleimg, A.publishdate, 
           concat(%(url)s,AT.slug,'?isH5=1') articleurl,AT.*
				   FROM articles A
				   INNER JOIN articles_translations AT ON AT.articleid = A.articleid AND AT.languageid = %(language_id)s
				   WHERE A.articleid = %(articleid)s AND AT.active = 1 AND AT.deleted = 0 AND A.publishdate < NOW()
          '''
    ret = Common.CursorToDict(db.query(sql,{'language_id':language_id,'articleid':articleid,'url':articlecontenturl}))
    return ret
    
def get_subscription_plans(gametypeid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT * FROM subscriptionplans WHERE gametypeid = %(gametypeid)s AND active = 1 ORDER BY sortorder ASC
          '''
    ret = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid}))
    return ret
    
    
def get_user_flag(key,userid):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT flagvalue FROM users_flags WHERE userid = %(userid)s AND flagkey = %(key)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'userid':userid,'key':key}))
    if ret:
        return ret[0]['flagvalue']
    return 0
    
def get_all_subscription_plans(gametypeid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT * FROM subscriptionplans WHERE  active = 1 and gametypeid=%(gametypeid)s ORDER BY sortorder ASC
          '''
    ret = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid}))
    return ret
    
def get_string_id(name):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT stringid FROM strings WHERE  name = %(name)s 
          '''
    ret = Common.CursorToDict(db.query(sql,{'name':name}))
    if ret:
        return ret[0]['stringid']
    return ret
    
def get_trial_desc(languageid,stringid):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT singular,plural FROM strings_translations WHERE  languageid = %(languageid)s  and stringid=%(stringid)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'languageid':languageid,'stringid':stringid}))

    return ret[0]
    
def get_trial_subscription_price(key,currency):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT * FROM multi_currency_matrix where matrixkey=%(key)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'key':key,'currency':currency}))

    return ret[0][currency]
    
def get_price(subscriptionplanid,currency):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT price FROM subscriptionplans_pricing WHERE subscriptionplanid = %(subscriptionplanid)s and currency = %(currency)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'subscriptionplanid':subscriptionplanid,'currency':currency}))

    return ret[0]['price']
    
def get_game_perprice(gametypeid,currency):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select price from gametypes_pricing where gametypeid = %(gametypeid)s and currency = %(currency)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid,'currency':currency}))

    return ret[0]['price']
    
def get_currencies(currency):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT * FROM currencies [currency]
          '''
    if currency:
        sql = sql.replace('[currency]',' where currency =%(currency)s')
        ret = Common.CursorToDict(db.query(sql,{'currency':currency}))
    else:
        sql = sql.replace('[currency]','')
        ret = Common.CursorToDict(db.query(sql))
    return ret
 
def get_language_id(user_language):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          SELECT * FROM languages WHERE active = 1 and locale=%(user_language)s ORDER BY name ASC
          '''
    ret = Common.CursorToDict(db.query(sql,{'user_language':user_language}))
    return ret
    
def get_free_drawings(userid):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select freedrawings from users where userid = %(userid)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'userid':userid}))
    if ret:
        return ret[0]
    return ret
    
def get_result():
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select * from games where gametypeid in (9,2,22,1,4,24,7,16,3,12,25,18,15,20,17,19,11,10,14,13,5,23,8,21) order by find_in_set(gametypeid,'9,2,22,1,4,24,7,16,3,12,25,18,15,20,17,19,11,10,14,13,5,23,8,21') , drawdate desc 
          '''
    ret = Common.CursorToDict(db.query(sql))

    return ret
    
def get_per_result(gametypeid):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select g.drawid,g.gametypeid,g.drawdate,g.localdrawdatetime,g.jackpotsize,cast(g.number1 as char) number1,
          cast(g.number2 as char) number2,cast(g.number3 as char) number3,cast(g.number4 as char) number4,cast(g.number5 as char) number5,
          cast(g.number6 as char) number6,cast(g.number7 as char) number7,cast(g.number8 as char) number8,cast(g.extranumber1 as char) extranumber1,
          cast(g.extranumber2 as char) extranumber2,cast(g.extranumber3 as char) extranumber3,
          cast(g.extranumber4 as char) extranumber4,cast(g.bonusnumber1 as char) bonusnumber1,cast(g.bonusnumber2 as char) bonusnumber2,
          cast(g.bonusnumber3 as char) bonusnumber3,cast(g.refundnumber1 as char) refundnumber1,
          g.winningsdistributed,g.verified, 
          gt.slug,gt.name,UNIX_TIMESTAMP(g.drawdate)-UNIX_TIMESTAMP(UTC_TIMESTAMP()) lasttime
	  from games g join gametypes gt on g.gametypeid = gt.gametypeid where g.gametypeid=%(gametypeid)s and g.number1 is not null order by g.drawid desc limit 1
          '''
    
    ret = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid}))
    if ret:
        return ret[0]
    return ret
    	
    
    
def get_last_login(userid):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
            SELECT logindate, loginid, loginip FROM users_logins WHERE userid = %(userid)s ORDER BY loginid DESC LIMIT 1
             '''
        args = {
                 'userid'    :   userid
                }
        rows = db.query(sql, args)
        logging.info('rows:%s', rows)

        if not rows:
            return {}
        return rows[0]
    except:
        logging.error(traceback.format_exc())
    return {}


def getgametypes():

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               select * from casinogametypes order by sortorder asc
             '''

        rows = Common.CursorToDict(db.query(sql))
        return rows
    except:
        logging.error(traceback.format_exc())
        return []
        
def getgames(siteid):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT cs.*
							 FROM casinogames cs
							 LEFT JOIN  casinogames_sortorder cso ON cso.casinogameid = cs.casinogameid AND cso.siteid = %(siteid)s
							 WHERE cs.active = 1
							 ORDER BY -cso.sortorder DESC, cs.casinogameid DESC

             '''

        rows = Common.CursorToDict(db.query(sql,{'siteid':siteid}))
        return rows
    except:
        logging.error(traceback.format_exc())
        return []
        
def getavailablegames(jurisdiction_id):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT casinogameid FROM casinogames_jurisdictions WHERE jurisdictionid = %(jurisdiction_id)s

             '''

        rows = Common.CursorToDict(db.query(sql,{'jurisdiction_id':jurisdiction_id}))
        ret = []
        for gameid in rows:
            ret.append(gameid['casinogameid'])
        return ret
    except:
        logging.error(traceback.format_exc())
        return []

def get_lotto_sales(country,gametypeid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT sum(ordervalueeur) salesmoney FROM orders WHERE gametypeid=%(gametypeid)s and orderstatusid=200 
               and userid in (select userid from users where countryid = %(country)s) 
               and orderdate between date_sub(now(),interval 5 day) and now() 
             '''
        rows = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid,'country':country}))
	if rows:
            if rows[0]['salesmoney']:
                return rows[0]['salesmoney']
            else:
                return 0
        else:
	    return 0
    except:
        logging.error(traceback.format_exc())
        return 0


def get_latest_consume(userid,gametypeid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT ordervalueeur FROM orders WHERE gametypeid=%(gametypeid)s and orderstatusid=200 
               and userid =%(userid)s 
               and orderdate between date_sub(now(),interval 3 month) and now() order by orderdate desc limit 1
             '''
        rows = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid,'userid':userid}))
        if rows:
            if rows[0]['ordervalueeur']:
                return rows[0]['ordervalueeur']
            else:
                return 0
        else:
	    return 0
    except:
        logging.error(traceback.format_exc())
        return 0
        
def get_lotto_allsales(gametypeid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT sum(ordervalueeur) salesmoney FROM orders WHERE gametypeid=%(gametypeid)s and orderstatusid=200 
               and orderdate between date_sub(now(),interval 5 day) and now()
             '''
        rows = Common.CursorToDict(db.query(sql,{'gametypeid':gametypeid}))
        if rows:
            if rows[0]['salesmoney']:
                return rows[0]['salesmoney']
            else:
                return 0
        else:
	    return 0
    except:
        logging.error(traceback.format_exc())
        return 0

def get_history_games(jurisdictionid,casinologo,casinourl,userid):

    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
	        select cg.*, CONCAT(%(casinologo)s,cg.logo) gamelogo,
	             CONCAT(%(casinourl)s,cg.slug) url,cp.name casinoprovidername
               from casinohistory ch 
	             INNER JOIN casinogames cg on cg.casinogameid = ch.casinogameid
               JOIN casinogames_jurisdictions cj on  cj.casinogameid = ch.casinogameid
               JOIN casinoproviders cp on cp.casinoproviderid = cg.casinoproviderid
				       WHERE ch.userid = %(userid)s and cg.ismobile = 1
				       and cj.jurisdictionid =%(jurisdictionid)s
				       and ch.createdate between date_sub(now(),interval 3 month) and now()
				       GROUP BY ch.casinogameid
				       ORDER BY max(ch.casinohistoryid)

              '''
        rows = Common.CursorToDict(db.query(sql,{'userid':userid,'casinologo':casinologo,'casinourl':casinourl,'jurisdictionid':jurisdictionid}))
        return rows
    except:
        logging.error(traceback.format_exc())
        return []
        
def get_top_casino(country,jurisdictionid,casinologo,casinourl):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT cg.*, CONCAT(%(casinologo)s,cg.logo) gamelogo,
	             CONCAT(%(casinourl)s,cg.slug) url, sum(abs(ts.transactionvalueeur)) salesmoney ,cp.name casinoprovidername
               FROM transactions ts 
               join casinogames cg on ts.casinogameid = cg.casinogameid
               JOIN casinogames_jurisdictions cj on  cj.casinogameid = ts.casinogameid
               JOIN casinoproviders cp on cp.casinoproviderid = cg.casinoproviderid
               WHERE transactiontypeid=14 and cg.ismobile = 1
               and cj.jurisdictionid =%(jurisdictionid)s
               and userid in (select userid from users where countryid = %(country)s) 
               and transactiondate between date_sub(now(),interval 5 day) and now()
               group by ts.casinogameid
               order by salesmoney desc limit 20
             '''
        rows = Common.CursorToDict(db.query(sql,{'jurisdictionid':jurisdictionid,'country':country,'casinologo':casinologo,'casinourl':casinourl}))
        return rows
    except:
        logging.error(traceback.format_exc())
        return []


def get_casino_sales(country,casinogameid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT sum(abs(transactionvalueeur)) salesmoney 
               FROM transactions 
               WHERE transactiontypeid=14 
               and userid in (select userid from users where countryid = %(country)s) 
               and casinogameid = %(casinogameid)s
               and transactiondate between date_sub(now(),interval 5 day) and now()
             '''
        rows = Common.CursorToDict(db.query(sql,{'country':country,'casinogameid':casinogameid}))
        if rows:
            if rows[0]['salesmoney']:
                return rows[0]['salesmoney']
            else:
                return 0
        else:
            return 0
    except:
        logging.error(traceback.format_exc())
        return 0

def get_casino_consume(userid,casinogameid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
               SELECT sum(abs(transactionvalueeur)) salesmoney 
               FROM transactions 
               WHERE transactiontypeid=14 
               and userid =%(userid)s
               and casinogameid = %(casinogameid)s
               and transactiondate between date_sub(now(),interval 3 day) and now()
             '''
        rows = Common.CursorToDict(db.query(sql,{'userid':userid,'casinogameid':casinogameid}))
        if rows:
            if rows[0]['salesmoney']:
                return rows[0]['salesmoney']
            else:
                return 0
        else:
            return 0
    except:
        logging.error(traceback.format_exc())
        return 0

def add_banner(args_list):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        # set all banner to not active
        sql = '''
            update banners set status = 0
        '''
        rows = db.execute(sql)

        sql = '''
              insert into banners 
                  (bannername, bannertype, lottoid, title, description, picture, buttondesc, backgroundurl, addtime, activestarttime, activeendtime, lastmodifytime, jurisdictionid, status, operator,casinoslug,articleid)
              values
                  (%(bannername)s, %(bannertype)s, %(lottoid)s, %(title)s, %(description)s, %(picture)s, %(buttondesc)s, %(backgroundurl)s, now(), now(), now(), now(), %(jurisdictionid)s, 1, 'admin',%(casinoslug)s,%(articleid)s)
             '''
        rows = db.executeMany(sql, args_list)
        db.commit()
        return rows
    except:
        db.rollback()
        logging.error(traceback.format_exc())
        return 0

def get_banners(jurisdictionid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        # set all banner to not active
        sql = '''
            select * from banners where status = 1 and jurisdictionid in(0,%(jurisdictionid)s)
        '''
        rows = Common.CursorToDict(db.query(sql,{'jurisdictionid':jurisdictionid}))

        return rows
    except:
        logging.error(traceback.format_exc())
    return []

def get_free_drawings(userid):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    sql = '''
          select freedrawings from users where userid = %(userid)s
          '''
    ret = Common.CursorToDict(db.query(sql,{'userid':userid}))
    if ret:
        return ret[0]
    return ret

def get_file_assetfilename_by_id(assetid):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
              SELECT assetfilename FROM file_assets WHERE assetid = %(assetid)s
              '''
        ret = Common.CursorToDict(db.query(sql,{'assetid':assetid}))
        if ret:
            return ret[0]['assetfilename']
    except:
        logging.error(traceback.format_exc())
    return ''

def get_game_by_id(casinogameid):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
             SELECT * FROM casinogames WHERE casinogameid = %(casinogameid)s and active = 1
              '''
        args = {'casinogameid':casinogameid}

        ret = Common.CursorToDict(db.query(sql,args))
        if ret:
            return ret[0]
    except:
        logging.error(traceback.format_exc())
    return ''

def delete_casinofreespins(userid):
    db = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
            DELETE FROM casinofreespins WHERE userid = %(userid)s AND uploaded = 0 
            AND (startdate < date_sub(now(), INTERVAL 72 HOUR) or startdate IS NULL and createdate < date_sub(now(), INTERVAL 72 HOUR))
        '''
        args = {
            'userid'    :   userid
        }
        rows = db.execute(sql, args)
        if rows <= 0:
            db.rollback()
        db.commit()
        return rows
    except:
        logging.error(traceback.format_exc())

def get_casinofreespins(userid,casinogameid):
    db  = esunmysql.connect(baseconfig.Environment.MYSQL_ML_DB_CONFIG)
    try:
        sql = '''
              SELECT * FROM casinofreespins WHERE userid = %(userid)s [AND casinogameid] 
              AND uploaded = 0 AND (startdate IS NULL or startdate < now())
              '''
        args = {'userid':userid}
        if not casinogameid:
            sql = sql.replace('[AND casinogameid]','')
        else:
            sql = sql.replace('[AND casinogameid]','AND casinogameid = %(casinogameid)s')
            args['casinogameid'] = casinogameid

        ret = Common.CursorToDict(db.query(sql,args))
        if ret:
            return ret
    except:
        logging.error(traceback.format_exc())
    return ''