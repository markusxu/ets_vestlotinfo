#coding=utf-8

'''
咨询相关接口
'''
import os
import time
import math
import logging
import traceback
from decimal import Decimal

import env
import geoip
import maininfo
import retcode
import baseconfig
import functions
import XmlConfig
import esuncache
import JsonUtil
from bonus import bonus
from appuser import user
from baseconfig import Environment
from dbo import dboperator
from urllib import quote
import requests
from xml.dom.minidom import parseString
from env.jurisdiction import Jurisdiction
from xml.dom.minidom import parse as xml_parse
################################homepage################################

def getlotteries(*args, **kwargs):
    resp = {
        'CODE': str(retcode.SYSTEM_OK),
        'MSG': '',
        'info': dict(),
        'list': list()
    }
    try:
        userid = kwargs.get('userid', '')
        siteid = kwargs.get('siteid','')
	currency = kwargs.get('currency','EUR')
        jurisdiction_id = kwargs.get('jurisdictionid','')
        gametypeid = kwargs.get('gametypeid','')
        user_check  =   kwargs.get('usercheck', '')
        user_language = kwargs.get('language','en').upper()
        languageid = env.Language.get_languageid(user_language)
        version = kwargs.get('version','2.0.1')
        islogin  = 0
        if  userid and user_check:
            islogin  = 1
            resp = _user_check(user_check,userid,languageid)
            
            if int(resp.get('CODE')) < 0:
                islogin  = 0
                resp['CODE'] = str(retcode.SYSTEM_OK)
                
            userinfo = dboperator.get_user_totalinfo(userid)
            if  not userinfo:
                resp['CODE'] = str(retcode.SYSTEM_FAIL)
                resp['MSG']  = functions.t('SYSTEM_FAIL',[Environment.SERVICE_PAGECODE],0,languageid)
                return resp
            currency = userinfo['accountcurrency']

	remote_addr=kwargs.get('remote_addr')
        if not userid and not remote_addr:
            resp['CODE'] = str(retcode.SYSTEM_FAIL)
            resp['MSG']  = functions.t('SYSTEM_FAIL',[Environment.SERVICE_PAGECODE],0,languageid)
            return resp
        
        if not jurisdiction_id and not userid:
            country = geoip.find_country_iso_code(remote_addr)
            jurisdiction_id = env.Jurisdiction.get_jurisdiction_for_country(country)
            currency = env.Country.get_currency(country.upper())
            ret = env.FX.get_currency(currency)
            if not ret:
                currency = 'EUR'

        if not jurisdiction_id and userid and user_check:
            jurisdiction_id = int(userinfo['jurisdictionid'])

        if not siteid and not userid :
            siteid = env.Site.get_siteid_for_country(country)
            
        if not siteid and userid and user_check:
            siteid = int(userinfo['siteid'])
        
        if not jurisdiction_id:
            jurisdiction_id = 2
            
        if not siteid:
            siteid = 6

        gametype = dboperator.get_biggest_lottery(gametypeid,jurisdiction_id,version)
        lotteries = _sortgametypesbyjackpot(gametype,version,currency,languageid)
	for lottery in lotteries:
	    lottery['url'] = 'https://www.multilotto.com/en/campaign/landing/app-download?mp_source=10000'
        resp['info']['list'] = lotteries
        resp['info']['islogin'] = islogin
        
        return resp
    except:
        logging.error(traceback.format_exc())
        resp['CODE'] = str(retcode.SYSTEM_FAIL)
        resp['MSG']  = functions.t('SYSTEM_FAIL',[Environment.SERVICE_PAGECODE],0,languageid)
        return resp

def getresult(*args, **kwargs):
    resp = {
        'CODE': str(retcode.SYSTEM_OK),
        'MSG': '',
        'info': dict(),
        'list': list()
    }
    try:
        userid   = kwargs.get('userid', '') 
        siteid = kwargs.get('siteid','')
	currency = kwargs.get('currency','EUR')
        jurisdiction_id = kwargs.get('jurisdictionid','')
        language = kwargs.get('language','en')
        remote_addr=kwargs.get('remote_addr')
        user_check  =   kwargs.get('usercheck', '')
        languageid = env.Language.get_languageid(language)
        version = kwargs.get('version','2.0.1')

        islogin  = 0
        if  userid and user_check:
            islogin  = 1
            resp = _user_check(user_check,userid,languageid)
            if int(resp.get('CODE')) < 0:
                islogin  = 0
                resp['CODE'] = str(retcode.SYSTEM_OK)
                
            userinfo = dboperator.get_user_totalinfo(userid)
            if  not userinfo:
                resp['CODE'] = str(retcode.SYSTEM_FAIL)
                resp['MSG']  = functions.t('SYSTEM_FAIL',[Environment.SERVICE_PAGECODE],0,languageid)
                return resp
            
        remote_addr=kwargs.get('remote_addr')
        logging.info(kwargs)
        if not remote_addr:
            resp['CODE'] = str(retcode.SYSTEM_FAIL)
            resp['MSG']  = functions.t('SYSTEM_FAIL',[Environment.SERVICE_PAGECODE],0,languageid)
            return resp
        
        if not jurisdiction_id and not userid:
            country = geoip.find_country_iso_code(remote_addr)
            jurisdiction_id = env.Jurisdiction.get_jurisdiction_for_country(country)
            currency = env.Country.get_currency(country.upper())
            ret = env.FX.get_currency(currency)
            if not ret:
                currency = 'EUR'

        if not jurisdiction_id and userid and user_check:
            jurisdiction_id = int(userinfo['jurisdictionid'])

        if not siteid and not userid :
            siteid = env.Site.get_siteid_for_country(country)
            
        if not siteid and userid and user_check:
            siteid = int(userinfo['siteid'])
        
        if not jurisdiction_id:
            jurisdiction_id = 2
            
        if not siteid:
            siteid = 6

        sitehost = baseconfig.Environment.get('site_host', siteid)
	if version<'2.0.3':
            logourl = XmlConfig.get('/service/url/lottologourl')
	else:
            logourl = XmlConfig.get('/service/url/newlottologourl')

        resp['info']['list']=[]

	gametype = dboperator.get_biggest_lottery('',jurisdiction_id,version)
        lotteries = _sortgametypesbyjackpot(gametype,version,currency,languageid)
        gameidlist = []
	for i in lotteries:
	    gameidlist.append(int(i['gametypeid']))
        addgameidlist = [1,18,15,20,17,11,10,14,13,5,23,8,21,62,63,64,65]
        diffgameid = list(set(addgameidlist)-set(gameidlist) )
        gameidlist.extend(diffgameid)
        for gametypeid in gameidlist:

            ret = dboperator.get_per_result(gametypeid)
            logging.info(ret)
            if not ret:
                continue

            ret['logo']   = logourl+ret['slug']+'.png'

            #if userid and user_check:
            #    ret['url']    = sitehost+'/'+language.lower()+'/touch/auth/applogin/'+str(userid)+'/'+str(user_check)+'?url=lotto-results/'+ret['slug']+'/'+ret['localdrawdatetime'].split(' ')[0]+'?isH5=1'
            #else:
            #    ret['url']    = sitehost+'/'+language.lower()+'/lotto-results/'+ret['slug']+'/'+ret['localdrawdatetime'].split(' ')[0]+'?isH5=1'
	    ret['url'] = 'https://www.multilotto.com/en/campaign/landing/app-download?mp_source=10000'
            logging.info(ret['url'])
            resp['info']['list'].append(ret)
        resp['info']['islogin'] = islogin 
        
        return resp
    except:
        logging.error(traceback.format_exc())
        resp['CODE'] = str(retcode.SYSTEM_FAIL)
        resp['MSG']  = functions.t('SYSTEM_FAIL',[Environment.SERVICE_PAGECODE],0,languageid)
        return resp

def _sortgametypesbyjackpot(gametype,version,currency='EUR',languageid=1):
    
    rds = esuncache.connect('r.other')
    currency = dboperator.get_currencies(currency)
    if not currency:
        currency = dboperator.get_currencies('EUR')
    gametypesbyjackpot = []
    pendingjackpots = []
    gametypesbyremaintime=[]
    specialgame  = []
    spegameid=[]
    spegameindex = []
    retgamelist = []
    if version <'2.0.3':
        logourl = XmlConfig.get('/service/url/lottologourl')
        cologourl = XmlConfig.get('/service/url/checkoutlottologourl')
    else:
        logourl = XmlConfig.get('/service/url/newlottologourl')
        cologourl = XmlConfig.get('/service/url/newlottologourl')
    if len(gametype)>1:
        spegameid = JsonUtil.read(rds.get('specialgameid'))
        spegameindex = JsonUtil.read(rds.get('specialgameindex'))
    for g in gametype:
        if 0 == int(g['bonusnumbers']):
            g['bonusnumbermax'] = '0'
        ticketminprice = env.FX.get_local_amount('ticket_share_min_price',currency[0]['currency'])
        g['ticketshareminprice'] = str(ticketminprice)
	g['isnew']  = maininfo.get_new_lotto(g['gametypeid'])
	g['isspecial'] = maininfo.get_special_lotto(g['gametypeid'])
	g['worth']  = ""
        priceperdraw = dboperator.get_game_perprice(g['gametypeid'],currency[0]['currency'])
        if currency[0]['symbollocation'] == 'after':
            g['showprice'] = str(priceperdraw)+currency[0]['currencysymbol']
            
        else:
            g['showprice'] = currency[0]['currencysymbol'] + str(priceperdraw)
        
        if 3 == int(g['gametypeid']):
            g['bonusnumbermax'] = g['refundnumbermax']
            g['bonusnumbers']   = g['refundnumbers']
            g['bonusnumbermin'] = g['refundnumbermin']
        if int(g['remaintime']) > 0:
            if int(g['currentjackpot'])>0:

                jackpot = g['currentjackpot']
                if not jackpot:
                    continue

                if 63 != int(g['gametypeid']):
                    jackpot = env.FX.convert(int(g['currentjackpot']),g['currency'],currency[0]['currency'])
                    if Decimal(g['currentjackpot']) > Decimal(5e5):
                        jackpot = str(round(jackpot,-5))[:-2]
                    else:
                        jackpot = round((float(jackpot)/float(5000)-0.1),0)*5000

                g['currentjackpot']=str(jackpot)
                g['currencysymbol']=currency[0]['currencysymbol']
                g['symbollocation']=currency[0]['symbollocation']
                g['logo']          = logourl+g['slug']+'.png'
                g['cologo']        = cologourl+g['slug']+'.png'
		
                if 58 == int(g['gametypeid']):
                    jackpot = _dealwithcash4life(g['currency'],currency[0]['currency'])
		    g['jackpot'] = functions.amount_format(str(jackpot),currency[0]['currency'], places=0)
		    g['jackpot_bak']= functions.t('a_day_for_life',[Environment.SERVICE_PAGECODE],0,languageid,argv=[g['jackpot']])
		    g['jackpot'] = functions.t('per_day',[Environment.SERVICE_PAGECODE],0,languageid,argv=[g['jackpot']])
                elif 63 == int(g['gametypeid']):
                    g['jackpot'] = functions.amount_format(str(jackpot),g['currency'], places=0)
		    worth   = env.FX.convert(int(g['currentjackpot']),g['currency'],currency[0]['currency'])
		    g['currentjackpot']=str(round(worth))[:-2]
		    g['worth'] = functions.amount_format(str(round(worth))[:-2],currency[0]['currency'], places=0)
                else:
                    g['jackpot'] = functions.amount_format(str(jackpot),currency[0]['currency'], places=0)
                if int(g['gametypeid']) not in spegameid:
                    gametypesbyjackpot.append(g)
                else:
		    specialgame.append(g)
            else:
                g['currencysymbol']=currency[0]['currencysymbol']
                g['symbollocation']=currency[0]['symbollocation']
                g['logo']          = logourl+g['slug']+'.png'
                g['cologo']        = cologourl+g['slug']+'.png'
                if currency[0]['symbollocation'] == 'after':
                    g['jackpot'] = g['currentjackpot']+currency[0]['currencysymbol']
                else:
                    #g['jackpot'] = currency[0]['currencysymbol']+str(round(jackpot,-5))[:-2]
                    g['jackpot'] = currency[0]['currencysymbol']+g['currentjackpot']
                g['jackpot_bak'] = g['jackpot']
 		pendingjackpots.append(g)
        else:
            g['currencysymbol']=currency[0]['currencysymbol']
            g['symbollocation']=currency[0]['symbollocation']
            g['logo']          = logourl+g['slug']+'.png'
            g['cologo']        = cologourl+g['slug']+'.png'
            jackpot = g['currentjackpot']
            if int(g['currentjackpot'])>0 and 58 != int(g['gametypeid']) and 63!=int(g['gametypeid']):
                jackpot = env.FX.convert(int(g['currentjackpot']),g['currency'],currency[0]['currency'])
                jackpot = str(round(jackpot,-5))[:-2]

            elif int(g['currentjackpot'])>0 and 58 == int(g['gametypeid']):
                jackpot = functions.t('cash4life_first_prize',[Environment.SERVICE_PAGECODE],0,1)
           
                
            if 58 == int(g['gametypeid']):
                jackpot = _dealwithcash4life(g['currency'],currency[0]['currency'])
		g['jackpot'] = functions.amount_format(str(jackpot),currency[0]['currency'], places=0)
                g['jackpot_bak']= functions.t('a_day_for_life',[Environment.SERVICE_PAGECODE],0,languageid,argv=[g['jackpot']])
                g['jackpot'] = functions.t('per_day',[Environment.SERVICE_PAGECODE],0,languageid,argv=[g['jackpot']])
            elif 63 == int(g['gametypeid']):
                g['jackpot'] = functions.amount_format(str(jackpot),g['currency'], places=0)
                worth   = env.FX.convert(int(g['currentjackpot']),g['currency'],currency[0]['currency'])
                g['currentjackpot']=str(round(worth))[:-2]
                g['worth'] = functions.amount_format(str(round(worth))[:-2],currency[0]['currency'], places=0)
                
            else:
                g['jackpot'] = functions.amount_format(str(jackpot),currency[0]['currency'], places=0)
            if int(g['gametypeid']) not in spegameid:
	        gametypesbyremaintime.append(g)
            else:
                specialgame.append(g)

    gametypesbyjackpot=sorted( gametypesbyjackpot, key=lambda x:float(x['currentjackpot']), reverse=True )
    gametypesbyjackpot.extend(pendingjackpots)
    gametypesbyremaintime = sorted( gametypesbyremaintime, key=lambda x:float(x['remaintime']), reverse=True )
    gametypesbyjackpot.extend(gametypesbyremaintime)
    if len(specialgame) == len(spegameindex):
        for i in range(len(spegameindex)):
            gametypesbyjackpot.insert(int(spegameindex[i]),specialgame[i])
   

    return gametypesbyjackpot


################################
_LOCALS_ = locals()
def reg_interface(prefix):
    '''系统固定注册接口函数'''

    import types
    logging.info('starting register interface!')

    functions = {}

    for item in _LOCALS_.items():
        name = item[0].lower()
        name2= prefix.lower() + '_' + name
        obj  = item[1]

        if name in ["reg_interface", "load_module"]:
            continue

        if len(name) > 0 and name[0] == "_":
            continue

        if type( obj ) == types.FunctionType:
            functions[ name ] = obj
            functions[ name2 ]= obj

    return functions
