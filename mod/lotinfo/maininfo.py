#coding=utf-8

#when the 'has_lotto_subscriptions' is true then get default_draws
__DEFAULT_DRAWS_TRUE__ = {
					1  : [1, 4], # Euromillions
					2  : [1, 4], # Mega millions
					3  : [1, 4], # La Primitiva
					4  : [1, 3], # SuperEnaLoto
					7  : [1, 4], # Super Lotto Plus
					9  : [1, 4], # USA Powerball
					12 : [1, 4], # New York Lotto
					16 : [1, 4], # Mega Sena
                                        17 : [1, 4], # Norway
					19 : [1, 4], # Poland Lotto
                                        20 : [1, 4], # Finland lotto
					22 : [1, 2], # EuroJackpot
					24 : [1, 6], # BonoLoto
					25 : [1, 2], # El Gordo
					28 : [1, 4], # 6aus49
					31 : [1, 4], # irish lotto
					58 : [1, 4], # Cash 4 Life
                                        62 : [1, 4], # mini lotto
                                        63 : [1, 4], # bitcoin lottery
                                        64 : [1, 4], # Mega millions max
                                        65 : [1, 4], # eurojackpot-max lottery
              }


def get_default_draws_true(gametypeid):
    return __DEFAULT_DRAWS_TRUE__.get(int(gametypeid))
    
#when the 'has_lotto_subscriptions' is false then get default_draws
__DEFAULT_DRAWS_FALSE__ = {
					1  : [1, 4, 8], # Euromillions
					2  : [1, 4, 8], # Mega millions
					3  : [1, 4, 8], # La Primitiva
					4  : [1, 3, 12], # SuperEnaLoto
					7  : [1, 4, 8], # Super Lotto Plus
					9  : [1, 4, 8], # USA Powerball
					12 : [1, 4, 8], # New York Lotto
					16 : [1, 4, 8], # Mega Sena
                                        17 : [1, 4, 8], # Norway
                                        19 : [1, 4, 8], # Poland lotto
                                        20 : [1, 4, 8], # Finland lotto
					22 : [1, 2, 4], # EuroJackpot
					24 : [1, 6, 12], # BonoLoto
					25 : [1, 2, 4], # El Gordo
                                        28 : [1, 4, 8], # 6aus49
					31 : [1, 4, 8], # irish lotto
					58 : [1, 4, 8], # Cash 4 Life
                                        62 : [1, 4, 8], # mini lotto
                                        63 : [1, 4, 8], # bitcoin lottery
                                        64 : [1, 4, 8], # Mega millions max
                                        65 : [1, 4, 8], # eurojackpot-max lottery
              }


def get_default_draws_false(gametypeid):
    return __DEFAULT_DRAWS_FALSE__.get(int(gametypeid))


__CASINO_ENVKEY__ = {
         
          1  : 'enable_casino_playngo',
          2  : 'enable_casino_quickspin',
          3  : 'enable_casino_elk_gaming',
          4  : 'enable_casino_playson',
          5  : 'enable_casino_redrake',
          6  : 'enable_casino_microgaming',
          7  : 'enable_casino_yggdrasil',
          8  : 'enable_casino_netent',
	  10 : 'enable_casino_pragmaticplay',
          }

def get_key(casinoproviderid):
    return __CASINO_ENVKEY__.get(int(casinoproviderid))
    
__SITEID__={
         'AT' : 2 ,
         'DE' : 2 ,
         'MT' : 2 ,
         'PL' : 2 ,
         'GB' : 3 ,
         'IE' : 5 ,
         }

def get_jurisdiction_id(country):
    return __SITEID__.get(country,1)
    
__URL__={
         '1' : 'https://validator.curacao-egaming.com/4edf7094-51ec-4314-8c66-1d980a00002a' ,
         '2' : 'http://www.authorisation.mga.org.mt/verification.aspx?lang=EN&company=dd211825-2114-47a1-8cfa-94514aa5dc49&details=1' ,
         '3' : 'https://secure.gamblingcommission.gov.uk/PublicRegister/Search/Detail/47707'
         }

def get_regulatory_url(jurisdiction_id):
    return __URL__.get(jurisdiction_id,'')

__NEWLOTTO__={
         63 : '1',
	 64 : '1',
         65 : '1'
         }

def get_new_lotto(gametypeid):
    return __NEWLOTTO__.get(int(gametypeid),'0')

__SPECIALLOTTO__={
     9 : '1'
         }

def get_special_lotto(gametypeid):
    return __SPECIALLOTTO__.get(int(gametypeid),'0')

upcoming_lang=['EN']

popularlottoid = ['31','64','9','2','1','4','65','24','12','7','19','28']
popularlottoid_in_set = '31,64,9,2,1,4,65,24,12,7,19,28'
fixedlottoid = ('9','64','1','65')
mustplaylottoid = ['31','63','28','58']
mustplaylottoid_in_set = '31,63,28,58'
morelottoid = ['31','4','1','22','7','12','3','16','25','58','24','63']
morelottoid_in_set = '31,4,1,22,7,12,3,16,25,58,24,63'

__MUSTPLAYDESCRIBE__={
         31 : 'Ireland\'s Top Jackpot��Irish',
         63 : 'Win A Crypto Fortune!��1:8',
         28 : 'Great Value And Big Wins!��only   �1.00/line',
         58 : 'Win �1k cash every day!�� �1,000 a day for Life'
         }

def get_mustplay_lotto(gametypeid):
    return __MUSTPLAYDESCRIBE__.get(int(gametypeid),'')

__ISSALE__={
         '3_ES'  : 1,
         '4_IT'  : 1,
         '7_US'  : 1,
         '12_US' : 1,
         '16_BR' : 1,
         '17_NO' : 1,
         '19_PL' : 1,
         '20_FI' : 1,
         '24_ES' : 1,
         '25_ES' : 1,
         '28_DE' : 1,
         '31_IE' : 1,
         '62_PL' : 1
         }

def get_issale(game):
    return __ISSALE__.get(game,0)

__LOTTO_TYPE__={
      '1' : 'popular',
      '2' : 'biggestjackpots',
      '3' : 'closingsoon'
}

def get_lotto_type(type):
    return __LOTTO_TYPE__.get(str(type))