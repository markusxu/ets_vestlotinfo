#coding=utf-8
import os
import sys
import time
import traceback
import pycurl
import StringIO
import urllib
import json 
import logging
import XmlConfig
import urllib2

try:
    import signal
    from signal import SIGPIPE, SIG_IGN
    signal.signal(signal.SIGPIPE, signal.SIG_IGN)
except ImportError:
    pass


url = "http://admin.boss.com/~ouwc/admin.500wan.com/html/pages/finance/aliautopay/alipay.php?batchno=%s&channel=%s"%( '10006',  '1001' )
post_data_dic  = {'name' : 'es490'}

# 区分ets框架服务请求以及普通http请求
gServiceHost = {'ihsquery':'tc-ets-ihsquery','paynotify':'tc-ets-paynotify','matchdataquery':'match.ets.500.com'}
try:
    XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + '/etc/service.xml')
    host = XmlConfig.list('/service/http_host/')
    if host and isinstance(host,dict):
        gServiceHost.update(host)
except:
    logging.error('[httpclient]no http config')

##################### 服务可视化日志输出 ##############################  
def logCallback(args):
    """ 打印服务日志可视化的回调函数 """
    pass
    
def getData():
    """ 获取服务本身附属数据信息 """
    return {
            'from':'python', 
            'cmd':" ".join(sys.argv), 
            'remote_addr' : '', 
            'uuid' : '', 
            'usercheck' : '', 
            'cdn-src-ip' : '', 
            'useragent' : '',
            'source':''}
    
def viewLog(func):
    """ 装饰函数，记录服务耗时 """
    def new_func( *args, **kwargs):
        begintime = time.time()
        #将异常抛出 
        ret = func(*args, **kwargs) 
        try:
            result = '1'
            if not ret:
                result = '-1'
            costtime = (time.time() - begintime)*1000
            args = [str(args[0]), result, costtime]
            logCallback(args)
        except:
            logging.error(traceback.format_exc())
        return ret
    return new_func
    
try:
    import logdecoration
except:
    pass
##################### 服务可视化日志输出 ##############################

@viewLog
def httppost(url, post_data_dic, ctimeout=3, timeout=10):
    crl = pycurl.Curl()
    b = StringIO.StringIO()
    crl.setopt(pycurl.VERBOSE,1)
    crl.setopt(pycurl.FOLLOWLOCATION, 1)
    crl.setopt(pycurl.MAXREDIRS, 5)
    #crl.setopt(pycurl.AUTOREFERER,1)
    crl.setopt(pycurl.CONNECTTIMEOUT, 60)
    crl.setopt(pycurl.TIMEOUT, 300)
    #crl.setopt(pycurl.PROXY,proxy)
    crl.setopt(pycurl.HTTPPROXYTUNNEL,1)
    crl.setopt(pycurl.NOSIGNAL, 1)
    crl.setopt(pycurl.USERAGENT, "dhgu hoho")
    
    global gServiceHost
    for k,v in gServiceHost.items():
        if url.find(v) >= 0:
            data = getData()
            crl.setopt(pycurl.HTTPHEADER, ["User-Agent: %s" % str(data['useragent']), 'from:%s'%str(data['from']),'source:%s'%str(data['source']),'remote_addr:%s'%str(data['remote_addr']),'referer:%s'%str(data['cmd'])])
            logging.info('[ihsxmldata test ets]:url,' + str(url) + ',v:' + str(v))
            break
     
    # Option -d/--data <data>   HTTP POST data
    urllib.urlencode(post_data_dic)
    
    
    crl.setopt(crl.POSTFIELDS,  urllib.urlencode(post_data_dic))
     
    crl.setopt(pycurl.URL, url)
    crl.setopt(crl.WRITEFUNCTION, b.write)
    crl.setopt(pycurl.CONNECTTIMEOUT, ctimeout)
    crl.setopt(pycurl.TIMEOUT, timeout)
    crl.perform()
     
    ret = b.getvalue()

    b.close()
    crl.close()
    
    #print '-----------------------\m' + ret +'-----------------------'
    #print ret.split('\n')
    for line in  ret.split('\n'):
        if line.find('''alipay":{"is_success":''') >0:
            ret = line
        
    
    return ret

@viewLog
def httpget(url, ctimeout=100, timeout=100):

    fp = StringIO.StringIO() 
    
    curl_handle = pycurl.Curl()  
    curl_handle.setopt(pycurl.FOLLOWLOCATION, 1) 
    curl_handle.setopt(pycurl.MAXREDIRS, 5) 
    curl_handle.setopt(pycurl.CONNECTTIMEOUT, ctimeout)
    curl_handle.setopt(pycurl.TIMEOUT, timeout)
    curl_handle.setopt(pycurl.NOSIGNAL, 1)  
    #curl_handle.setopt(pycurl.LOW_SPEED_LIMIT, 100)  
    #curl_handle.setopt(pycurl.LOW_SPEED_TIME, 120)
    curl_handle.setopt(pycurl.MAXFILESIZE, 20485760 )
    curl_handle.setopt(pycurl.URL, url)
    curl_handle.setopt(pycurl.WRITEFUNCTION, fp.write)
    
    global gServiceHost
    flag = '0'
    for k,v in gServiceHost.items():
        if url.find(v) >= 0:
            data = getData()
            curl_handle.setopt(pycurl.HTTPHEADER, ["User-Agent: %s" % str(data['useragent']), 'from:%s'%str(data['from']),'source:%s'%str(data['source']),'remote_addr:%s'%str(data['remote_addr']),'referer:%s'%str(data['cmd'])])
            flag = '1'
            logging.info('[ihsxmldata test ets]:url,' + str(url) + ',v:' + str(v))
            break
    if flag == '0':
        curl_handle.setopt(pycurl.HTTPHEADER, ["User-Agent: %s" % "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)", '*/*'])
        logging.info('[ihsxmldata test http]:url,' + str(url))

    
    curl_handle.perform() 
    ret = fp.getvalue()

    fp.close()
    curl_handle.close()
    return ret
 
@viewLog 
def httpJPost(url, post_data_dic, ctimeout=8, timeout=10):
    crl = pycurl.Curl()
    b = StringIO.StringIO()
    crl.setopt(pycurl.SSL_VERIFYPEER, 0)
    crl.setopt(pycurl.SSL_VERIFYHOST, 0)
    crl.setopt(pycurl.VERBOSE,1)
    crl.setopt(pycurl.FOLLOWLOCATION, 1)
    crl.setopt(pycurl.MAXREDIRS, 5)
    crl.setopt(pycurl.NOSIGNAL, 1)
    crl.setopt(pycurl.HTTPPROXYTUNNEL,1)
    crl.setopt(crl.POSTFIELDS,  post_data_dic)
    crl.setopt(pycurl.URL, url)
    crl.setopt(crl.WRITEFUNCTION, b.write)
    crl.setopt(pycurl.CONNECTTIMEOUT, ctimeout)
    crl.setopt(pycurl.TIMEOUT, timeout)
    
    global gServiceHost
    flag = '0'
    for k,v in gServiceHost.items():
        if url.find(v) >= 0:
            data = getData()
            crl.setopt(crl.HTTPHEADER, ["User-Agent: %s" % str(data['useragent']), 'from:%s'%str(data['from']),'source:%s'%str(data['source']),'remote_addr:%s'%str(data['remote_addr']),'referer:%s'%str(data['cmd'])])
            flag = '1'
            logging.info('[ihsxmldata test ets]:url,' + str(url) + ',v:' + str(v))
            break
    if flag == '0':
        crl.setopt(crl.HTTPHEADER, ["Content-Type: text/json; charset=utf-8","Content-Length:"+str(len(post_data_dic))])
        logging.info('[ihsxmldata test http]:url,' + str(url))

    crl.perform()
    ret = b.getvalue()
    b.close()
    crl.close()
    return ret    

#HTTPS请求
def httpsPost(requrl,params):

    retdata =''
    try:
        paramsdata = urllib.urlencode(params)
        req = urllib2.Request(requrl,paramsdata)
        response = urllib2.urlopen(req)
        retdata = response.read()
        response.close()
        #logging.info("retdata=%s" %(retdata))

    except:
        logging.error('err=%s,params=%s' % (traceback.format_exc(),params))

    return retdata