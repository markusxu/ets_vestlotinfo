#coding=utf-8
'''
    使用实例:
'''
import os
import logging
from ftplib import FTP
import traceback
import socket


class FtpObj:
    #ftp初始化类
    def __init__(self, hostaddr, username, password, remotedir, port = 21, debug = 0):
        self.hostaddr  = hostaddr
        self.username  = username
        self.password  = password
        self.remotedir = remotedir
        self.port      = port
        self.ftp       = FTP()
        self.file_list = []
        self.debug     = debug
        if self.debug == 1:
            self.ftp.set_debuglevel(2)

    def __del__(self):
        self.ftp.close()
        if self.debug == 1:
            self.ftp.set_debuglevel(0) 
            
    #ftp登陆方法
    def login(self):
        timeout = 60
        try:
            socket.setdefaulttimeout(timeout)
            #true 被动模式 其他为主动模式
            self.ftp.set_pasv(0)
            self.ftp.connect( self.hostaddr, self.port )
            self.ftp.login( self.username, self.password )
            logging.info( "ftp login suc[%s]" %(self.ftp.getwelcome()) )
        except:
            logging.error( "ftp login err[%s]" %(traceback.format_exc()))
            return False
            
        try:
            self.ftp.cwd( self.remotedir )
            return True
        except:
            logging.error( "ftp cwd err[%s]" %(traceback.format_exc()))
            return False
    
    #ftp判断两个文件大小是否相等
    def isSameSize(self, localfile, remotefile):
        try:
            remotefile_size = self.ftp.size(remotefile)
        except:
            remotefile_size = -1
            
        try:
            localfile_size = self.ftp.size(localfile)
        except:
            localfile_size = -1
            
        if localfile_size == remotefile_size:
            return 1
            
        return 0
        
    #下载文件
    def getFile(self, localfile, remotefile = ''):
        #if self.isSameSize(localfile, remotefile):
        #    logging.info('文件大小相同,不需要下载')
        #    return
        
        if remotefile == '':
            remotefile = localfile
        
        try:
            file_handler = open(localfile, 'wb')
            self.ftp.retrbinary('retr %s'%(remotefile), file_handler.write)
            file_handler.close()
            return True
        except:
            logging.error('getFile error[%s]'%(traceback.format_exc()))
            return False
        
    #获取文件
    def _getFileName(self, line):
        pos = line.rfind(' ')
        #while( line[pos] != ' ' ):
        #    pos += 1
        while( line[pos] == ' ' ):
            pos += 1
        #兼容win ftp服务器
        if 'DIR' in line:
            return ['d', line[pos:]]
            
        if line[0] == 'd':
            return ['d', line[pos:]]
            
        file_arr = ['-', line[pos:]]
        return file_arr
        
        
    #获取文件列表
    def _getFileList(self, line):
        ret_arr = []
        file_arr = self._getFileName( line )
        if file_arr[1] not in ['.', '..']:
            self.file_list.append(file_arr)
            
    # 获取远程文件列表
    def getRemoteFileList(self, remotedir='./'):
        try:
            self.ftp.cwd(remotedir)
        except:
            logging.error('目录不存在%s'%(remotedir))
            return False
            
        self.file_list = []
        self.ftp.dir(self._getFileList)
        return self.file_list
        
        
    #批量下载文件
    def getFiles(self, localdir = './', remotedir = './'):
        try:
            self.ftp.cwd(remotedir)
        except:
            logging.error('目录不存在%s'%(remotedir))
            return False
        if not os.path.isdir(localdir):
            os.makedirs(localdir)
        
        try:    
            self.file_list = []
            self.ftp.dir(self._getFileList)
            remotenames = self.file_list
            for item in remotenames:
                filetype = item[0]
                filename = item[1]
                local = os.path.join(localdir, filename)
                if filetype == 'd':
                    self.getFiles(local, filename)
                elif filetype == '-':
                    self.getFile(local, filename)
            self.ftp.cwd('..')
        except:
            logging.error('getFiles error[%s]'%(traceback.format_exc()))
            return False
        
    #上传文件
    def putFile(self, localfile, remotefile=''):
        if not os.path.isfile(localfile):
            return False
            
        if remotefile == '':
            remotefile = localfile
        
        try:
            file_handler = open(localfile, 'rb')
            self.ftp.storbinary('STOR %s' %remotefile, file_handler)
            file_handler.close()
        except:
            logging.error('putFile error[%s]'%(traceback.format_exc()))
            return False

        return True

    #批量上传文件
    def putFiles(self, localdir='./', remotedir='./'):
        if not os.path.isdir(localdir):
            return
        localnames = os.listdir(localdir)
        self.ftp.cwd(remotedir)
        for item in localnames:
            src = os.path.join(localdir, item)
            if os.path.isdir(src):
                try:
                    self.ftp.mkd(item)
                except:
                    logging.error('目录已经存在 %s' %(traceback.format_exc()))
                self.putFiles(src, item)
            else:
                self.putFile(src, item)
        self.ftp.cwd('..')
        
    #删除文件
    def deleteFile(self, remotefile):
        try:
            self.ftp.delete( remotefile )
        except:
            logging.error('deleteFile error[%s]' %(traceback.format_exc()))
            return False
        return True
    
        
if __name__ == '__main__':
    f = FtpObj('localhost', 'ftptest', '123456', './')
    f = FtpObj('59.151.35.71', '12010001', '1qaz@WSX', './')
    f.login()
    #f.getFiles('/home/es686/ftptest')
    f.putFile('/home/es686/uploadfile1', './uploadfile1')
        
        
    
        
