#coding=utf-8
from mcrypt import *
import hashlib

MCRYPT_ENCRYPT = 0
MCRYPT_DECRYPT = 1
MCRYPT_DEV_RANDOM = 0
MCRYPT_DEV_URANDOM = 1
MCRYPT_RAND = 2
MCRYPT_3DES = "tripledes"
MCRYPT_ARCFOUR_IV = "arcfour-iv"
MCRYPT_ARCFOUR = "arcfour"
MCRYPT_BLOWFISH = "blowfish"
MCRYPT_BLOWFISH_COMPAT = "blowfish-compat"
MCRYPT_CAST_128 = "cast-128"
MCRYPT_CAST_256 = "cast-256"
MCRYPT_CRYPT = "crypt"
MCRYPT_DES = "des"
MCRYPT_ENIGNA = "crypt"
MCRYPT_GOST = "gost"
MCRYPT_LOKI97 = "loki97"
MCRYPT_PANAMA = "panama"
MCRYPT_RC2 = "rc2"
MCRYPT_RIJNDAEL_128 = "rijndael-128"
MCRYPT_RIJNDAEL_192 = "rijndael-192"
MCRYPT_RIJNDAEL_256 = "rijndael-256"
MCRYPT_SAFER64 = "safer-sk64"
MCRYPT_SAFER128 = "safer-sk128"
MCRYPT_SAFERPLUS = "saferplus"
MCRYPT_SERPENT = "serpent"
MCRYPT_THREEWAY = "threeway"
MCRYPT_TRIPLEDES = "tripledes"
MCRYPT_TWOFISH = "twofish"
MCRYPT_WAKE = "wake"
MCRYPT_XTEA = "xtea"
MCRYPT_IDEA = "idea"
MCRYPT_MARS = "mars"
MCRYPT_RC6 = "rc6"
MCRYPT_SKIPJACK = "skipjack"
MCRYPT_MODE_CBC = "cbc"
MCRYPT_MODE_CFB = "cfb"
MCRYPT_MODE_ECB = "ecb"
MCRYPT_MODE_NOFB = "nofb"
MCRYPT_MODE_OFB = "ofb"
MCRYPT_MODE_STREAM = "stream"


def encodeUsername(username,key='en_*'):
    '''加密用户名'''
    return encodeStr(key+username)
    
def decodeUsername(username,key='en_*'):
    '''解密用户名'''
    username = decodeStr(username)
    if username[0:4]!=key:
        return False
    return username[4:]

def encodeStr(str,key='asd*()'):
    '''加密字符串'''
    str = str.strip()
    m = MCRYPT(MCRYPT_CAST_256,MCRYPT_MODE_CFB)
    iv = hashlib.md5(key).hexdigest()[:MCRYPT.get_iv_size(m)] 
    keysize = m.get_key_size()
    if len(key)<keysize:
        key = key+'\0'*(keysize-len(key))
    else:
        key = key[:keysize]
    m.init(key,iv)
    str = m.encrypt(str)
    return str.encode('base64').strip()

def decodeStr(str,key='asd*()'):
    '''解密字符串'''
    str = str.decode('base64')
    m = MCRYPT(MCRYPT_CAST_256,MCRYPT_MODE_CFB)
    iv = hashlib.md5(key).hexdigest()[:MCRYPT.get_iv_size(m)] 
    keysize = m.get_key_size()
    if len(key)<keysize:
        key = key+'\0'*(keysize-len(key))
    else:
        key = key[:keysize]
    m.init(key,iv)
    return m.decrypt(str).strip()

    