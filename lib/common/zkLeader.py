#coding=utf-8
import os
import logging
import threading
import time
import traceback
import sys

import zkclient as zk
from zkclient import watchmethod

_ELECTION_PATH_ = "/election/"
_CANDIDATE_PREFIX_ = "CA"
_ELECTION_INS_ = {}


class election:

    def __init__(self, appid, zk_address, func=None, time_out=2000):
        global _ELECTION_PATH_
        
        self.appid          = appid
        self.zk_address     = zk_address
        self.func           = func
        self.time_out       = time_out
        self.election_path  = _ELECTION_PATH_ + self.appid
        self.is_leader      = False
        self.candidate_id   = None
        self.leader         = None
        self.first_candidate= None
        self.candidate_list = []
        self.last_session_state = None

        self.zkcli          = self.connect( zk_address, time_out )

    def connect(self, address, timeout):
        
        def connection_watcher(type, state):
            if type != 'session':
                # 不处理session以外的event
                return

            if 'connecting' == self.last_session_state :
                # 如果上一个会话状态是“连接中”，则表示会话已经断开
                if 'connected' == state:
                    # 如果重新连接上了，则需要重新进行选举，并且设置watcher
                    logging.info( 'reconnect zookeeper!' )
                    self.elect()
                    self.set_watcher()

            self.last_session_state = state
        
        return zk.ZKClient( address, timeout, connection_watcher )

    def get_election_status( self ):
        status = self.get_value( self.election_path )
        if not status:
            return 'off'
        return status
    
    def get_value_by_cid(self, cid):
        return self.get_value(self.election_path + '/' + cid)

    def get_value(self, path):
        try:
            znode = self.zkcli.get( path )
            if znode:
                return znode[0]
        except:
            logging.error( traceback.format_exc() )
        return None

    def pause(self):
        self.zkcli.set( self.election_path, 'off' )

    def start(self):
        self.zkcli.set( self.election_path, 'on' )
    
    def create_path(self, path):

        # 如果路径存在，直接返回
        if self.zkcli.exists( path ):
            return True

        # 检查路径的格式
        path = path.strip()
        if len(path) <=0 or path[0] != '/':
            logging.error('[zookeeper]wrong format of path [%s]'%path)
            return False
        path_split  = path.split('/')
        path_split.remove( '' )
    
        # 创建znode
        path_tmp = ""
        for i in range(len(path_split)):
            path_tmp = path_tmp + '/' + path_split[i]
            if not self.zkcli.exists( path_tmp ):
                self.zkcli.create( path_tmp, "on" )

        if self.zkcli.exists( path ):
            return True
        else:
            return False


    def renew_candidates(self):
        try:
            self.candidate_list = self.zkcli.get_children( self.election_path )
        except:
            logging.error( traceback.format_exc() )
            return False

    def elect(self):

        # 取候选人列表
        self.renew_candidates()
        logging.info( "[zookeeper]get candidate_list %s"%self.candidate_list )
        if len(self.candidate_list) <= 0:
            # 都没人，选啥呢？
            return False
        
        # 排序检查
        candidate_list = self.candidate_list
        candidate_list.sort()
        
        # 序号在最前面的就是leader
        self.leader = candidate_list[0]
        if self.candidate_id == self.leader:
            self.is_leader = True

        # 记录第一候选人
        if len( candidate_list ) >= 2:
            self.first_candidate = candidate_list[1]

        return True

    def set_watcher(self):
        
        @watchmethod
        def watch_election(event):
            logging.info( '[zookeeper]get election event' )
            logging.info( "<ClientEvent %s at %r state: %s>" % (event.type_name, event.path, event.state_name) )
            
            if 'changed' != event.type_name:
                # 不是结点被修改，则无视
                return self.zkcli.get( self.election_path, watch_election )
            
            if 'on' == self.get_election_status():
                # 选举被打开，则重新进行选举
                self.elect()
                self.set_watcher()
            else:
                # 如果选举依然被关闭，则继续关注父节点
                self.zkcli.get( self.election_path, watch_election )

        @watchmethod
        def watch_candidate(event):
            logging.info( '[zookeeper]get candidate event' )
            logging.info( "<ClientEvent %s at %r state: %s>" % (event.type_name, event.path, event.state_name) )

            if 'deleted' != event.type_name:
                # 不是结点消失，则无视
                return self.set_watcher()

            if 'on' != self.get_election_status():
                # 如果选举被关闭，则转而关注父节点
                self.zkcli.get( self.election_path, watch_election )
                return True
            
            if self.candidate_id == self.first_candidate:
                # 如果是第一候选人，则直接成为leader
                self.is_leader = True
                self.leader = self.candidate_id
                self.set_watcher()
            else:
                # 不是第一候选人，则重新进行选举
                self.elect()
                self.set_watcher()

        if None == self.candidate_id or None == self.leader:
            return False

        if 'on' != self.get_election_status():
            # 如果选举被关闭，则转而关注父节点
            self.zkcli.get( self.election_path, watch_election )
            return True

        # 如果是leader，则调用指定的方法，然后返回
        if self.is_leader and None != self.func:
            try:
                func = self.func
                func()
            except:
                logging.error( traceback.format_exc() )
            return True


        # 如果是第一候选人，则关注leader
        if None != self.first_candidate and self.candidate_id == self.first_candidate:
            try:
                self.zkcli.get( self.election_path + '/' + self.leader, watch_candidate )
            except:
                logging.error( traceback.format_exc() )

        # 如果是其他候选人，则关注第一候选人
        if None != self.first_candidate and self.candidate_id != self.first_candidate:
            try:
                self.zkcli.get( self.election_path + '/' + self.first_candidate, watch_candidate )
            except:
                logging.error( traceback.format_exc() )

        return True

    def join(self):
        if self.candidate_id != None:
            # 防止重复join
            return True

        global _CANDIDATE_PREFIX_
        
        # 先创建election路径
        election_path  = self.election_path
        self.create_path( election_path )

        # 取本机ip、本进程id
        try:
            ip = _get_ip_address()
            pid = os.getpid()
        except:
            ip  = ''
            pid = ''

        # 再创建子结点，子节点的value="本机ip/本进程id"
        try:
            value = str(ip) + '/' + str(pid)
            candidate_path = election_path + "/" + _CANDIDATE_PREFIX_

            create_path = self.zkcli.create( candidate_path , value, flags=zk.zookeeper.EPHEMERAL | zk.zookeeper.SEQUENCE)

        except:
            logging.error( traceback.format_exc() )
            return False
        
        # 得到候选者ID
        create_child = create_path.split('/')
        self.candidate_id = create_child[-1]
        logging.info( "[zookeeper]create path[%s]"%create_path )

        return True

def setup( appid, addr, func=None):
    '''
    功能：开启zookeeper的leader选举功能
    入参：appid服务名称
          addr是zookeeper服务的地址，为空则到etc/service.xml里面获取
          （可选）func回调函数----被选为leader以后，希望被调用的函数。
    返回：被分配的（候选者）ID
    '''
    global _ELECTION_INS_
    
    # 前面防止重复setup
    if _ELECTION_INS_.has_key(appid) and _ELECTION_INS_[appid].candidate_id != None:
        logging.info( "[zookeeper]appid[%s] has been setup"%appid )
        return True

    # 初始化选举对象
    try:
        elec = election( appid, addr, func )
        _ELECTION_INS_[appid] = elec
    except:
        logging.error( traceback.format_exc() )
        return False

    return True

def join( appid=None ):
    elec = _get_election( appid )
    if not elec:
        return False

    # 参与到选举
    elec.join()

    # 做一次选举
    elec.elect()

    # 设置监控
    elec.set_watcher()

    logging.info( "[zookeeper]join in election successful. appid[%s] candidate_id[%s]"%(elec.appid, elec.candidate_id) )
    return True

def start( appid=None ):
    '''
    功能：暂停选举，不再选举新的Leader；
            如果appid为空，则参考_ELECTION_INS_的第一个对象
    '''
    elec = _get_election( appid )
    if not elec:
        return False
    elec.start()
    return True

def pause( appid=None ):
    '''
    功能：暂停选举，不再选举新的Leader；
            如果appid为空，则参考_ELECTION_INS_的第一个对象
    '''
    elec = _get_election( appid )
    if not elec:
        return False
    elec.pause()
    return True
    
def get_leader( appid=None ):
    '''
    功能：返回指定appid的leader的ID；
            如果appid为空，则参考_ELECTION_INS_的第一个对象
    '''
    elec = _get_election( appid )
    if not elec:
        return []
    # 获取最新的列表
    elec.elect()
    leader = elec.leader
    if not leader:
        return leader, ''
    return leader, elec.get_value_by_cid( leader )

def get_candidates( appid=None ):
    '''
    功能：取候选人列表
    '''
    ret = []
    elec = _get_election( appid )
    if not elec:
        return ret

    # 获取最新的列表
    elec.elect()
    for cid in elec.candidate_list:
        info = (cid, elec.get_value_by_cid( cid ))
        ret.append( info )

    return ret


def get_cid( appid = None ):
    '''
    功能：取自己的候选人ID
    '''
    elec = _get_election( appid )
    if not elec:
        return None
    return elec.candidate_id

def get_status( appid = None ):
    elec = _get_election( appid )
    if not elec:
        return ''
    return elec.get_election_status()


def is_leader( appid=None ):
    '''
    功能：如果是指定appid的leader，就返回True，否则返回False；
            如果appid为空，则参考_ELECTION_INS_的第一个对象
    '''
    elec = _get_election( appid )
    if not elec:
        return False
    return elec.is_leader



def _get_election( appid=None ):
    global _ELECTION_INS_

    # 如果没有传入appid，则取第一个election对象
    if None == appid and len(_ELECTION_INS_) > 0:
        appid = _ELECTION_INS_.items()[0][0]

    if not _ELECTION_INS_.has_key( appid ):
        return None
    else:
        return _ELECTION_INS_[ appid ]
    

def _get_ip_address(ifname='eth0'):

    import socket
    import fcntl
    import struct

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
    except:
        ifname = "bond0"
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])

    

def usage():
    print 'usage: python ', sys.argv[0], ' address  appid  <start|pause|candidates|leader|status>'
    sys.exit()

if __name__ == '__main__':
    if len(sys.argv) != 4:
        usage()

    address     = sys.argv[1]
    appid       = sys.argv[2]
    operation   = sys.argv[3]
    if operation not in ( 'start', 'pause', 'candidates', 'leader', 'status' ):
        usage()
    
    try:
        setup( appid, address )
        if operation == 'start':
            start( appid )
        elif operation == 'pause':
            pause( appid )
        elif operation == 'candidates':
            print get_candidates( appid )
        elif operation == 'leader':
            print get_leader( appid )
        elif operation == 'status':
            print get_status( appid )
        else:
            usage()

    except:
        print traceback.format_exc()
        usage()

