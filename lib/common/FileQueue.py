#coding=utf-8
import os
import cPickle
import Queue
import time
import threading
import traceback
import uuid

''' 文件队列
*）使用说明：
1.如果队列数据需要保存，实例化对象时必须传入file
2.一个file只能实例化一个对象，否则抛出异常
3.调用close方法完成队列信息保存
*）使用示例：
    q = FileQueue('test.data')
    q.put(1)
    print q.get()
    q.put(1)
    q.close()
'''

class Exists(Exception):
    pass
    
class Empty(Exception):
    pass
    
class HasClosed(Exception):
    pass

class FileQueue(object):
    """文件队列
    """
    lock = threading.RLock()
    ins = {}
    
    def __init__(self, file=None, qsize=10000, fsize=30 * 1024 * 1024):
        """创建队列，加载file文件数据
            file： 文件名
                如果不为None，当队列关闭时，保存队列信息
                如果为None，自动生成，当队列关闭时，删除文件
            qsize：内存队列大小
            fsize：文件轮转大小
        """
        try:
            FileQueue.lock.acquire()
            self._lock = threading.RLock()
            self._max_qsize = qsize #内存队列最大长度
            self._max_fsize = fsize #文件轮转大小
            self._use_file = False #是否使用本地文件标志
            self._file_list = [] #本地文件列表
            self._queue = Queue.Queue() #内存队列
            self._c_w = None #当前处理的本地文件路径
            self._c_w_f = None #当前处理的本地文件描述符
            self._size = 0 #队列长度
            self._persist = False #对象销毁时是否需要保留数据
            self._has_closed = False #是否已经保存
            if file is not None:
                file = os.path.realpath(file)
                if FileQueue.ins.has_key(file):
                    raise Exists
                FileQueue.ins[file] = None
                self._file = file #文件路径
                self._persist = True 
            else:
                self._file = '/tmp/filequeue/%s.data'%(uuid.uuid1().get_hex()[:12])
            dname = os.path.dirname(self._file)
            if not os.path.isdir(dname):
                os.makedirs(dname, 0755)
            if os.path.isfile(self._file):
                f = open(self._file, 'r')
                try:
                    if self._file == cPickle.load(f):
                        self._size = cPickle.load(f)
                        self._file_list = cPickle.load(f)
                        self._use_file = True
                except:
                    pass
                f.close()
                os.remove(self._file)
        finally:
            FileQueue.lock.release()
            
    def put(self, data):
        """添加
        """
        try:
            self._lock.acquire()
            if self._has_closed is True:
                raise HasClosed
            if self._use_file:
                try:
                    cPickle.dump(data, self._get_file_descriptor(), cPickle.HIGHEST_PROTOCOL)
                except:
                    pass
            else:
                self._queue.put(data)
                if self._queue.qsize() >= self._max_qsize:
                    self._use_file = True
            self._size += 1
        finally:
            self._lock.release()

    def get(self):
        """获取（非阻塞）
        """
        try:
            self._lock.acquire()
            if self._has_closed is True:
                raise HasClosed
            data = self._queue.get(0)
            self._size -= 1
            return data
        except Queue.Empty:
            if self._use_file is False:
                raise Empty
            info = self._get_file()
            if not info:
                raise Empty
            if info[0] == self._c_w:
                self._c_w_f.flush()
            f = open(info[0], 'r')
            f.seek(info[1])
            try:
                for i in xrange(self._max_qsize):
                    self._queue.put(cPickle.load(f))
            except EOFError:
                os.remove(info[0])
                if info[0] == self._c_w:
                    self._use_file = False
                    self._c_w = None
                    self._c_w_f.close()
                    self._c_w_f = None
            else:
                info[1] = f.tell()
                self._add_file(info, 0)
            finally:
                f.close()
            return self.get()
        finally:
            self._lock.release()
            
    def qsize(self):
        return self._size
            
    def _get_file(self):
        """取本地文件信息
        """
        if len(self._file_list) == 0:
            return None
        info = self._file_list.pop(0)
        return info
        
    def _add_file(self, info, pos=0):
        """添加本地文件信息
        """
        if pos == 0:
            self._file_list.insert(0, info)
        else:
            self._file_list.append(info)
            
    def _get_file_descriptor(self):
        """获取写文件的描述符
        """
        if self._c_w is None or os.path.getsize(self._c_w) >= self._max_fsize:
            try:
                self._c_w_f.close()
            except:
                pass
            self._c_w = '%s.%s' % (self._file, uuid.uuid1().get_hex()[:12])
            self._c_w_f = open(self._c_w, 'a+')
            self._add_file([self._c_w, 0], 1)
        return self._c_w_f
        
    def close(self):
        """保存队列数据
        """
        try:
            self._lock.acquire()
            if self._has_closed is True:
                raise HasClosed
            self._has_closed = True
            if self._persist is False:
                for i in self._file_list:
                    try:
                        os.remove(i[0])
                    except:
                        pass
                return None
            if self._c_w_f is not None:
                self._c_w_f.close()
            if not self._queue.empty():
                fname = '%s.%s' % (self._file, uuid.uuid1().get_hex()[:12])
                f = open(fname, 'w+')
                while 1:
                    try:
                        cPickle.dump(self._queue.get(0), f, cPickle.HIGHEST_PROTOCOL)
                    except:
                        break        
                f.close()
                self._add_file([fname, 0], 0)
            if len(self._file_list) == 0:
                return None
            f = open(self._file, 'w+')
            try:
                cPickle.dump(self._file, f, cPickle.HIGHEST_PROTOCOL)
                cPickle.dump(self._size, f, cPickle.HIGHEST_PROTOCOL)
                cPickle.dump(self._file_list, f, cPickle.HIGHEST_PROTOCOL)
            except:
                pass
            f.close()
        finally:
            self._lock.release()
        
        try:
            FileQueue.lock.acquire()
            FileQueue.ins.pop(self._file)
        except:
            pass
        finally:
            FileQueue.lock.release()
