#coding=utf-8
import Queue

class Singleton(type):  
    def __init__(cls, name, bases, dict):  
        super(Singleton, cls).__init__(name, bases, dict)  
        cls._instance = None  
    def __call__(cls, *args, **kw):  
        if cls._instance is None:  
            cls._instance = super(Singleton, cls).__call__(*args, **kw)  
        return cls._instance  
  
class Queueconn(object):  
    __metaclass__ = Singleton 
    
    def __init__(self, maxsize = 0 ):
        self.conn_q = Queue.Queue( maxsize )
    
    #队列中添加数据
    #返回1：添加成功
    #返回0：队列已满
    def put(self, data, block = 0):
        try:
            self.conn_q.put(data, block)
            return 1
        except:
            return 0
    
    #从队列中获取数据
    def get(self, num = 1, block = 0, timeout = 60):
        res = []
        try:
            for i in range(num):   
                res.append(self.conn_q.get(block, timeout))
            return res
        except:
            return res
            
    def qsize(self):
        return self.conn_q.qsize()
        
    def empty(self):
        return self.conn_q.empty()
        
    def full(self):
        return self.conn_q.full()
        
        
#test
if __name__ == '__main__':
    a = Queueconn(5)
    b = Queueconn()
    print a.qsize()
    for i in range(10):
        print b.put("test")
        
    print a.get(10)
        
        
        
        
        
        
    