#coding=utf-8
import sys, traceback, time
import logging
import DbPool
import Common
import Config
import json



class tool:

    def setDB( self, dbname ):
        self.dbname = dbname
        return True

    def setSQL( self, sql ):
        sql = Common.setLabel( sql )
        self.sql = sql
        return True

    def setArgs(self, args):
        self.args = args
        
        if list == type(args):
            for i in range(len(args)):
                if args[i] == Config._NUM_ORA_TYPE_:
                    self.resultNUM['v_'+str(i)] = i

                elif args[i] == Config._STR_ORA_TYPE_:
                    self.resultSTR['v_'+str(i)] = i

                elif args[i] == Config._CUR_ORA_TYPE_:
                    self.resultCUR['v_'+str(i)] = i

                else:
                    pass

        return True

    def setRange( self, skip=0, limit=1000 ):
        self.skip   = skip
        self.limit  = limit
        return True

    def setPage( self, pagenum=0, pagelen=1000 ):
        self.pagenum = pagenum
        self.pagelen = pagelen
        return True

    def call(self):
        logging.error( '错误的调用：父类' )
        return False


    def getNumberResult(self):
        for key, value in self.resultNUM.items():
            self.resultNUM[ key ] = self.args[value].getvalue()

        return self.resultNUM


    def getStringResult(self):
        for key, value in self.resultSTR.items():
            self.resultSTR[ key ] = self.args[value]

        return self.resultSTR


    def getCursorResult(self):
        for key, value in self.resultCUR.items():
            self.resultCUR[ key ] = Common.CursorToDict( self.args[value] )

        return self.resultCUR

    def __init__( self ):
        self.dbname = ''
        self.sql    = ''
        self.args   = None
        self.skip   = 0
        self.limit  = 1000
        self.pagenum = 0
        self.pagelen = 1000
        self.resultNUM = {}
        self.resultSTR = {}
        self.resultCUR = {}




class function( tool ):
    def __init__( self ):
        tool.__init__( self )
        self.returnType = Config._NUM_ORA_TYPE_
        self.returnVal  = ''

    def setReturnType( self, returnType ):
        self.returnType = returnType
        return True

    def call(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            self.returnVal = db.callfunc(self.sql, self.returnType, self.args)
            db.commit()
        except db.dbErr, err:
            logging.error("数据库调用异常")
            logging.error(err)
            db.rollback()
            return False

        return True

    def getReturnVal( self ):
        return self.returnVal


class procedure( tool ):

    def call(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            ret = db.callproc2(self.sql, self.skip, self.limit, self.args)
            db.commit()
        except db.dbErr, err:
            logging.error("数据库调用异常")
            logging.error(err)
            db.rollback()
            return False

        return True

    def call3(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            ret = db.callproc3(self.sql, self.pagenum, self.pagelen, self.args)
            db.commit()
        except db.dbErr, err:
            logging.error("数据库调用异常")
            logging.error(err)
            db.rollback()
            return False

        return True

class query( tool ):
    def __init__( self ):
        tool.__init__( self )
        self.cur     = []



    def call(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            self.cur = db.query2(self.skip, self.limit, self.sql, self.args)
        except db.dbErr, err:
            logging.error("数据库调用异常")
            logging.error(err)
            return False

        return True


    def call3(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            self.cur = db.query3(self.pagenum, self.pagelen, self.sql, self.args)
        except db.dbErr, err:
            logging.error("数据库调用异常")
            logging.error(err)
            return False

        return True


    def getCursorResult(self):
        self.resultCUR[ 'cur' ] = Common.CursorToDict( self.cur )

        return self.resultCUR



class execute( tool ):
    def __init__( self ):
        tool.__init__( self )
        self.rowcount = 0

    def call(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            self.rowcount = db.execute(self.sql, self.args)
            db.commit()
        except db.dbErr, err:
            logging.error("数据库调用异常")
            logging.error(err)
            db.rollback()
            return False

        return True

    def getNumberResult(self):
        self.resultNUM[ 'rowcount' ] = self.rowcount

        return self.resultNUM













#####################
#                   #
#   下面是调用函数  #
#                   #
#####################
def callexecute(dbname, sql, args):
    # 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = execute()

    # 调用通用的流程
    ret = commonCall( db, resp, dbname, sql, args )
    if not ret:
        return resp

    return resp




def callquery(dbname, sql, args, skip=0, limit=1000):
    # 1. 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = query()

    # 调用通用的流程
    ret = commonCall( db, resp, dbname, sql, args, skip, limit )
    if not ret:
        return resp

    return resp


# 分页
def callquery3(dbname, sql, args, pagenum=0, pagelen=1000):
    # 1. 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = query()

    # 调用通用的流程
    ret = commonCall3( db, resp, dbname, sql, args, pagenum, pagelen )
    if not ret:
        return resp

    return resp

def callproc(dbname, sql, args, skip=0, limit=1000):
    # 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = procedure()

    # 调用通用的流程
    ret = commonCall( db, resp, dbname, sql, args, skip, limit )
    if not ret:
        return resp

    return resp

# 分页
def callproc3(dbname, sql, args, pagenum=0, pagelen=1000):
    # 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = procedure()

    # 调用通用的流程
    ret = commonCall3( db, resp, dbname, sql, args, pagenum, pagelen )
    if not ret:
        return resp

    return resp



def callfunc(dbname, sql, args, rettype=Config._NUM_ORA_TYPE_):
    # 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = function()

    # 设置存储过程返回值类型
    db.setReturnType( rettype )

    # 调用通用的流程
    ret = commonCall( db, resp, dbname, sql, args )
    if not ret:
        return resp

    # 填充到返回值
    ret = db.getReturnVal()
    resp.setCode( str(int(ret)) )

    return resp


def commonCall( db, resp, dbname, sql, args, skip=0, limit=1000 ):
    # 1. 设置数据库
    db.setDB( dbname )

    # 2. 设置SQL
    db.setSQL( sql )

    # 3. 设置参数
    db.setArgs( args )

    # 4. 设置查询区间
    db.setRange( skip, limit )

    # 5. 执行
    ret = db.call()
    if not ret:
        logging.error( "执行失败，入参[%s]"%args )
        resp.setCode( Config.RET_SYSTEM_ERROR )
        resp.setMsg( "执行失败" )
        return False

    # 6. 取结果
    respNUM = db.getNumberResult()
    respSTR = db.getStringResult()
    respCUR = db.getCursorResult()

    # 7. 填充到返回值
    resp.setInfo( respNUM )
    resp.setInfo( respSTR )
    for key, LIST in respCUR.items():
        resp.setList( LIST )
        break

    return True


# 分页
def commonCall3( db, resp, dbname, sql, args, pagenum=0, pagelen=1000 ):
    # 1. 设置数据库
    db.setDB( dbname )

    # 2. 设置SQL
    db.setSQL( sql )

    # 3. 设置参数
    db.setArgs( args )

    # 4. 设置分页
    db.setPage( pagenum, pagelen )

    # 5. 执行
    ret = db.call3()
    if not ret:
        logging.error( "执行失败，入参[%s]"%args )
        resp.setCode( Config.RET_SYSTEM_ERROR )
        resp.setMsg( "执行失败" )
        return False

    # 6. 取结果
    respNUM = db.getNumberResult()
    respSTR = db.getStringResult()
    respCUR = db.getCursorResult()

    # 7. 填充到返回值
    resp.setInfo( respNUM )
    resp.setInfo( respSTR )
    for key, LIST in respCUR.items():
        resp.setList( LIST )
        break

    return True

