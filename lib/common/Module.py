#!/usr/bin/env python
#coding=utf-8

import sys
import os
import logging
import traceback
import time
import types
import imp
import inspect
import pkgutil
import Common

_BASIC_PATH_ = os.environ['_BASIC_PATH_']



#
# 功能：取指定模块的成员函数列表
# 编写：chend@500wan.com
# 时间：2011-05-17 11:04
# 入参：
#       模块
#       关键字前缀  默认为空
#       分隔符      默认为下划线
# 出参：
#       字典 = { key :  {   'method' : 函数,
#                           'issoa'  : True or False,
#                       },
#                ......
#              }
#              其中key = 关键字前缀 + 分隔符 + 函数名
#
def getModuleMethodList( module, prefix="", separator="_" ):
    methodList = {}
    try:
        objectList = filter(lambda x:x[0] != '_', dir(module))  # 取模块里面第一个字母不为'_'的成员名列表

        for objectName in objectList:
            _object = eval( "module." + objectName )            # 取到具体的成员对象
            
            if types.FunctionType == type(_object) :            # 如果这个成员类型是函数，则登记
                if '<__NOT_INTERFACE__>' == _object.__doc__:
                    continue
                if module.__dict__.has_key('__THIS_IS_OLD__'):
                    issoa = True
                else:
                    issoa = False
                name = prefix + separator + objectName
                name = Common.mb_strtolower( name )             # 转小写
                methodList[ name ] = {
                    'method' : _object,
                    'issoa'  : issoa,
                }
                
                name_nonprefixal = Common.mb_strtolower( objectName )
                methodList[ name_nonprefixal ] = {
                    'method' : _object,
                    'issoa'  : issoa,
                }

    except:
        err = traceback.format_exc()
        logging.error("取指定模块的成员函数列表[%s]失败。%s", module, err)

    return methodList



# 功能：取模块，如果发现模块文件被更新，则重新加载模块
def load_module(path, name=''):
    '''<__NOT_INTERFACE__>'''
    if '' == name:
        name = path
    name = name.replace('/', '_')
    # 取模块
    if not sys.modules.has_key( name ):
        try:
            modFile = '%s/%s.py'%(_BASIC_PATH_, path)
            module = imp.load_source( name,  modFile )            # 加载该模块
        except:
            err = traceback.format_exc()
            logging.error("加载模块[%s]失败。%s", path, err)
            module = None
    else:
        module = sys.modules[ name ]

    # 取模块失败则返回
    if not module:
        return module

    # 检查模块文件是否更新
    if False:#module.__dict__.has_key( '_LOAD_TIME_' ):

        filename = module.__file__
        suffix   = filename[filename.rfind('.')+1:]
        mtime    = 0
        mtime_py = 0

        if os.path.isfile(filename):
            mtime =  os.path.getmtime( filename )
        # 如果后缀是pyc，则再检查相应的py文件
        if 'pyc' == suffix :
            filename_py = filename.replace('.pyc', '.py')
            if os.path.isfile(filename_py):
                mtime_py =  os.path.getmtime( filename_py )

        if mtime > module._LOAD_TIME_ or mtime_py > module._LOAD_TIME_:
            logging.info( "模块[%s]有更新，重载之"%module.__name__ )
            Reload( module )
            module._LOAD_TIME_ = time.time()

    else:
        module._LOAD_TIME_ = time.time()

    return module

def invoke_func(func, params, argspec = None):
    if not argspec:
        argspec = inspect.getargspec(func)

    # match args
    if set(params) - set(argspec[0]):
        # more args then require
        for i in set(params) - set(argspec[0]):
            params.pop(i)

    return func(**params)

class Modules(object):
    IGNORE_INTERFACE = ('start', 'stop', 'reg_interface')
    def __init__(self):
        self.interfaces = dict()
        self.modules = dict()
        self.hooks = {
            'start': dict(),
            'stop': dict(),
        }

    def load(self, module):
        '''load module recursively'''
        logging.info("loading module %s" % module)
        ld = pkgutil.find_loader(module)
        mod = ld.load_module(module)

        if hasattr(mod, 'start'): # 执行start方法
            if type(getattr(mod, 'start')) == types.FunctionType:
                if not self.hooks['start'].has_key(module):
                    getattr(mod, 'start')()
                self.hooks['start'][module] = 1

        if hasattr(mod, 'stop'): # 缓存stop方法
            self.hooks['stop'][module] = getattr(mod, 'stop')

        if not hasattr(mod, 'reg_interface'):
            minfo = mod.__dict__
        else:
            minfo = getattr(mod, 'reg_interface')(module)
        for mname, mfunc in minfo.items():
            if mname in self.IGNORE_INTERFACE:
                continue
            if self.interfaces.has_key(mname):
                logging.error('server error: interface %s existed!' % mname)
            if type(mfunc) == types.FunctionType:
                self.interfaces[mname] = {
                    'funcinfo': inspect.getargspec(mfunc),
                    'function': mfunc,
                    'funcdoc': mfunc.__doc__
                }

        if ld.is_package(module):
            importer = pkgutil.ImpImporter(ld.filename)
            for name, ispkg in importer.iter_modules():
                self.load("%s.%s" % (module, name))
        self.modules[module] = mod
        return mod

    def load_module(self, mod):
        try:
            if not self.load(mod):
                logging.error('Model(%s) load failure!' % mod)
                return
        except:
            logging.error(traceback.format_exc())

    def invoke(self, cmdId, req):
        '''run the interface'''
        if not self.interfaces.has_key(cmdId): # 方法不存在
            logging.error('Method %s not found!' % cmdId)
            return
        startime = time.time()
        ret = invoke_func(self.interfaces[cmdId]['function'], req, self.interfaces[cmdId]['funcinfo'])
        costtime = time.time() - startime
        logging.info('invoke %s success, cost %.3f microseconds' % (cmdId, costtime * 1000))
        return ret

    def __del__(self):
        for name, stop_func in self.hooks['stop'].items():
            stop_func()
