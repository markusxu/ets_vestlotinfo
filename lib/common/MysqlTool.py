#coding=utf-8
import sys, traceback, time
import logging
import DbPool
import Common
import Config
import json



class tool:

    def setDB( self, dbname ):
        self.dbname = dbname
        return True

    def setSQL( self, sql ):
        sql = Common.setLabel( sql )
        self.sql = sql
        return True

    def setArgs(self, args):
        self.args = args

        return True

    def call(self):
        logging.error( '错误的调用：父类' )
        return False


    def getNumberResult(self):
        return self.resultNUM


    def getCursorResult(self):

        return Common.CursorToDict( self.resultCUR )


    def __init__( self ):
        self.dbname = ''
        self.sql    = ''
        self.args   = None
        self.resultNUM = {}
        self.resultCUR = []




class query( tool ):

    def call(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            self.resultCUR = db.query(self.sql, self.args)
        except :
            logging.error("数据库调用异常")
            logging.error( traceback.format_exc() )
            return False

        return True




class execute( tool ):
    def __init__( self ):
        tool.__init__( self )
        self.rowcount = 0

    def call(self):
        session = DbPool.DbSession( self.dbname )
        db = session.getConn()
        if None == db:
            logging.error( "取数据库连接失败" )
            return False

        try:
            self.rowcount = db.execute(self.sql, self.args)
            db.commit()
        except :
            logging.error("数据库调用异常")
            logging.error( traceback.format_exc() )
            db.rollback()
            return False

        return True

    def getNumberResult(self):
        self.resultNUM[ 'rowcount' ] = self.rowcount

        return self.resultNUM













#####################
#                   #
#   下面是调用函数  #
#                   #
#####################
def callexecute(dbname, sql, args):
    # 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = execute()

    # 调用通用的流程
    ret = commonCall( db, resp, dbname, sql, args )
    if not ret:
        return resp

    return resp



def callquery(dbname, sql, args):
    # 1. 实例化一个结果类、一个处理类
    resp = Common.Result()
    db   = query()

    # 调用通用的流程
    ret = commonCall( db, resp, dbname, sql, args )
    if not ret:
        return resp

    return resp


def commonCall( db, resp, dbname, sql, args ):
    # 1. 设置数据库
    db.setDB( dbname )

    # 2. 设置SQL
    db.setSQL( sql )

    # 3. 设置参数
    db.setArgs( args )

    # 4. 执行
    ret = db.call()
    if not ret:
        logging.error( "执行失败，入参:" )
        logging.error( args )
        resp.setCode( Config.RET_SYSTEM_ERROR )
        resp.setMsg( "执行失败" )
        return False

    # 5. 取结果
    respNUM = db.getNumberResult()
    respCUR = db.getCursorResult()

    # 6. 填充到返回值
    resp.setInfo( respNUM )
    resp.setList( respCUR )

    return True
