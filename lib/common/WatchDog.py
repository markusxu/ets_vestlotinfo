#coding=utf-8
# -------------------------------------
# 模块名称: 线程闲忙监控
# 用    途: god knows
# 作    者: chend
# 创建时间: 2011-03-24
# 修改记录:
# -------------------------------------


import sys
import logging
import time
import threading
import XmlConfig
import Common
import traceback
#
#   _THREADS_STORE_ 保存线程的闲忙信息，基本结构如下
#
#   _THREADS_STORE_ = {
#                       线程ID : {
#                                   'activetime' : 开始工作时间,
#                                   'isbusy' : 是否忙，True代表忙，False代表不忙,
#                                   'key'    : 关键字（ID）,
#                       }
#                       ......
#                     }
#
_THREADS_STORE_   = {}
_CHECK_INTERVAL_  = 10
_TIMEOUT_         = 10
_watcher_lock_    = threading.Lock()
_WARN_TOPIC_      = 'TradeLogicWarn'
_TIMEOUT_INVOKE_  = {}

# 自动更新线程的信息
class autoFeeder:
    def __init__(self, tid, key=''):
        global _THREADS_STORE_
        self.tid = str(tid)
        self.key = key
        if _THREADS_STORE_.has_key( self.tid ):
            updateTidInfo( self.tid, True, self.key )

    def __del__(self):
        global _THREADS_STORE_
        if _THREADS_STORE_.has_key( self.tid  ):
            updateTidInfo( self.tid, False, self.key )

def init( WatchDogInterval, WatchDogTimeout, WatchDogClose, WatchDogWarnTopic = 'TradeLogicWarn'):
    v_close = WatchDogClose

    if None == v_close :
        logging.info('线程闲忙监控没有配置，退出监控')
        return False

    if '0' != v_close:
        logging.info('线程闲忙监控没有打开，退出监控')
        return True

    global _CHECK_INTERVAL_
    global _TIMEOUT_
    global _WARN_TOPIC_
    _CHECK_INTERVAL_ = int( WatchDogInterval )
    _TIMEOUT_ = int( WatchDogTimeout )
    _WARN_TOPIC_ = WatchDogWarnTopic

    return True

def addTID( tid ):
    global _THREADS_STORE_

    lol = Common.LOCK( _watcher_lock_ )

    TID = str(tid)
    if not _THREADS_STORE_.has_key( TID ):
        _THREADS_STORE_[ TID ] = {}
        _THREADS_STORE_[ TID ][ 'activetime' ] = Common.now()
        _THREADS_STORE_[ TID ][ 'isbusy' ] = False
        logging.debug( "线程监控新增[tid:%s] _THREADS_STORE_ = %s", TID, _THREADS_STORE_ )

    return True

def delTID( tid ):
    global _THREADS_STORE_

    lol = Common.LOCK( _watcher_lock_ )

    TID = str(tid)
    if _THREADS_STORE_.has_key( TID ):
        del _THREADS_STORE_[ TID ]
        logging.debug( "线程监控剔除[tid:%s] _THREADS_STORE_ = %s", TID, _THREADS_STORE_ )

    return True

def updateTidInfo( tid, state, key='' ):
    global _THREADS_STORE_

    lol = Common.LOCK( _watcher_lock_ )

    if _THREADS_STORE_.has_key( tid ):
        _THREADS_STORE_[ tid ][ 'activetime' ] = Common.now()
        _THREADS_STORE_[ tid ][ 'isbusy' ] = state
        _THREADS_STORE_[ tid ][ 'key' ] = key

    return True


def run():
    global _THREADS_STORE_
    global _TIMEOUT_
    global _WARN_TOPIC_
    global _TIMEOUT_INVOKE_
    while 1:
        time.sleep( _CHECK_INTERVAL_ )
        logging.debug(_TIMEOUT_INVOKE_)
        try:
            nBusy = 0
            nNotBusy = 0
            for tid, info in _THREADS_STORE_.items():
                logging.debug(info)
                activetime = info[ 'activetime' ]
                isbusy     = info[ 'isbusy' ]
                key        = info.get('key', '')
                if True == isbusy:
                    nBusy = nBusy + 1
                    tNow = Common.now()
                    diff = Common.compTwoTimes( tNow, activetime )
                    if diff > _TIMEOUT_:
                        content = "线程执行时间过长[id:%s]"%str(key)
                        logging.error(content)
                        
                        # 一个线程最近3分钟内超过3次则发送告警
                        if not _TIMEOUT_INVOKE_.has_key(key):
                            logging.info("has key")
                            _TIMEOUT_INVOKE_[key] = {'time':time.time(),'cnt':1}
                        else:
                            _TIMEOUT_INVOKE_[key]['cnt'] += 1                    
                        if time.time() - _TIMEOUT_INVOKE_[key]['time'] <180:
                            if _TIMEOUT_INVOKE_[key]['cnt'] >3:
                                try:
                                    import esunpublisher
                                    msg = esunpublisher.connect(_WARN_TOPIC_)
                                    msg.sendWarn( _WARN_TOPIC_, content,'' )
                                    del _TIMEOUT_INVOKE_[key]
                                except:
                                    del _TIMEOUT_INVOKE_[key]
                                    
                            else:
                                pass
                        else:
                            del _TIMEOUT_INVOKE_[key]                   
                else :
                    nNotBusy = nNotBusy + 1
                
                if _TIMEOUT_INVOKE_.has_key(key) and time.time() - _TIMEOUT_INVOKE_[key]['time'] >180:
                    del _TIMEOUT_INVOKE_[key]
        except:
            logging.error(traceback.format_exc())

        #logging.info( "线程监控当前状态：在忙[%s]个，在闲[%s]个", nBusy, nNotBusy )

        if nNotBusy <=0 :
            try:
                content = '空闲进程为0'
                logging.error(content)
                import esunpublisher
                msg = esunpublisher.connect(_WARN_TOPIC_)
                msg.sendWarn( _WARN_TOPIC_, content,'' )
            except:
                pass
