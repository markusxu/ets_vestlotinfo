#coding=utf-8

import os
import sys
import socket
import logging
import traceback

import XmlConfig
import HttpClient

gServiceHost = {'fire':'http://192.168.41.160:10501/','ihsquery':'http://192.168.41.160:55555'}
gService = ''   
gModuleList = [] 
gFrame = ''

gIp = ''
gProcess = ''
try:
    """ 获取本机ip信息 """
    gIp = socket.getfqdn(socket.gethostname())
except:
    logging.info('no ip')

try:
    """获取框架配置信息信息"""
    frameList = sys.argv[0].split('/')
    gProcess = sys.argv[1]
    gFrame = frameList[-1].split('.')[0]
    serviceList = frameList[-3].split('_')
    gService = frameList[-3]
    if len(serviceList) >= 2:
        gService = gFrame + '_' + serviceList[-1]
    XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + '/etc/' + gFrame +'.xml')
    
    if gFrame and gFrame in ['eas','ets']:
        configStr = '/' + gFrame + '/applications/'    
        serviceList = XmlConfig.list(configStr)
        
        moduleList = []
        for k,v in serviceList.items():
            processList = []
            if isinstance(v, list):
                for sl in v:
                    path = sl.get('path', '')
                    if path:
                        path = path.replace('.', '/')
                        processList.append(path)
                gModuleList += processList
            if isinstance(v, dict):
                path = v.get('path', '')
                if path:
                    path = path.replace('.', '/')
                    processList.append(path)
                gModuleList += processList
        gModuleList = list(set(gModuleList))
except:
    logging.info('[system view]no config')
    
try:
    XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + '/etc/service.xml')
    host = XmlConfig.list('/service/http_host/')
    if host and isinstance(host,dict):
        gServiceHost.update(host)
except:
    logging.info('[system view]no http config')
    
def getServiceName():
    """ 获取当前服务名和接口名 """
    global gModuleList
    global gService
    service = ''
    method = ''
    try:
        service = gService
        moduleList = traceback.extract_stack()
        for v in gModuleList: 
            for module in moduleList:
                if v in module[0]:
                    method = module[2].lower()
                    break
    except:
        logging.debug('no service name')
        
    ret = {
            'service' : service,
            'func' : method,
        }
    return ret
    
def serviceDataCallback():
    """ 返回服务相关信息 """
    data = {}
    try:
        global gFrame
        global gIp
        global gProcess
        sret = getServiceName()
        source = sret['service'] + '/' + str(gProcess) + '/' + sret['func']
        data ={
                'from':str(gFrame), 
                'cmd':" ".join(sys.argv), 
                'remote_addr' : str(gIp), 
                'uuid' : '', 
                'usercheck' : '', 
                'cdn-src-ip' : '', 
                'useragent' : '', 
                'source':source,
        }
    except:
        logging.debug('no service data callback')
    return data
    
def httpLogCallback(args):
    """ 服务间http调用信息打印 """
    try:
        global gServiceHost
        sret = getServiceName()
        service = 'none_service'
        for k,v in gServiceHost.items():
            if args[0].find(v) >= 0:
                service = k
        logging.info('''[system viewer] [dest_service:%s] [dest_func:%s] [ret:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]'''%(service, 'ets_http', args[1], args[2], sret['service'], sret['func']))
    except:
        logging.debug('no decoration log')
        
HttpClient.logCallback = httpLogCallback
HttpClient.getData = serviceDataCallback
