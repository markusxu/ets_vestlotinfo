#coding=utf-8
import os
import zkclient as zk
from zkclient import watchmethod
import logging
import threading
import XmlConfig
import time

try:
    XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + '/etc/service.xml')
except:
    pass

_ZK_ADDRESS_ = XmlConfig.get('/service/zookeeper/address', '192.168.0.237:2181')
_TIME_OUT_   = int( XmlConfig.get('/service/zookeeper/time_out', '2000') )
_WORK_PATH_  = XmlConfig.get('/service/zookeeper/work_path', '/server/xysc')
_PREFIX_     = XmlConfig.get('/service/zookeeper/prefix', 'proc')

_EVENT_MASTER_ = threading.Event()

class watcher:

    def __init__(self, appid, zk_address, time_out, work_path, prefix):
        self.appid      = appid
        self.zk_address = zk_address
        self.time_out   = time_out
        self.work_path  = work_path + '/' + appid
        self.prefix     = prefix
        self.is_master  = False

        self.zkcli      = self._connect( zk_address, time_out )

    def _connect(self, address, timeout):
         return zk.ZKClient( address, timeout )
    
    
    def _init_work_path(self, path):
    
        # check the format of path
        path = path.strip()
        if len(path) <=0 or path[0] != '/':
            logging.error('[zookeeper]wrong format of path [%s]'%path)
            return False
        path_split  = path.split('/')
        path_split.remove( '' )
    
        # if path not exist, then create it
        path_tmp = ""
        for i in range(len(path_split)):
            path_tmp = path_tmp + '/' + path_split[i]
            if not self.zkcli.exists( path_tmp ):
                logging.error( '[zookeeper][%s]not exist'%path_tmp )
                self.zkcli.create( path_tmp, "" )
            else:
                logging.info( '[zookeeper][%s]exist'%path_tmp )
    
        return True
    

    def _is_master(self):

        @watchmethod
        def watchfunc(event):
            logging.info( '[zookeeper]get event' )
            logging.info( event )
            if not self.is_master:
                if self._is_master():
                    logging.info( "i am master" )
                    _EVENT_MASTER_.set()

        # get children
        children = self.zkcli.get_children( self.work_path, watchfunc )
        logging.info( "[zookeeper]get children[%s]"%children )
        children.sort()
    
        # check who master is
        if self.node_name in children[:1]:
            self.is_master = True
            return True    

        return False

    def register(self, _END_EVENT_):
        work_path = self.work_path
        node_path = work_path + "/" + self.prefix
    
        # init work path
        self._init_work_path( work_path )
    
        # create my own path
        create_path = self.zkcli.create( node_path , "1", flags=zk.zookeeper.EPHEMERAL | zk.zookeeper.SEQUENCE)
        create_child = create_path.split('/')
        self.node_name = create_child[-1]
        logging.info( "[zookeeper]create path[%s]"%create_path )
    
        if self._is_master():
            logging.info( "i am master" )
        else:
            while not _END_EVENT_.isSet():
                time.sleep(1)       # 做了一个比较笨拙的办法，每一秒检查一下是否是主节点
                if self.is_master:
                    return True
        return False


def register(appid, _END_EVENT_):

    w = watcher( appid, _ZK_ADDRESS_, _TIME_OUT_, _WORK_PATH_, _PREFIX_ )
    return w.register(_END_EVENT_)



if __name__ == '__main__':
    print "just test"


