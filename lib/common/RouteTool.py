#coding=utf-8
import logging
import sys
import os
import traceback
import time
import JsonUtil
import esuneas

_TABLES_ = ["B_USERINFO", "B_EXPUSERINFO", "T_USERPAYLOG", "T_USERPAYLOG_HIS", "T_LOTTERY_ORDER",
"T_USERTMPPAY", "T_USERTMPPAY_BAK", "T_CPLOGINFO"]
_TABLE2DB_ = {
    "B_USERINFO"        :   "debao",
    "B_EXPUSERINFO"     :   "debao",
    "T_USERPAYLOG"      :   "debao",
    "T_USERPAYLOG_HIS"  :   "debao",
    "T_USERTMPPAY"      :   "debao",
    "T_USERTMPPAY_BAK"  :   "debao",
    "T_LOTTERY_ORDER"   :   "boss",
    "T_CPLOGINFO"       :   "as"
}
_TABLE2COUNTS_ = {
    "B_USERINFO"        :   100,
    "B_EXPUSERINFO"     :   100,
    "T_USERPAYLOG"      :   100,
    "T_USERPAYLOG_HIS"  :   100,
    "T_USERTMPPAY"      :   100,
    "T_USERTMPPAY_BAK"  :   100,
    "T_LOTTERY_ORDER"   :   100,
    "T_CPLOGINFO"       :   100
}

class SqlScanner:

    def __init__(self, sql):
        self.sql = sql
        self.dbname = "NULL"
        self.counts = 100
        self.tables = []
        self.scan_sql()

    def scan_sql(self):
        '''扫描sql
            生成：格式化后的sql、对应的分表别名(list)、数据库别名(string)
        '''
        SQL = self.sql.replace('\n', ' ').replace('\t', ' ')
        SQL = SQL + ' '
        SQL_upper = SQL.upper()

        tablename2flags = {}
        for t in _TABLES_:
        
            # 找出关键字所在的位置
            tmp = ' ' + t + ' '
            SQL_split_tmp = SQL_upper.split(tmp)
            if len(SQL_split_tmp) <= 1:
                continue
            
            # 在SQL里面找到了关键字：分表的别名
            self.dbname = _TABLE2DB_[t]
            self.counts = _TABLE2COUNTS_[t]
            self.tables.append(t)

            flag = 0
            flags = []
            for i in range( len(SQL_split_tmp) - 1 ):
                if i == 0:
                    flag = len( SQL_split_tmp[i] )
                else:
                    flag = flag + len( tmp ) + len( SQL_split_tmp[i] )
                flags.append(flag)

            tablename2flags[tmp] = flags # 记录关键字出现的位置

        # 把源sql里面的分表表名，替换成大写的表名
        for tmp, flags in tablename2flags.items():
            for i in flags:
                SQL = SQL[:i] + tmp + SQL[i+len(tmp):]

        self.sql = SQL
    
    def COUNTS(self):
        return self.counts

    def DBNAME(self):
        return self.dbname
        
    def SQL(self, NO):
        if NO > self.counts - 1 and NO < 0:
            return None

        newsql = self.sql
        for table in self.tables:
            newtable = table + "_" + str(NO)
            newsql = newsql.replace(" "+table+" ", " "+newtable+" ")
            
        return newsql 


def _smart_select(sql, args, expinfo={}, inOneTable=False):
    
    logging.info("_smart_select begin")
    
    sqlS = SqlScanner(sql)
    
    GROUP_KEYS = expinfo.get( "GROUP" ) # 解析sql里面group by的字段有哪些
    ORDER_KEYS = expinfo.get( "ORDER" )
    MIN_KEYS   = expinfo.get( "MIN" )
    MAX_KEYS   = expinfo.get( "MAX" )
    SUM_KEYS   = expinfo.get( "SUM" )
    CNT_KEYS   = expinfo.get( "COUNT" )
    dbname     = sqlS.DBNAME()
    counts     = sqlS.COUNTS()
    if not dbname:
        logging.error("error dbname. sql:%s", sql)
        return []

    resp = []
    if len(GROUP_KEYS) > 0 or len(MIN_KEYS) > 0 or\
        len(MAX_KEYS) > 0 or len(SUM_KEYS) > 0 or len(CNT_KEYS) > 0:
        
        # 逐个表进行查询: 异步调用多进程
        easResp = []
        EasObj = esuneas.connect('usercpu')  # eas连接对象
        for i in range( counts ):
            SQL = sqlS.SQL(i)
            easReq = args
            easReq["sql"] = SQL
            easReq["dbname"] = dbname

            retEas = EasObj.invokeAsync('search', easReq)
            easResp.append(retEas)
        
        # 逐个表进行查询：异步获取多进程返回结果
        RET_ALL_TABLE = {}
        for i in range( counts ):
            
            curs = easResp[i].getResult()
            
            if curs[0] < 0 or not curs[1].has_key("result"):
                logging.error("第[%s]个查询失败: %s", i, curs)
                return []

            curs = JsonUtil.read( curs[1]["result"] )
            if len(curs) <= 0:
                continue

            RET_ONE_TABLE = {}
            for row in curs:
                
                # group by 处理
                key = ""
                if len(GROUP_KEYS) > 0:
                    for k in GROUP_KEYS:
                        key = key + str(row[k]) + "@"
                else:
                    key = "single"

                RET_ONE_TABLE[key] = row

            for k, v in RET_ONE_TABLE.items():
                if not RET_ALL_TABLE.has_key(k):
                    RET_ALL_TABLE[k] = v
                else:
                    # 取出已有的数据进行合并
                    for key in RET_ALL_TABLE[k].keys():
                        if key in MIN_KEYS:
                            if RET_ALL_TABLE[k][key] > RET_ONE_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = RET_ONE_TABLE[k][key]
                        elif key in MAX_KEYS:
                            if RET_ALL_TABLE[k][key] < RET_ONE_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = RET_ONE_TABLE[k][key]
                        elif key in SUM_KEYS:
                            if not RET_ALL_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = 0
                            #查询结果是None
                            if RET_ONE_TABLE[k][key] == None:
                                RET_ONE_TABLE[k][key] = 0

                            tmp =  RET_ALL_TABLE[k][key] + RET_ONE_TABLE[k][key]
                            RET_ALL_TABLE[k][key] = tmp
                        elif key in CNT_KEYS:
                            if not RET_ALL_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = 0
                            RET_ALL_TABLE[k][key] += RET_ONE_TABLE[k][key]
        #logging.debug( RET_ALL_TABLE )
        
        for k, v in RET_ALL_TABLE.items():
            resp.append(v)
            
    else:
        # 逐个表进行查询: 异步调用多进程
        easResp = []
        EasObj = esuneas.connect('usercpu')  # eas连接对象
        for i in range( counts ):
            SQL = sqlS.SQL(i)
            easReq = args
            easReq["sql"] = SQL
            easReq["dbname"] = dbname

            retEas = EasObj.invokeAsync('search', easReq)
            easResp.append(retEas)
        
        # 逐个表进行查询：异步获取多进程返回结果
        for i in range( counts ):
            curs = easResp[i].getResult()
            
            if curs[0] < 0 or not curs[1].has_key("result"):
                logging.error("第[%s]个查询失败: %s", i, curs)
                return []

            curs = JsonUtil.read( curs[1]["result"] )
            if len(curs) <= 0:
                continue

            resp.extend(curs)

            if inOneTable == True:
                # 如果明确指出数据只会存在一张表里面，那么这里直接退出
                break

    # 排序
    for order_key in ORDER_KEYS:
        key_name = order_key[0]
        order_type = order_key[1]
        if order_type == "DESC":
            resp = sorted(resp, key=lambda x:x[key_name], reverse=True)
        elif order_type == "ASC":
            resp = sorted(resp, key=lambda x:x[key_name])

    return resp


def search(sql, args={}, order=[], group=[],\
    min=[], max=[], sum=[], count=[], inOneTable=False):
    """ 分表查询的手动版本的接口 """
    """
        例:
            SELECT   COUNT (*) AS cntrow, SUM (ui_usermoney) AS paymoney,
                     ui_hezuotype hezuotype
                FROM b_userinfo_[r]
               WHERE ui_username LIKE '%test%'
            GROUP BY ui_hezuotype
            order by ui_hezuotype desc

        group = ["hezuotype"]
        count = ["cntrow"]
        sum   = ["paymoney"]
        order = [("hezuotype", "asc")]
    """

    try:

        # 入参格式化
        expinfo = {"GROUP":[], "ORDER":[], "MIN":[], "MAX":[], "COUNT":[], "SUM":[]}
        ORDER = []
        for i in order:
            ORDER.append( (i[0].upper(), i[1].upper()) )
        expinfo["ORDER"] = ORDER
        
        GROUP = []
        for i in group:
            GROUP.append( i.upper() )
        expinfo["GROUP"] = GROUP
        
        MIN = []
        for i in min:
            MIN.append( i.upper() )
        expinfo["MIN"] = MIN
        
        MAX = []
        for i in max:
            MAX.append( i.upper() )
        expinfo["MAX"] = MAX
        
        COUNT = []
        for i in count:
            COUNT.append( i.upper() )
        expinfo["COUNT"] = COUNT

        SUM = []
        for i in sum:
            SUM.append( i.upper() )
        expinfo["SUM"] = SUM

        ARGS = {}
        for k,v in args.items():
            ARGS[k] = str(v)
        
        retList = _smart_select(sql, ARGS, expinfo, inOneTable)
        for ret in retList:
            for k in ret.keys():
                ret[k] = str(ret[k])
        
        return retList
    
    except:
        logging.error(traceback.format_exc())
        logging.error("函数异常")
        return []
    return []

