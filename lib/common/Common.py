#coding=utf-8

import os
import re
import sys
import time
import json
import socket
import struct
import signal
import hashlib
import inspect
import logging
import datetime
import threading
import traceback

import fcntl
import Encrypt

__NO_DEFAULT_DATA__ = "<NO_DEFAULT_DATA>"
__MUTEX_LOCK__ = threading.RLock()
################################################################

#获取默认昵称
def getDefaultNickname(username):
    nickname = username
    try:
        if re.match("^\d+$", username) or re.search("\d{6}",username) or re.match(r"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$", username):
            randstr = hashlib.md5(username).hexdigest()
            nickname = '500cpw_'+randstr[0:9]
    except:
        logging.error(traceback.format_exc())

    return nickname

#
#  入参：
#       strtime 类型string 形式如"2010-11-03 19:54:11"
#  出参：
#       strtime与当前系统时间的相差，单位 秒
#
def compTime(strtime):
     tTmp = time.strptime(strtime,"%Y-%m-%d %H:%M:%S")
     tTmp = datetime.datetime( *tTmp[:6] )
     now = datetime.datetime.now()
     comptime=tTmp-now
     if comptime.days ==0 :
        return comptime.days*720+comptime.seconds
     else:
        return comptime.days*86400+comptime.seconds
#
#  入参：
#       strtime 类型string 形式如"2010-11-03 19:54:11"
#  出参：
#       strtime与当前系统时间的相差，单位为天
#
def compDays(strtime):

    tTmp = strtime
    now  = datetime.datetime.now()
    compDays = (now - tTmp).days
    return compDays

def time_strptime( strtime, format ):
    with __MUTEX_LOCK__:
        return time.strptime(strtime,format)

def datetime_strptime( strtime, format ):
    with __MUTEX_LOCK__:
        return datetime.datetime.strptime(strtime,format)

def compTwoTimes( endtime, nowtime ):
     import imp
     imp.acquire_lock()
     tEnd = time.strptime(endtime,"%Y-%m-%d %H:%M:%S")
     tEnd = datetime.datetime( *tEnd[:6] )
     tNow = time.strptime(nowtime,"%Y-%m-%d %H:%M:%S")
     tNow = datetime.datetime( *tNow[:6] )
     imp.release_lock()

     comptime=tEnd-tNow
     if comptime.days ==0 :
        return comptime.days*720+comptime.seconds
     else:
        return comptime.days*86400+comptime.seconds

def addSeconds( tEnd, tAdd, flag=False ):
    if len(tEnd.split(':')) == 2:
        tEnd = tEnd + ':00'

    tTmp = time.strptime(tEnd,"%Y-%m-%d %H:%M:%S")
    tTmp = datetime.datetime( *tTmp[:6] )
    tNew = str(tTmp + datetime.timedelta( seconds=tAdd ))[:19]
    return tNew

#取系统当前时间
def now():
    return time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
def now2ms():
    timestamp = time.time()
    resp= {
       'timestamp' : timestamp,
       'datetime'  : time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(timestamp)),

    }
    return resp

#把字典所有的value值转化为str类型
def mapMembers2str( oldMap ):
    newMap = {}
    for key, value in oldMap.items():
        if type(value) == float:
            #newMap[ key ] = ("%0.6f"%value)[:-4]
            newMap[ key ] = "%s"%value
        elif type(value) == unicode:
            newMap[ key ] = value.encode('GB18030')
        else:
            newMap[ key ] = str( value )
        if None == value:
            newMap[ key ] = ''
    return newMap

#字典转化为字符串，主要用于输出中文
def param2string( param ):
    s = ''
    for key, value in  param.items():
        s = s + "[" + str(key) + ":" + str(value) + "]"

    return s

#列表转化为字符串，主要用于输出中文
def list2string( _list ):
    s = ''
    for i, param in enumerate(_list):
        s = s + '{'
        for key, value in  param.items():
            s = s + "[" + str(key) + ":" + str(value) + "]"
        s = s + '} '
    return s

#文件操作
#file_name   文件名
#record_date 记录数据
#suffix      文件后缀
#说明：此函数只处理隐藏文件记录'w'单条数据
def record_file(file_name,record_data,suffix):
    if os.path.exists('.'+file_name+'.'+suffix):
          f=open('.'+file_name+'.'+suffix,'w')
    else:
          f=open('.'+file_name+'.'+suffix,'w')
    f.write(record_data)
    f.close()


class Timer:
    def __init__(self, ID=""):
        self._beginTime = datetime.datetime.now()
        self._ID = ID
        self._addInfo = ""

    def taketimes(self):
        # 返回已经耗时
        _tmp = datetime.datetime.now() - self._beginTime
        _timeDiff =  _tmp.seconds*1000000 + _tmp.microseconds
        return _timeDiff/1000

    def setAddInfo(self, addInfo):
        self._addInfo = self._addInfo + addInfo + " "
        return True

    def __del__(self):
        _tmp = datetime.datetime.now() - self._beginTime
        _timeDiff =  _tmp.seconds*1000000 + _tmp.microseconds
        logging.info( "%s 耗时 %s 毫秒 %s", self._ID, str(_timeDiff/1000), self._addInfo )

class LOCK:
    def __init__(self, lock):
        self._lock = lock
        self._lock.acquire()
        self._isRelease = False

    def acquire(self):
        # 确保释放了锁才能申请锁
        if self._isRelease :
            self._lock.acquire()
            self._isRelease = False

    def release(self):
        self._lock.release()
        self._isRelease = True

    def __del__(self):
        if not self._isRelease :
            self._lock.release()


# 把数组里面的字典按指定key排序，然后返回
def sortArray( _array, key , rev =True ):
    _array.sort( reverse = rev,key=lambda obj:obj.get( key ) )
    return _array


# 截取数据的前num个返回
def cutArray(_array, num =10):
    _mem_array = []
    e_count = 0
    for _arr in _array:
        if e_count < num:
            _mem_array.append(_arr)
        else:
            break
        e_count += 1
    return _mem_array

#
# 把数组里面的字典按指定key排序，然后组装成字符串
# example：
#       a = [ {'p' : 10}, {'p' : 10.02}, {'p' : 9.56} ]
#
#       Common.sortArray(a, 'p')
#       '{"p":9.560000}{"p":10}{"p":10.020000}'
#
def sortArray2str( _array, key, rev = True ,num =10):
    _str = '['
    _array.sort(reverse = rev,key=lambda obj:obj.get( key ))
    e_count = 0
    for _map in _array :
        if e_count <= num:
            _str = _str + json.write( _map )+','
        else:
            break
        e_count += 1
    return _str[:-1]+']'

# 时间戳到日期的转换
def timestamp2date( stamp ):
    ltime=time.localtime( stamp )
    timeStr=time.strftime("%Y-%m-%d %H:%M:%S", ltime)

    return timeStr

# 日期到时间戳的转换
def date2timestamp( date ):
    tTmp = time.strptime(date,"%Y-%m-%d %H:%M:%S")
    dateC = datetime.datetime( *tTmp[:6] )
    timestamp=time.mktime(dateC.timetuple())

    return  timestamp


class ServiceX:

    def __init__(self, id):
        self.id = id
        self.pidFile = os.environ['_BASIC_PATH_'] + '/bin/' + id + '.pid'

    def status(self):
        if not os.path.isfile(self.pidFile):
            return 0
        f = open(self.pidFile, 'r')
        pid = f.read()
        f.close()
        fname = '/proc/' + pid + '/environ'
        if not os.path.isfile(fname):
            return 0
        if os.stat(fname)[4] != os.getuid():
            return 0
        return int(pid)

    def start(self):
        pid = os.getpid()
        dname = os.path.dirname(self.pidFile)
        if not os.path.isdir(dname):
            os.makedirs(dname, 0755)
        f = open(self.pidFile, 'w')
        f.write(str(pid))
        f.close()
        return pid

    def clear(self):
        os.remove(self.pidFile)

    def stop(self):
        pid = self.status()
        if pid == 0:
            return -1
        for i in range(40):
            os.kill(pid, signal.SIGTERM)
            time.sleep(1)
            if self.status() == 0:
                return 0
        return -2

#把字典所有的value值转化为解密后的信息
def map2decr( oldMap ):
    newMap = {}
    for key, value in oldMap.items():
        newMap[ key ] = Encrypt.decrypt( value )
    return newMap

#
# 取订单编号
# 入参 prefix   string  前缀
#      flowid   string  尾数
#      date_len   int   日期长度取右边n位
#      flowid_len int   尾数长度
# 出参 订单号 = 前缀 + 日期（date_len位） + flowid（flowid_len位，不足补0）
#
#   创建: chende  2010-12-09 12:11:00
#   修改:
#
def getOrderSn( prefix, flowid, date_len=6, flowid_len=8 ):
    flowid = str( flowid )
    flowid = flowid[-flowid_len:] # 先截取
    flowid = flowid.zfill( flowid_len )

    today = time.strftime('%Y%m%d',time.localtime(time.time()))
    today = today[-date_len:]

    orderSn = str(prefix) + today + flowid

    return orderSn

# 转大写
def mb_strtoupper( key ):
    return key.decode('GB18030').upper().encode('GB18030')

# 转小写
def mb_strtolower( key ):
    return key.decode('GB18030').lower().encode('GB18030')

def combine2maps(map1, map2):
    for key, value in map2.items():
        map1[ key ] = value

    return map1

def get_runing_info():
  try:
    raise Exception
  except:
    f = sys.exc_info()[2].tb_frame.f_back
    return {"line":f.f_lineno, "file":f.f_code.co_filename, "func":f.f_code.co_name}

def setLabel(sql):
    try:
        raise Exception
    except:
        f = sys.exc_info()[2].tb_frame.f_back
    filename = f.f_code.co_filename
    lineno = f.f_lineno
    label = "/*" + str(filename) + ":" + str(lineno) + "*/"

    return label + sql


#把字典所有的value值转化为解密后的信息
def map2decr( oldMap ):
    newMap = {}
    for key, value in oldMap.items():
        newMap[ key ] = Encrypt.decrypt( value )
    return newMap

def CursorToDict( cur ):
    LIST = []
    for rows in cur:
        row = {}
        for key in rows.keys():
            row[ key ] = rows[ key ]
        LIST.append( mapMembers2str(row) )

    return LIST

def invokeAutoMatchArgs(fun, inargs):

    funinfo=inspect.getargspec(fun)

    _name=funinfo[0]    # 参数列表
    if len(_name) == 0:
        # 这是一个不需要入参的函数
        return fun()


    _defval=list(funinfo[-1] or ()) # 默认值列表
    _args=[]

    # 填充默认值列表，没有默认值的以 NO_DEFAULT_DATA 值填充
    _defval = [__NO_DEFAULT_DATA__ for i in range(len(_name)-len(_defval))]+_defval

    # 组织参数列表
    for idx, argname in enumerate(_name):
        # 参数传入
        if 'self' == argname :
            continue

        if inargs.has_key(argname):
            _args.append(inargs[argname])

        elif _defval[idx] != __NO_DEFAULT_DATA__: # 参数没有传入,检查有没有默认值
            _args.append(_defval[idx])

        else: # 参数错误
            logging.error( "缺少参数%s"%argname )
            return False

    return fun(*_args)


class Result:

    def __init__( self ):
        self.myList = list()
        self.myInfo = dict()
        self.myCode = '1'
        self.myMsg  = ''

    def setList( self, L ):
        if list != type(L):
            logging.error( "赋值类型错误" )
            return False
        self.myList.extend( L )
        return True

    def setInfo( self, I ):
        if dict != type(I):
            logging.error( "赋值类型错误" )
            return False
        self.myInfo.update( mapMembers2str(I) )
        return True

    def setCode( self, C ):
        self.myCode = str(C)
        return True

    def setMsg( self, M ):
        if str != type(M):
            logging.error( "赋值类型错误" )
            return False
        self.myMsg = M
        return True

    def set( self, value ):
        typeOfValue = type(value)
        if typeOfValue == dict:
            self.myInfo.update( mapMembers2str(value) )
        elif typeOfValue == list:
            self.myList.extend( value )
        elif typeOfValue == int:
            self.myCode = str( value )
        elif typeOfValue == str:
            self.myMsg = value
        else:
            logging.error( "赋值类型错误" )
            return False
        return True

    def get( self, target, default=None ):
        target = target.lower()

        if 'info' == target :
            return self.myInfo
        elif 'code' == target :
            return self.myCode
        elif 'msg' == target :
            return self.myMsg
        elif 'list' == target :
            return self.myList
        else:
            return default

        return default

def packret(code=None, msg=None, info=None, list=None):
    #这样写是有原因的，详情请电万路
    if code == None: code = '1'
    if msg == None: msg = ''
    if info == None: info = {}
    if list == None: list = []
    return {"CODE":str(code), "MSG":msg, "info":info, "list":list}



class threadSpace:
    thread_space=threading.local()

    def set( self, key, value ):
        self.thread_space.__dict__[str(key)] = value
        return self


    def get( self, key ):
        return self.thread_space.__dict__.get(str(key), '')

def get_hostname():
    try:
        return socket.getfqdn(socket.gethostname())
    except:
        return "errorhost"

def get_ip_address(ifname='eth0'):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
    except:
        ifname = "bond0"
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
