#coding=utf-8
import sys, traceback, time
import Queue
import logging
import threading
import XmlConfig
import esunoracle
import esunmysql

def push( db_name ):
    try:
        exist_ora = esunoracle.exist_db(db_name)
        exist_my = esunmysql.exist_db (db_name )

        if exist_ora == True and exist_my == True:
            logging.error('数据库配置名[%s]在mysql.conf和oracle.conf中发生冲突'%db_name)
            return False

        if exist_ora == False and exist_my == False:
            logging.error('数据库配置名[%s]在mysql.conf和oracle.conf中均不存在'%db_name)
            return False

        logging.info('push %s'%db_name)
        if exist_ora == True: #只有oracle才检测
            t = threading.Thread(target=watch, args = (db_name,))
            t.setDaemon(True)
            t.start()
    except:
        logging.error(traceback.format_exc())

    return True


def watch( db_name ):
    db_check_interval = 20 

    while(1):
        time.sleep(db_check_interval)
        try:
            db = esunoracle.connect(db_name)
            ret = db.ping()
        except:
            logging.error(traceback.format_exc())
            ret = False
        if not ret:
            logging.error( "[db:%s]连接异常"%db_name )
        else:
            logging.info( "[db:%s]连接正常"%db_name )

    return True

class DbSession:
    def __init__(self, dbname):
        self._dbname = dbname

    def getConn(self, isWatcher=False):

        if esunoracle.exist_db(self._dbname) and esunmysql.exist_db(self._dbname ):
            logging.error('数据库配置名[%s]在mysql.conf和oracle.conf中发生冲突'%self._dbname)
            return None
        
        if esunoracle.exist_db(self._dbname):
            return esunoracle.connect( self._dbname )
        if esunmysql.exist_db (self._dbname ):
            return esunmysql.connect( self._dbname )
        return None

