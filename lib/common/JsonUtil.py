#coding=gbk
from ujsonx import loads, dumps

def write(data):
    '''不改变原编码的json dumps'''
    return dumps(data, original_encoding=True)
    
def read(data):
    '''不改变原编码的json loads'''
    return loads(data, original_encoding=True)
