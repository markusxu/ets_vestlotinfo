#coding=utf-8

"""
通信组件
"""

import httplib
import logging
import traceback
import JsonUtil

def httpRequest(param,addr,cmdId,timeout):
    """
    发送数据
    """
    try:
        addr = str( addr[0] )  + ':' + str( addr[1] )
        conn = httplib.HTTPConnection(addr,timeout=timeout)
        conn.request("POST", "/%s" % cmdId, JsonUtil.write(param), {})

        response = conn.getresponse()
        data1 = response.read()
        conn.close()

        return response.status, data1
    except:
        pass
        return '404','{"CODE":"-404"}'

def callto(addr,data,timeout=2):
    """
    发送数据并接收回复
    """
    cmdId = 'monitor/handlerequest'

    param = {
        'data': JsonUtil.write(data)
    }

    ret,returndata=httpRequest(param, addr, cmdId, timeout)
    logging.debug(returndata)

    return JsonUtil.read(returndata)
