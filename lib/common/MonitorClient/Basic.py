#coding=utf-8

"""
MonitorClient的Basic模块
"""
import threading
import logging
import traceback
import Comm
################################

class Client:
    """
    监控服务的客户端
    """

    def __init__(self, app_id, svc_addr):
        """
        构造函数
        参数：
            app_id: 被监控的应用的ID
            svc_addr: 监控服务端的地址
        """
        self.app_id     = app_id
        self.svc_addr   = svc_addr
        self._end_event = threading.Event()

    def _call(self, cmd_id, data, timeout=2):
        """
        发送数据并接收回复
        参数：
            cmd_id: 执行的命令
            data: 跟命令相关的数据
            timeout: 超时限制
        返回服务端的回复
        """
        return Comm.callto(self.svc_addr, {
                    'app_id': self.app_id,
                    'cmd_id': cmd_id,
                    'data': data,
                 }, timeout)

    def register(self, data):
        """
        register
        """
        self._end_event.clear()

        ret = ''
        try:
            ret = self._call('register', data)
            if int(ret.get('CODE','-1')) < 0:
                raise Exception(ret)
        except:
            logging.error('monitoring register failed: [%s] %s', self.app_id, traceback.format_exc())
            return

        logging.info('monitoring register: [%s] %s', self.app_id, ret)
        return ret

    def unregister(self,data):
        """
        unregister
        """
        self._end_event.set()
        try:
            ret = self._call('unregister', data)
            if int(ret.get('CODE','-1')) < 0:
                raise Exception(ret)
        except:
            logging.error('monitoring unregister failed: [%s] %s', self.app_id, traceback.format_exc())
            return

        logging.info('monitoring unregister: [%s] %s', self.app_id, ret)
        return ret

    def alert(self, short_msg, long_msg, encoding='GBK'):
        """
        发送告警
        """
        logging.info('[monitor report]: %s | %s', short_msg, long_msg)
        self._call('alert', {
                      'short_msg': short_msg,
                      'long_msg': long_msg,
                      'encoding': encoding,
        })

    def _inform(self, data=None):
        """
        单次通知
        """
        try:
            logging.debug('monitor inform')
            self._call('inform', data)
        except:
            logging.debug(traceback.format_exc())

    def _loop_inform(self, data, interval):
        """
        自动循环通知
        """
        while True:
            self._end_event.wait(interval)
            if self._end_event.isSet():
                break
            try:
                self._inform(data)
            except:
                logging.error('monitoring inform failed: [%s] %s', self.app_id, traceback.format_exc())

    def start_loop(self, data=None, interval=60):
        """
        启动自动循环通知
        """
        self._end_event.clear()
        th = threading.Thread(target=self._loop_inform, args=(data, interval))
        th.setDaemon(True)
        th.start()

    def stop_loop(self):
        """
        自动循环通知停止
        """
        self._end_event.set()
