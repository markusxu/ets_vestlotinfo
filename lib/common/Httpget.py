#coding=utf-8
import os
import sys
import json
import shutil
import pycurl
import urllib
import urllib2
import threading
import traceback
import logging
import lxml.html as H
import cStringIO as _StringIO

low_speed_time = 120
max_size = 20485760 # 10MB

def get_curl(connection_timeout, timeout):   
    ''' 初始化curl_handle '''
    dev_null = _StringIO.StringIO()
    curl_handle = pycurl.Curl()
    curl_handle.setopt(pycurl.FOLLOWLOCATION, 1)
    curl_handle.setopt(pycurl.MAXREDIRS, 5)
    curl_handle.setopt(pycurl.CONNECTTIMEOUT, connection_timeout)
    curl_handle.setopt(pycurl.TIMEOUT, timeout)
    curl_handle.setopt(pycurl.NOSIGNAL, 1)
    curl_handle.setopt(pycurl.LOW_SPEED_LIMIT, 100)
    curl_handle.setopt(pycurl.LOW_SPEED_TIME, low_speed_time)
    curl_handle.setopt(pycurl.MAXFILESIZE, max_size)
    curl_handle.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl_handle.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl_handle.setopt(pycurl.VERBOSE, 1)
    curl_handle.setopt(pycurl.CUSTOMREQUEST,"GET")
    curl_handle.setopt(pycurl.CAINFO, os.environ['_BASIC_PATH_'] + '/etc/client_ln_004.pem') #根证书
    curl_handle.setopt(curl_handle.SSLCERT, os.environ['_BASIC_PATH_'] + '/etc/client_ln_004.cer') #客户端证书
    curl_handle.setopt(pycurl.SSLKEY, os.environ['_BASIC_PATH_'] + '/etc/client_ln_004.key') #客户端密钥
    curl_handle.setopt(pycurl.WRITEFUNCTION, dev_null.write)

    return curl_handle

def curl_fetch(curl_handle, url):
    ''' 获得调用返回值 '''
    fp = _StringIO.StringIO()
    curl_handle.setopt(pycurl.URL, url)
    curl_handle.setopt(pycurl.WRITEFUNCTION, fp.write)

    # perform the transfer   
    try:
        curl_handle.perform()
    except pycurl.error, e:
        logging.info( traceback.format_exc() )
        return {}
    content_type = curl_handle.getinfo(pycurl.CONTENT_TYPE)
    print curl_handle.getinfo(pycurl.CONTENT_TYPE)
    print curl_handle.getinfo(pycurl.HTTP_CODE)
    return json.read(fp.getvalue())


def httpget( url, param_dict={}, connection_timeout=500, timeout=800):
    ''' 调用入口 '''
    #1 获取处理对象
    curl_handle = get_curl(connection_timeout, timeout)

    #组成完整的url
    full_url = url
    if param_dict != dict():
        full_url = url + '?' + urllib.urlencode(param_dict)

    logging.info('调用链接为:%s'%(full_url))

    return curl_fetch(curl_handle, full_url)
