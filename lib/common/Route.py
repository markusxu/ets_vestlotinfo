#coding=utf-8
import ctypes
import XmlConfig
import logging
import sys
import os
import traceback
import time
import JsonUtil
import esuneas

##
#   service.xml
#       <service>
#           <split_table>
#               <spuser>0</spuser>
#               <splog>0</splog>
#           </split_table>
#       
#       </service>
#  spuser 用户表分表策略
#  -- 0表示强旧弱新
#  -- 1表示弱旧强新
#  -- 2表示停旧换新
#
#  splog 用户流水表分表策略
#  -- 0表示双写，读旧表
#  -- 1表示双写，读旧表和新表
#  -- 2表示单写，读新表
spuser = 2
splog = 2
spfreeze = 2
def init():
    global spuser
    global splog
    global spfreeze
    logging.info("route spuser [%s]"%XmlConfig.get('/service/split_table/spuser'))
    logging.info("route splog [%s]"%XmlConfig.get('/service/split_table/splog'))
    logging.info("route spfreeze [%s]"%XmlConfig.get('/service/split_table/spfreeze'))
    spuser=int( XmlConfig.get('/service/split_table/spuser', 2)  )
    splog=int( XmlConfig.get('/service/split_table/splog', 2)  )
    spfreeze=int( XmlConfig.get('/service/split_table/spfreeze', 2)  )

__sptable = ctypes.cdll.LoadLibrary("libsptable.so")



##
# @param key username
# @param object 类型 1.用户名分表策略
def route( key, object = 1 ):
    result = __sptable.route(object, key)
    return result
        
if __name__ == '__main__':

    input = [
       "",
       "a",
       "admin",
       "message digest",
       "abcdefghijklmnopqrstuvwxyz",
       "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
       "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
    ]
    
    for i in input:
        print i
        print route(i) 
        
        

        
###
#  以下是分表并发查询接口
#
def _sql_analyse(sql, key):
    ''' 解析SQL '''
    SQL = sql.upper()
    SQL = SQL.replace("\n", " ")    # 回车换成空格
    SQL = SQL.replace("\t", " ")    # 制表符换成空格
    KEY = key.upper()
    ret = []

    if KEY in ["MIN", "MAX", "COUNT", "SUM"]:
        flagLast = 0
        cnt = SQL.count(KEY)
        for i in range(cnt):
            flagKey = SQL.find(KEY, flagLast)
            
            # 查找下一个左括号
            flagBrackets = SQL.find("(", flagKey)
            if flagBrackets < 0:
                # 找不到左括号，直接退出
                break

            if SQL[flagKey+len(KEY):flagBrackets].strip() != "":
                # 下一个关键字不是左括号，忽略
                flagLast = flagKey + len(KEY)
                continue
            
            # 查找下一个逗号
            flagPause = SQL.find(",", flagBrackets)
            if flagPause < 0:
                # 如果没有找到逗号，则找 FROM 关键字
                flagPause = SQL.find(" FROM", flagBrackets)
            else:
                # 如果找到了，还要判断这个逗号前面是否有 FROM 关键字
                if SQL[flagBrackets:flagPause].count(" FROM") > 0:
                    flagPause = SQL.find(" FROM", flagBrackets)
                
            tmp   = SQL[flagKey:flagPause]
            tmp   = tmp.split()
            ret.append( tmp[-1] )
            flagLast = flagPause
    
    elif KEY == "GROUP":
        flagKey = SQL.find(" GROUP ")
        if flagKey <0 :
            return []
        flagKey = SQL.find("BY", flagKey)
        flagLast = SQL.find(" ORDER ", flagKey)
        if flagLast < 0:
            tmp   = SQL[flagKey+2:]
        else:
            tmp   = SQL[flagKey+2:flagLast]
        keysGroup = []
        for i in tmp.split(","):
            keysGroup.append( i.strip() )
        for KEY in keysGroup:
            flagKey = SQL.find(KEY)
            # 查找下一个逗号
            flagPause = SQL.find(",", flagKey)
            if flagPause < 0:
                # 如果没有找到逗号，则找 FROM 关键字
                flagPause = SQL.find(" FROM", flagKey)
            else:
                # 如果找到了，还要判断这个逗号前面是否有 FROM 关键字
                if SQL[flagKey:flagPause].count(" FROM") > 0:
                    flagPause = SQL.find(" FROM", flagKey)
            
            tmp   = SQL[flagKey:flagPause]
            tmp   = tmp.split()
            ret.append( tmp[-1] )

    elif KEY == "ORDER":
        flagKey = SQL.find(" ORDER ")
        if flagKey < 0:
            return ret
        flagKey = SQL.find("BY", flagKey)
            
        tmp   = SQL[flagKey+2:]
        keysOrderList = []
        for i in tmp.split(","):
            i = i.strip()
            i = i.split() # 要判断是否包含 ASC 或者 DESC
            if len(i) == 1:
                keysOrderList.append( {i[0]:"ASC"} )
            elif len(i) == 2:
                if i[1] == "ASC":
                    keysOrderList.append( {i[0]:"ASC"} )
                else:
                    keysOrderList.append( {i[0]:"DESC"} )

        ret = []
        for keysOrder in keysOrderList:
            KEY = keysOrder.keys()[0]
            # 这个判断用于避免 select * from t_xxx order by f_xxx desc
            if SQL.count(KEY) == 1:
                ret.append( (KEY, keysOrder[KEY]) )
                continue
            flagKey = SQL.find(KEY)
            # 查找下一个逗号
            flagPause = SQL.find(",", flagKey)
            if flagPause < 0:
                # 如果没有找到逗号，则找 FROM 关键字
                flagPause = SQL.find(" FROM", flagKey)
            else:
                # 如果找到了，还要判断这个逗号前面是否有 FROM 关键字
                if SQL[flagKey:flagPause].count(" FROM") > 0:
                    flagPause = SQL.find(" FROM", flagKey)
            
            tmp   = SQL[flagKey:flagPause]
            tmp   = tmp.split()
            ret.append( (tmp[-1], keysOrder[KEY]) )

            
    return ret


def _smart_select(dbname, sql, args, expinfo={}, inOneTable=False):
    
    logging.info("_smart_select begin")
    
    GROUP_KEYS = expinfo.get( "GROUP", _sql_analyse(sql, "GROUP") ) # 解析sql里面group by的字段有哪些
    ORDER_KEYS = expinfo.get( "ORDER", _sql_analyse(sql, "ORDER") )
    MIN_KEYS   = expinfo.get( "MIN", _sql_analyse(sql, "MIN") )
    MAX_KEYS   = expinfo.get( "MAX", _sql_analyse(sql, "MAX") )
    SUM_KEYS   = expinfo.get( "SUM", _sql_analyse(sql, "SUM") )
    CNT_KEYS   = expinfo.get( "COUNT", _sql_analyse(sql, "COUNT") )

    resp = []
    if len(GROUP_KEYS) > 0 or len(MIN_KEYS) > 0 or\
        len(MAX_KEYS) > 0 or len(SUM_KEYS) > 0 or len(CNT_KEYS) > 0:
        
        # 逐个表进行查询: 异步调用多进程
        easResp = []
        EasObj = esuneas.connect('usercpu')  # eas连接对象
        for i in range(100):
            SQL = sql.replace("[r]", str(i))
            easReq = {
                "sql" : SQL,
                "args" : JsonUtil.write(args),
                "dbname" : dbname,
            }
            retEas = EasObj.invokeAsync('lonely_query', easReq)
            easResp.append(retEas)
        
        # 逐个表进行查询：异步获取多进程返回结果
        RET_ALL_TABLE = {}
        for i in range(100):
            
            curs = easResp[i].getResult()
            
            if curs[0] < 0 or not curs[1].has_key("result"):
                logging.error("第[%s]个查询失败: %s", i, curs)
                return []

            curs = JsonUtil.read(curs[1]["result"])

            if len(curs) <= 0:
                continue

            RET_ONE_TABLE = {}
            for row in curs:
                
                # group by 处理
                key = ""
                if len(GROUP_KEYS) > 0:
                    for k in GROUP_KEYS:
                        key = key + str(row[k]) + "@"
                else:
                    key = "single"
        
                TMP = {}
                for k in row.keys():
                    TMP[k] = row[k]
                RET_ONE_TABLE[key] = TMP

            #logging.debug(RET_ONE_TABLE)
            for k, v in RET_ONE_TABLE.items():
                if not RET_ALL_TABLE.has_key(k):
                    RET_ALL_TABLE[k] = v
                else:
                    # 取出已有的数据进行合并
                    for key in RET_ALL_TABLE[k].keys():
                        if key in MIN_KEYS:
                            if RET_ALL_TABLE[k][key] > RET_ONE_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = RET_ONE_TABLE[k][key]
                        elif key in MAX_KEYS:
                            if RET_ALL_TABLE[k][key] < RET_ONE_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = RET_ONE_TABLE[k][key]
                        elif key in SUM_KEYS:
                            if not RET_ALL_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = 0
                            if RET_ONE_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] += RET_ONE_TABLE[k][key]
                        elif key in CNT_KEYS:
                            if not RET_ALL_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] = 0
                            if RET_ONE_TABLE[k][key]:
                                RET_ALL_TABLE[k][key] += RET_ONE_TABLE[k][key]
        #logging.debug( RET_ALL_TABLE )
        
        for k, v in RET_ALL_TABLE.items():
            resp.append(v)
            
    else:
        # 逐个表进行查询: 异步调用多进程
        easResp = []
        EasObj = esuneas.connect('usercpu')  # eas连接对象
        for i in range(100):
            SQL = sql.replace("[r]", str(i))
            easReq = {
                "sql" : SQL,
                "args" : JsonUtil.write(args),
                "dbname" : dbname,
            }
            retEas = EasObj.invokeAsync('lonely_query', easReq)
            easResp.append(retEas)
        
        # 逐个表进行查询：异步获取多进程返回结果
        for i in range(100):
            curs = easResp[i].getResult()
            
            if curs[0] < 0 or not curs[1].has_key("result"):
                logging.error("第[%s]个查询失败: %s", i, curs)
                return []

            curs = JsonUtil.read(curs[1]["result"])
            if len(curs) <= 0:
                continue
        
            for row in curs:
                TMP = {}
                for k in row.keys():
                    TMP[k] = row[k]
                resp.append(TMP)

            if inOneTable == True:
                # 如果明确指出数据只会存在一张表里面，那么这里直接退出
                break

    # 排序
    for order_key in ORDER_KEYS:
        key_name = order_key[0]
        order_type = order_key[1]
        if order_type == "DESC":
            resp = sorted(resp, key=lambda x:x[key_name], reverse=True)
        elif order_type == "ASC":
            resp = sorted(resp, key=lambda x:x[key_name])

    return resp

def manual_query(sql, args={}, order=[], group=[],\
    min=[], max=[], sum=[], count=[], dbname="debao", inOneTable=False):
    """ 分表查询的手动版本的接口 """
    """
        例:
            SELECT   COUNT (*) AS cntrow, SUM (ui_usermoney) AS paymoney,
                     ui_hezuotype hezuotype
                FROM b_userinfo_[r]
               WHERE ui_username LIKE '%test%'
            GROUP BY ui_hezuotype
            order by ui_hezuotype desc

        group = ["hezuotype"]
        count = ["cntrow"]
        sum   = ["paymoney"]
        order = [("hezuotype", "asc")]
    """

    try:

        # 入参格式化
        expinfo = {"GROUP":[], "ORDER":[], "MIN":[], "MAX":[], "COUNT":[], "SUM":[]}
        ORDER = []
        for i in order:
            ORDER.append( (i[0].upper(), i[1].upper()) )
        expinfo["ORDER"] = ORDER
        
        GROUP = []
        for i in group:
            GROUP.append( i.upper() )
        expinfo["GROUP"] = GROUP
        
        MIN = []
        for i in min:
            MIN.append( i.upper() )
        expinfo["MIN"] = MIN
        
        MAX = []
        for i in max:
            MAX.append( i.upper() )
        expinfo["MAX"] = MAX
        
        COUNT = []
        for i in count:
            COUNT.append( i.upper() )
        expinfo["COUNT"] = COUNT

        SUM = []
        for i in sum:
            SUM.append( i.upper() )
        expinfo["SUM"] = SUM

        retList = _smart_select(dbname, sql, args, expinfo, inOneTable)
        for ret in retList:
            for k in ret.keys():
                ret[k] = str(ret[k])
        
        return retList
    
    except:
        logging.error(traceback.format_exc())
        logging.error("函数异常")
        return []
    return []

def smart_query(sql, args={}, dbname="debao", inOneTable=False):
    """ (有限)智能查询分表 """
    """
        只能支持满足以下条件的sql
        1. 表名带有[r]后缀，如 t_userpaylog_[r] 、 b_userinfo_[r]
        2. group by 最多出现一次，order by 同理
        3. group by 的键值必须被 select 出来，order by 同理
        4. 聚合函数只支持 COUNT SUM MIN MAX
        
        例子：
        SELECT   COUNT (*) AS cntrow, SUM (ui_usermoney) AS paymoney,
                 ui_hezuotype hezuotype, ui_hezuoname as name, ui_hezuotype
            FROM b_userinfo_[r]
           WHERE ui_username LIKE '%test%'
        GROUP BY ui_hezuotype, ui_hezuoname
        order by ui_hezuotype desc
        
    """
    try:
        retList = _smart_select(dbname, sql, args, {}, inOneTable)
        for ret in retList:
            for k in ret.keys():
                ret[k] = str(ret[k])
        
        return retList
    
    except:
        logging.error(traceback.format_exc())
        logging.error("函数异常")
        return []
    return []
    