#coding=utf-8
"""
esunpublisher消息发布模块
author:kings
date:2015-11-03



(1)  配置默认路径 /etc/esun/ice.conf 
     配置节点名：msgcenter
     格式：
     [msgcenter]
     ice_file = /etc/esun/ice/ESunMsgCenter.ice
     conn = IceStorm/TopicManager:tcp -h 192.168.0.235 -p 50001 -t 2000
     Ice.MessageSizeMax = 20480
(2) 使用方法
    3.1 直接发送
    import esunpublisher
    esunpublisher.send('ESUN_PRIZE','PROPRIZE','message body','espublish_test')
    3.2 获取一个大主题，连续发送
    import esunpublisher
    #获取大主题
    msg = esunpublisher.connect('ESUN_PRIZE') 
    #发送
    msg.send('PROPRIZE','message body','espublish_test')
    msg.send('BONPRIZE','another message body','espublish_test')
"""
import os
import sys
import ConfigParser
import traceback
import threading
import Ice
import IceStorm
import logging



#异常类
class ESPublish_Error(Exception):
    pass




#配置文件路径 默认路径 /etc/esun/ice.conf
ICE_CONF = '/etc/esun/ice.conf'
#配置解析器
CONFIG_PARSER = None
#节点名字
SECTION_NAME = 'msgcenter'
#Ice文件路径
ICE_FILE = '/etc/esun/ice/ESunMsgCenter.ice'
try:
    #自定义配置路径
    if os.getenv('ESUNICE_CONF'):
        ICE_CONF = os.getenv('ESUNICE_CONF')
    CONFIG_PARSER = ConfigParser.ConfigParser()
    CONFIG_PARSER.read(ICE_CONF)
    if CONFIG_PARSER.has_option(SECTION_NAME,'ice_file'):
        ICE_FILE = CONFIG_PARSER.get(SECTION_NAME,'ice_file')
except:
    raise ESPublish_Error('config file:%s is not exist!'%ICE_CONF)

if not os.path.exists(ICE_FILE):
    raise ESPublish_Error('ice file:%s is not exist!'%ICE_FILE)


try:
    Ice.loadSlice(ICE_FILE)
    #导入模块
    import ESUN
except:
    raise ESPublish_Error('load module error!')


#主题管理器实例
MANAGER_INSTANCE = None
#锁
MANAGER_LOCK = threading.Lock()

#初始化主题管理器
def initTopicManager ():
    global MANAGER_INSTANCE
    if MANAGER_LOCK.acquire(1):
        try:
            if not MANAGER_INSTANCE:
                conn = CONFIG_PARSER.get(SECTION_NAME,'conn')
                prop = Ice.createProperties()
                for k,v in CONFIG_PARSER.items(SECTION_NAME):
                    if 'Ice.' in k:
                        prop.setProperty(k,v)
                data = Ice.InitializationData()
                data.properties = prop
                #创建通讯器
                ic = Ice.initialize(data)
                #适配器
                adapter = ic.stringToProxy(conn)
                #主题管理器
                MANAGER_INSTANCE = IceStorm.TopicManagerPrx.checkedCast(adapter)
        except:
            raise ESPublish_Error('create MANAGER_INSTANCE failed!')
        finally:
            MANAGER_LOCK.release()

#获取IP地址
def get_ip_address(ifname='eth0'):
    try:
        import socket
        import fcntl
        import struct
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
    except:
        ifname = "bond0"
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])

#消息发布类
class Publisher:

    def __init__ (self,topic):
        global MANAGER_INSTANCE
        if not MANAGER_INSTANCE:
            initTopicManager ()

        self.topic = topic
        try:
            self.topic = MANAGER_INSTANCE.retrieve(topic)
        except IceStorm.NoSuchTopic, e:
            self.topic = MANAGER_INSTANCE.create(topic)
    
        pub = self.topic.getPublisher().ice_oneway()
        self.msgPrx = ESUN.ESunMsgCenterPrx.uncheckedCast(pub)
        

    def send (self,func,args,res):
        logging.info("发送ICESTORM消息[func:%s],[args:%s],[res:%s]", func, args, res)
        return self.msgPrx.sendmsg(func,args,res)

    def sendWarn (self,func,args,res):
        ip = get_ip_address()
        app = ''
        if len(sys.argv) >= 2:
            app = sys.argv[1]
        header = "[%s][%s]"%(ip,app)
        _args = "%s%s"%(header, args)
        logging.info("发送ICESTORM消息[func:%s],[args:%s],[res:%s]", func, _args, res)
        return self.msgPrx.sendmsg(func,_args,res)

#获取消息发布对象
def connect (topic):
    return Publisher(topic)
        
#发消息 异常未捕获 调用者需处理异常
#topic 大主题
#func 小主题
#args 消息体
#res  消息来源
def send (topic,func,args,res):
    msg = Publisher(topic)
    return msg.send(func,args,res)

#发告警消息 带IP和进程名
#topic 大主题
#func 小主题
#args 消息体
#res  消息来源
def sendWarn (topic,func,args,res):
    msg = Publisher(topic)
    return msg.sendWarn(func,args,res)



if __name__ == '__main__':
    #send('ESUN_PRIZE','PROPRIZE','{"test":"test"}','publish_test')
    for i in range(0,10000):
        t = threading.Thread( target=sendWarn,args=('ESUN_PRIZE','PROPRIZE','{"test":"test_%s"}'%(i+1),'publish_test'))
        t.start()
        
