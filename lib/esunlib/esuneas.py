#coding=utf-8
"""
esuneas eas客户端
author:kings
date:2015-11-09



(1)  配置默认路径 /etc/esun/ice.conf 
     配置节点名：eas_xxx 
     格式：
     [eas_xxx]
     ice_file = /etc/esun/ice/EAS.ice
     conn = ESunEAS:tcp -h 192.168.50.141 -p 50020 -t 30000:tcp -h 192.168.50.141 -p 50020 -t 30000
     Ice.MessageSizeMax = 20480
(2) 使用方法
    import esuneas 
    client = esuneas.connect('user') #连接 连接时不需要带eas_
    ret = client.invoke('getuserexpinfo',{'username':'card100'}) #同步调用
    callback = client.invokeAsync('getuserexpinfo',{'username':'card100'}) #异步调用
    callback.getResult()   
"""
import os
import sys
import time
import ConfigParser
import traceback
import threading
import Ice
import logging



##################### 服务可视化日志输出 ##############################  
def logCallback(args):
    """ 打印服务日志可视化的回调函数 """
    pass

def getData():
    """ 获取服务本身附属数据信息 """
    return {
            'from':'python', 
            'cmd':" ".join(sys.argv), 
            'remote_addr' : '', 
            'uuid' : '', 
            'usercheck' : '', 
            'cdn-src-ip' : '', 
            'useragent' : '',
            'source':'',}

def viewLog(type):         
    def _viewLog(func):
        """ 装饰函数，记录服务耗时 """
        def new_func( *args, **kwargs):
            begintime = time.time()
            #将异常抛出 
            ret = func(*args, **kwargs)
            try:
                costtime = (time.time() - begintime)*1000
                code = str(1)
                if isinstance(ret, tuple) and len(ret) > 0:
                    code = str(ret[0])
                args = [str(args[0].__dict__.get('eas_name', '')), str(args[1]), code, costtime,type]
                logCallback(args)
            except:
                logging.error(traceback.format_exc())
            return ret
        return new_func
    return _viewLog
    
try:
    import logdecorationutil
except:
    pass
##################### 服务可视化日志输出 ##############################


#异常类
class ESEAS_Error(Exception):
    pass




#配置文件路径 默认路径 /etc/esun/ice.conf
ICE_CONF = '/etc/esun/ice.conf'
#配置解析器
CONFIG_PARSER = None
#节点名字
SECTION_NAME = 'eas'
#Ice文件路径
ICE_FILE = '/etc/esun/ice/EAS.ice'
try:
    #自定义配置路径
    if os.getenv('ESUNICE_CONF'):
        ICE_CONF = os.getenv('ESUNICE_CONF')
    CONFIG_PARSER = ConfigParser.ConfigParser()
    CONFIG_PARSER.read(ICE_CONF)
    if CONFIG_PARSER.has_option(SECTION_NAME,'ice_file'):
        ICE_FILE = CONFIG_PARSER.get(SECTION_NAME,'ice_file')
except:
    raise ESEAS_Error('config file:%s is not exist!'%ICE_CONF)

if not os.path.exists(ICE_FILE):
    raise ESEAS_Error('ice file:%s is not exist!'%ICE_FILE)


try:
    Ice.loadSlice(ICE_FILE)
    #导入模块
    import ESUN
except:
    raise ESEAS_Error('load module error!')


#eas实例
EAS_IC = None
#锁
EAS_LOCK = threading.Lock()
#proxy池
PROXY_POOL = {}
#初始化ic实例
def initEAS_IC ():
    global EAS_IC
    if EAS_LOCK.acquire(1):
        try:
            if not EAS_IC:
                prop = Ice.createProperties()
                for k,v in CONFIG_PARSER.items(SECTION_NAME):
                    if 'Ice.' in k:
                        prop.setProperty(k,v)
                prop_data = Ice.InitializationData()
                prop_data.properties = prop
                #创建通讯器
                EAS_IC = Ice.initialize(prop_data)
        except:
            print traceback.format_exc()
            raise ESEAS_Error('create EAS_IC failed')
        finally:
            EAS_LOCK.release()

#easclient 类
class EASClient:

    def __init__ (self,eas_name):
        self.eas_name = '%s_%s'%(SECTION_NAME,eas_name)
        self.twoway = None
        self.oneway = None

    def initEas (self):
        self.conn = CONFIG_PARSER.get(self.eas_name,'conn')
        #初始化eas节点
        global EAS_IC
        if not EAS_IC:
            initEAS_IC()

        proxy = PROXY_POOL.get(self.eas_name)
        if proxy is None:
            proxy = EAS_IC.stringToProxy(self.conn)
            PROXY_POOL[self.eas_name] = proxy

        self.twoway = ESUN.EASPrx.checkedCast(proxy)
        self.oneway = ESUN.EASPrx.uncheckedCast(proxy.ice_oneway().ice_timeout(5))

    @viewLog('0')
    def invoke (self,id, param):
        #同步调用
        try:
            data = getData()
            
            if not self.twoway:
                self.initEas()

            ret = self.twoway.invoke(id,param,data)
            return ret
        except:
            logging.error( '%s:%s' %(self.eas_name,traceback.format_exc()) )
            ret = ( -1, {'CODE' : '-1', 'MSG' : '取EAS连接失败'}, [] )
            return ret

    @viewLog('1')
    def invoke1 (self,id,param,timeout=0):
        #异步调用 
        try:
            data = getData()
            if not self.oneway:
                self.initEas()

            if timeout>0:
                self.oneway = ESUN.EASPrx.uncheckedCast(self.oneway.ice_timeout(timeout))
            elif timeout<0:
                self.oneway = ESUN.EASPrx.uncheckedCast(self.oneway.ice_timeout(-1))
            #无返回值
            self.oneway.invoke1(id, param, data)
            return True
        except:
            logging.error( '%s:%s' %(self.eas_name,traceback.format_exc())  )
            return False

    @viewLog('1')
    def invokeAsync (self, id , param):
        #异步回调
        try:
            callback = EasCallBack()
            data = getData()

            if not self.twoway:
                self.initEas()

            self.twoway.invoke_async(callback, id, param, data)
            return callback
        except:
            logging.error( '%s:%s' %(self.eas_name,traceback.format_exc())  )
            ret = ( -1, {'CODE' : '-1', 'MSG' : '调用异常'}, [] )
            callback.ice_response(*ret) 
            return callback

#回调类
class EasCallBack(object):
    def __init__(self):
        self.ret = None
        
    def ice_response(self, *args, **kargs):
        self.ret = args
        
    def ice_exception(self, *args, **kargs):
        self.ret = ( -1, {'CODE' : '-1', 'MSG' : '调用异常'}, [] )
        
    def getResult(self, timeout = 0):
        timeout = timeout / 1000.0
        t = time.time()
        while True:
            if self.ret is not None:
                return self.ret
            else:
                time.sleep(0.005)
            if timeout > 0 and time.time() - t > timeout:
                return ( -1, {'CODE' : '-1', 'MSG' : '调用超时'}, [] )



def connect (eas_name):
    return EASClient(eas_name)

if __name__ == '__main__':
    '''
    user = connect('user')
    ret = user.invoke('getuserexpinfo',{'username':'card100'})
    for i in range(len(ret)):
        print ret[i] '''
    hiscmp = connect('compare')
    ret = hiscmp.invoke('hiscompare',{'codes':'01,02,03,04,05,06,07,08,09,10|01,02#1','count':'400'})
    for i in range(len(ret)):
        print ret[i]
        
    callback = hiscmp.invokeAsync('hiscompare',{'codes':'01,02,03,04,05,06,07,08,09,10|01,02#1','count':'400'})
    print callback.getResult()

