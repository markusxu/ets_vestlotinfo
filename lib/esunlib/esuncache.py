#coding=utf-8
"""
esuncache缓存模块
author:kings
date:2015-09-10

(1)
Config_Path: 缓存配置默认路径 /etc/esun/esuncache.conf 
自定义配置需要设置环境变量:export ESUNCACHE_CONF=/mydir/esuncache.conf
配置格式：
*************************************************
[Node_1]
host=127.0.0.1
port=6379
type=redis

[Node_2]
host=192.168.41.161
port=11211
type=memcache
**************************************************

(2) redis
import esuncache
#不使用池
redis = esuncache.connect('Node_1')
redis.set(key,value)
redis.get(key)
#使用池
cachepool = esuncache.CachePool('Node_1',pool_max=10)
redis = esuncache.connect(None,cachepool)
redis.set(key,value)
redis.get(key)

(3) memcache
import esuncache
#不使用池
mem = esuncache.connect('Node_2')
mem.set(key,value)
mem.get(key)
#使用池
cachepool = esuncache.CachePool('Node_2',pool_max=10)
mem = esuncache.connect(None,cachepool)
mem.set(key,value)
mem.get(key)

"""

import os
import sys
import ConfigParser
import traceback
import threading
import redis
import memcache


#异常类
class ESCache_Error(Exception):
    pass

Cache_Type = {'redis':1,'memcache':2}
#默认配置 :/etc/esun/esuncache.conf
Config_Path = '/etc/esun/esuncache.conf'
Config_Parser = None
try:
    #自定义配置路径
    if os.getenv('ESUNCACHE_CONF'):
        Config_Path = os.getenv('ESUNCACHE_CONF')
    Config_Parser = ConfigParser.ConfigParser()
    Config_Parser.read(Config_Path)
except:
    raise ESCache_Error('config file:%s is not exist!'%Config_Path)



#redis 缓存对象
class RedisCache(object):
    
    def __init__ (self,nodeName,cache_pool = None):
        #check node 
        if not Config_Parser.has_section(nodeName):
            raise ESCache_Error('cache node [%s] is not exist!'%nodeName)
        self.host = Config_Parser.get(nodeName,'host')
        self.port = Config_Parser.get(nodeName,'port')
        self.socket_timeout = None
        if Config_Parser.has_option(nodeName,'socket_timeout'):
            self.socket_timeout = float(Config_Parser.get(nodeName,'socket_timeout'))
        self.db = 0
        if Config_Parser.has_option(nodeName,'db'):
            self.db = Config_Parser.get(nodeName,'db')
        
        self.cache = None
        if not cache_pool:
            self.cache = redis.Redis(host=self.host,
                                     port=int(self.port),
                                     db = self.db,
                                     socket_timeout = self.socket_timeout)
        else:
            self.cache = redis.Redis(host=self.host,
                                     port=int(self.port),
                                     db = self.db,
                                     socket_timeout=self.socket_timeout,
                                     connection_pool = cache_pool.pool)
     #基本操作
    def get (self,key):
        return self.cache.get(key)

    def set (self,key,value):
        return self.cache.set(key,value)

    def setex (self,key,value,time):
        return self.cache.setex(key,value,time)
        

    def exists (self,key):
        return self.cache.exists(key)

    def delete (self,*keys):
        return self.cache.delete(*keys)

    def expire (self,key,time):
        return self.cache.expire(key,time)

    def incr (self,key,amount =1):
        return self.cache.incr(key,amount)

    def decr (self,key,amount =1):
        return self.cache.decr(key,amount)
     
    #列表操作
    def llen (self,key):
        return self.cache.llen(key)

    def lpush (self,key,*values):
        return self.cache.lpush(key,*values)

    def lpop (self,key):
        return self.cache.lpop(key)

    def rpush (self,key,*values):
        return self.cache.rpush(key,*values)

    def rpop (self,key):
        return self.cache.rpop(key)

    def lrange (self, key, start, end):
        return self.cache.lrange(key,start,end)

    #hash操作
    def hexists (self,name,key):
        return self.cache.hexists(name,key)

    def hdel (self,name,*keys):
        return self.cache.hdel(name,*keys)
        
    def hset (self,name,key,value):
        return self.cache.hset(name,key,value)

    def hget (self,name,key):
        return self.cache.hget(name,key)

    def hlen (self,name):
        return self.cache.hlen(name)  
        
    def hgetall (self,name):
        return self.cache.hgetall(name)

    def hmget (self,name,keys):
        return self.cache.hmget(name,keys)

    def hmset (self,name,mapping):
        return self.cache.hmset(name,mapping)

    def publish(self, channel, message):
        return self.cache.publish(channel,message)
        
    #有序集合
    def zadd (self, name, value=None, score=None, **pairs):
        return self.cache.zadd(name,value,score,**pairs)

    def zrem(self, name, *values):
        return zrem(name,*values)

    def zcard (self, name):
        return self.cache.zcard(name)

    def zcount (self, name, min, max):
        return self.cache.zcount(name,min,max)

    def zrange (self, name, start, end, desc=False, withscores=False,
               score_cast_func=float):
        return self.cache.zrange(name,start,end,desc,withscores,score_cast_func)

    def __getattr__(self, name):
        return getattr(self.cache, name)

#memcache 缓存对象
class MemCache(object):

    def __init__ (self,nodeName,cache_pool = None):
        #检查节点配置是否存在 
        if not Config_Parser.has_section(nodeName):
            raise ESCache_Error('cache node [%s] is not exist!'%nodeName)
        self.host = Config_Parser.get(nodeName,'host')
        self.port = Config_Parser.get(nodeName,'port')
        self.cache_pool = cache_pool
        self.cache = None
        if not self.cache_pool:
            servers = []
            allhost = self.host.split('/')
            allport = self.port.split('/')
            if len(allhost) != len(allport):
                raise ESCache_Error('cache node [%s] host and port is not match!'%nodeName)
            for i in range(len(allhost)):
                servers.append('%s:%s'%(allhost[i],allport[i]))
            self.cache = memcache.Client(servers,debug=0)
        else:
            self.cache = None

    def set (self,key,value,expire=0):
        return self.execute_command('set',key,value,expire)

    def get (self,key):
        return self.execute_command('get',key)

    def delete (self,key,time = 0):
        return self.execute_command('delete',key,time)

    def delete_multi(self, keys, time=0, key_prefix=''):
        return self.execute_command('delete_multi',keys,time,key_prefix)

    def incr(self, key, delta=1):
        return self.execute_command('incr',key,delta)

    def decr(self, key, delta=1):
        return self.execute_command('decr',key,delta)

    def get_multi (self, keys, key_prefix=''):
        return self.execute_command('get_multi',keys,key_prefix)

    def set_multi(self, mapping, time=0, key_prefix='', min_compress_len=0):
        return self.execute_command('set_multi',mapping,time,key_prefix,min_compress_len)

    def execute_command (self,command,*args):
        conn = None
        try:
            if not self.cache_pool:
                conn = self.cache
            else:
                conn = self.cache_pool.get_memconn()
            
            func = getattr(conn,command,'None')
            return func(*args)
        except:
            raise ESCache_Error(traceback.format_exc())
        finally:
            if self.cache_pool and conn:
                self.cache_pool.release_memconn(conn)


#缓存连接池对象
class CachePool(object):

    def __init__ (self,node_name,pool_max=10):
        print 'init cache pool'
        #check node 
        if not Config_Parser.has_section(node_name):
            raise ESCache_Error('cache node [%s] is not exist!'%node_name)
        self.node_name = node_name
        self.host = Config_Parser.get(node_name,'host')
        self.port = Config_Parser.get(node_name,'port')
        self.cache_type = Config_Parser.get(node_name,'type')
        if not Cache_Type.has_key(self.cache_type):
            raise ESCache_Error('unknown cache type:%s'%self.cache_type)
        self.pool_max = pool_max
        self.pool = None
        self.pool_used = None
        if Cache_Type[self.cache_type] == 1:
            self.pool = redis.ConnectionPool(max_connections = self.pool_max ,
                                             host=self.host,
                                             port=int(self.port))
        elif Cache_Type[self.cache_type] == 2:
            servers = []
            allhost = self.host.split('/')
            allport = self.port.split('/')
            if len(allhost) != len(allport):
                raise ESCache_Error('cache node [%s] host and port is not match!'%nodeName)
            for i in range(len(allhost)) :
                servers.append('%s:%s'%(allhost[i],allport[i]))
            self.pool = []
            self.pool_used = set()
            #初始化连接池
            for i in range(self.pool_max):
                conn = memcache.Client(servers,debug=0)
                self.pool.append(conn)
        else:
            raise ESCache_Error('invalid cache type!')


    #获取memcache连接
    def get_memconn (self):
        if  Cache_Type[self.cache_type] != 2:
            raise ESCache_Error('%s can not support this method!'%self.cache_type)
        conn = None
        try:
            conn = self.pool.pop()
        except:
            raise ESCache_Error('cache pool aleady reach max number!')
        self.pool_used.add(conn)
        return conn
    #将memcache连接放回池中
    def release_memconn (self,conn):
        if Cache_Type[self.cache_type] != 2:
            raise ESCache_Error('%s can not support this method!'%self.cache_type)
        self.pool_used.remove(conn)
        self.pool.append(conn)
    
#根据节点名字或者连接池获取缓存对象
#node_name: 节点名字
#cache_pool: 连接池对象
#当连接池对象为空时,使用node_name创建缓存对象
#当连接池对象不为空时,将使用cache_pool创建对象,忽略node_name
def connect (node_name,cache_pool = None):
    cache = None
    node = node_name
    if cache_pool:
        node = cache_pool.node_name
    if not Config_Parser.has_section(node):
        raise ESCache_Error('cache node [%s] is not exist!'%node)

    nodeType = Config_Parser.get(node,'type')
    if not Cache_Type.has_key(nodeType):
        raise ESCache_Error('unknown cache type:%s'%nodeType)
    
    if Cache_Type[nodeType] == 1:
        if not cache_pool:
            cache = RedisCache(node)
        else:
            cache = RedisCache(node,cache_pool)
    elif Cache_Type[nodeType] == 2:
        if not cache_pool:
            cache = MemCache(node)
        else:
            #cache = MemCache(node,cache_pool)
            #暂时不支持memcache连接池
            raise ESCache_Error("can't support memecache pool")

    return cache

if __name__ == '__main__':
    print '**********begin redis test**********'
    #不用池
    print 'begin connect esuncache'
    r = connect('r.lot')
    print 'redis set key:name,return:%s'%r.set('name','peter')
    print 'redis get key:name,return:%s'%r.get('name')
    #创建池对象
    print 'begin create cachepool'
    cachePool = CachePool(node_name ='r.lot', pool_max = 10)
    r_p = connect(None,cachePool)
    print 'from cachepool get key:name,return:%s'%r_p.get('name')
    print '**********end redis test**********'

    print '**********begin memcache test**********'
    #不用连接池
    print 'begin connect esuncache'
    mem = connect('m.login')
    print 'memcache set key:name,return:%s'%mem.set('name','aaa',10)
    print 'memcache get key:name,return:%s'%mem.get('name')
    dict = {'t1':'111','t2':'222','t3':'333'}
    print 'memcache set_multi:%s,return:%s'%(dict,mem.set_multi(dict))
    print 'memcache get_multi:%s'%mem.get_multi(['t1','t2','t3'])
    
    print 'memcache delete_multi,return %s'%mem.delete_multi(['t1','t2'])
    print 'memcache get_multi,return %s'%mem.get_multi(['t1','t2','t3'])
    #使用连接池
    print 'begin create cachepool'
    cachepool = CachePool(node_name ='m.login', pool_max = 20)
    mem_p = connect(None,cachepool)
    print 'from cachepool set key:name,return:%s'%mem_p.set('name','test')
    print 'from cachepool get key:name,return:%s'%mem_p.get('name')

    print '**********end memcache test**********'
