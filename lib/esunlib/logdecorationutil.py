#coding=utf-8

import os
import sys
import socket
import logging
import traceback

import XmlConfig

import esuneas
import esunacct
import esunmysql
import esunoracle
import esunpublisher
import esunrabbit

gService = ''   
gModuleList = [] 
gFrame = ''
gIp = ''
gProcess = ''
try:
    """ 获取本机ip信息 """
    gIp = socket.getfqdn(socket.gethostname())
except:
    logging.info('no ip')
try:
    """获取框架配置信息信息"""
    frameList = sys.argv[0].split('/')
    gProcess = sys.argv[1]
    gFrame = frameList[-1].split('.')[0]
    serviceList = frameList[-3].split('_')
    gService = frameList[-3]
    if len(serviceList) >= 2:
        gService = gFrame + '_' + serviceList[-1]
    XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + '/etc/' + gFrame +'.xml')
    
    if gFrame and gFrame in ['eas','ets']:
        configStr = '/' + gFrame + '/applications/'    
        serviceList = XmlConfig.list(configStr)
        
        moduleList = []
        for k,v in serviceList.items():
            processList = []
            if isinstance(v, list):
                for sl in v:
                    path = sl.get('path', '')
                    if path:
                        path = path.replace('.', '/')
                        processList.append(path)
                gModuleList += processList
            if isinstance(v, dict):
                path = v.get('path', '')
                if path:
                    path = path.replace('.', '/')
                    processList.append(path)
                gModuleList += processList
        gModuleList = list(set(gModuleList))
except:
    logging.info('[system view]no config')
    
gMysqlswitch = '0'
gOracleswitch = '0'
gHttpswitch = '0'
gEasswitch = '0'
gMqswitch = '0'
gAcctswitch = '0'   
try:
    #是否进行数据库耗时计算的开关（0关闭， 1开启）
    XmlConfig.loadFile(os.environ['_BASIC_PATH_'] + '/etc/service.xml')
    query = XmlConfig.list('/service/decoration_flag/')
    gMysqlswitch = query.get('mysql', '0')
    gOracleswitch = query.get('oracle', '0')
    gHttpswitch = query.get('http', '0')
    gEasswitch = query.get('eas', '0')
    gMqswitch = query.get('mq', '0')
    gAcctswitch = query.get('acct', '0')
except:
    logging.info('no switch')
    
def getServiceName():
    """ 获取当前服务名和接口名 """
    global gModuleList
    global gService
    service = ''
    method = ''
    try:
        service = gService
        moduleList = traceback.extract_stack()
        for v in gModuleList: 
            for module in moduleList:
                if v in module[0] and module[2].find('_') != 0:
                    method = module[2].lower()
                    break
    except:
        logging.error(traceback.format_exc())
        
    ret = {
            'service' : service,
            'func' : method,
        }
    return ret
        
def easLogCallback(args):
    """ 服务间调用信息打印 """
    try:
        global gFrame
        global gIp
        global gProcess
        sret = getServiceName()
        if args[4] == '1':
            logging.info('''[systemAsync viewer] [dest_service:%s] [dest_func:%s] [ret:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]'''%(str(args[0]), str(args[1]), str(args[2]), args[3], sret['service'], sret['func']))
        else:
            logging.info('''[system viewer] [dest_service:%s] [dest_func:%s] [ret:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]'''%(str(args[0]), str(args[1]), str(args[2]), args[3], sret['service'], sret['func']))
    except:
        logging.error(traceback.format_exc())
        
def acctLogCallback(args):
    """ 账务调用信息打印 """
    try:
        sret = getServiceName()
        logging.info('''[system viewer] [dest_service:%s] [dest_func:%s] [ret:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]'''%('acct', args[0], args[1], args[2], sret['service'], sret['func']))
    except:
        logging.error(traceback.format_exc())
        
def mysqlLogCallback(args):
    """ mysql调用信息打印 """
    try:
        sret = getServiceName()
        logging.info('[db viewer] [dest_type:%s] [dest_db:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]' % ('mysql', str(args[0]), args[1], sret['service'], sret['func']))
    except:
        logging.error(traceback.format_exc())
        
def oracleLogCallback(args):
    """ oracle调用信息打印 """
    try:
        sret = getServiceName()
        logging.info('[db viewer] [dest_type:%s] [dest_db:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]' % ('oracle', str(args[0]), args[1], sret['service'], sret['func']))
    except:
        logging.error(traceback.format_exc())
        
def mqLogCallback(args):
    """ mq调用信息打印 """
    try:
        sret = getServiceName()
        logging.info('[mq viewer] [dest_type:%s] [dest_topic:%s] [time_used(ms):%.3f] [source_service:%s] [source_func:%s]' % ('mq', str(args[0]), args[1], sret['service'], sret['func']))
    except:
        logging.error(traceback.format_exc())
        
def serviceDataCallback():
    """ 返回服务相关信息 """
    data = {}
    try:
        global gFrame
        global gIp
        global gProcess
        sret = getServiceName()
        source = sret['service'] + '/' + str(gProcess) + '/' + sret['func']
        data ={
                'from':str(gFrame), 
                'cmd':" ".join(sys.argv), 
                'remote_addr' : str(gIp), 
                'uuid' : '', 
                'usercheck' : '', 
                'cdn-src-ip' : '', 
                'useragent' : '', 
                'source':source,
        }
    except:
        logging.error(traceback.format_exc())
    return data
        
esuneas.logCallback = easLogCallback
esuneas.getData = serviceDataCallback
esunmysql.logCallback = mysqlLogCallback
esunoracle.logCallback = oracleLogCallback
esunacct.logCallback = acctLogCallback
esunrabbit.logCallback = mqLogCallback