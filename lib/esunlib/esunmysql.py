#coding=utf-8
'''
esunmysql mysql数据访问模块
author:kings
date:2015-11-19

(1)  配置默认路径 /etc/esun/mysql.conf 
     配置节点名：彩种id缩写
     格式：

     [sms]
     host=192.168.41.180
     port=3306
     user=sms
     passwd=sms
     db=sms
     charset=gbk
     use_unicode=False
     auto_commit=True
     pooling = pool_size:21;timeout:16;use_threadlocal:True

(2)  参考文档：
    DBAPI-2.0说明：
        http://www.python.org/topics/database/DatabaseAPI-2.0.html
    MySQLdb的参数和用法：
        http://mysql-python.sourceforge.net/MySQLdb.html
    SQLAlchemy连接池的参数和用法：
        http://www.sqlalchemy.org/docs/core/pooling.html

(3) 使用方法
    3.1 自定义连接池
        import esunmysql
        poolArgs = {'pool_size':4,'timeout':16,'use_threadlocal':True}
        esunmysql.initPool('sms',**poolArgs)
        db = esunoracle.connect('sms')
        rows = db.query('select * from NS_City limit 0,10')
    3.2 使用默认连接池
        import esunmysql
        db = esunmysql.connect('sms')
        rows = db.query('select * from NS_City limit 0,10')


'''


import MySQLdb

import warnings
warnings.filterwarnings('ignore', module='Mysql', append=1)
warnings.filterwarnings('ignore', module='Db.Mysql', append=1)

import os
import sys
import time
import thread
import traceback
import logging

##################### 服务可视化日志输出 ##############################  
def logCallback(args):
    """ 打印服务日志可视化的回调函数 """
    pass
    
def viewLog(func):
    """ 装饰函数，记录服务耗时 """
    def new_func( *args, **kwargs):
        begintime = time.time()
        #将异常抛出 
        ret = func(*args, **kwargs)
        try:
            costtime = (time.time() - begintime)*1000
            args = [str(args[0].__dict__.get('dbname', '')), costtime]
            logCallback(args)
        except:
            logging.error(traceback.format_exc())
        return ret
    return new_func
    
try:
    import logdecorationutil
except:
    pass
##################### 服务可视化日志输出 ##############################

#异常类
class ESMysql_Error(Exception):
    pass


class Connection:

    def __init__(self, conn, dbId = None):
        self.conn = conn
        self.dbname = dbId

    @viewLog
    def setAutoCommit (self,auto=True):
        self.conn.autocommit(auto)

    @viewLog
    def query(self, *args, **kwargs):
        '''查询，返回所有行记录'''
        cur = self.conn.cursor(MySQLdb.cursors.DictCursor)
        cur.execute(*args, **kwargs)
        return cur.fetchall()

    @viewLog
    def queryOne(self, *args, **kwargs):
        '''查询，返回单行记录'''
        cur = self.conn.cursor(MySQLdb.cursors.DictCursor)
        cur.execute(*args, **kwargs)
        return cur.fetchone()

    @viewLog
    def execute(self, *args, **kwargs):
        '''执行，返回被影响的行数'''
        cur = self.conn.cursor()
        cur.execute(*args, **kwargs)
        return cur.rowcount

    @viewLog
    def callproc(self, *args, **kwargs):
        '''执行存储过程，返回参数元组'''
        cur = self.conn.cursor()
        ret = cur.callproc(*args, **kwargs)
        return ret

    @viewLog
    def executeMany(self, *args, **kwargs):
        '''执行，many'''
        cur = self.conn.cursor()
        return cur.executemany(*args, **kwargs)

    @viewLog
    def insert(self, *args, **kwargs):
        '''插入，返回自增长字段的值，没有的话返回0'''
        cur = self.conn.cursor()
        cur.execute(*args, **kwargs)
        return cur.lastrowid

    def commit(self):
        '''提交'''
        return self.conn.commit()

    def rollback(self):
        '''回滚'''
        return self.conn.rollback()

    def isAlive(self):
        '''检查是否存活'''
        try:
            self.conn.ping()
        except MySQLdb.OperationalError:
            return 0
        return 1

    def getCursor(self):
        try:
            self.conn.ping()
        except:
            self.close()
            self.connect()
        if self.conn:
            return self.conn.cursor(MySQLdb.cursors.DictCursor)
        else:
            return None

    def ping(self):
        try:
            self.conn.ping()
        except:
            self.close()
            self.connect()
        if self.conn:
            return True
        else:
            return False

    def close(self):
        try:
            self.conn.close()
        except:
            pass

    def __del__(self):
        self.close()


def _connect(**connArgs):
    auto_commit = connArgs.pop('auto_commit', False)
    init_command = connArgs.pop('init_command', None)
    conn = MySQLdb.connect(**connArgs)
    if auto_commit:
        conn.autocommit(auto_commit)
    conn.cursor().execute("set session transaction isolation level read committed")
    if init_command:
        conn.cursor().execute(init_command)
    return conn


import ConfigParser
import threading
from sqlalchemy.pool import QueuePool

_DB_LOCK_ = threading.RLock() # 锁
_DB_POOL_ = {} # 连接池

#默认配置 :/etc/esun/mysql.conf
CFG_PATH = '/etc/esun/mysql.conf'
CFG_PARSER = None
try:
    #自定义配置路径
    if os.getenv('ESUNMYSQL_CONF'):
        CFG_PATH = os.getenv('ESUNMYSQL_CONF')
    CFG_PARSER = ConfigParser.ConfigParser()
    CFG_PARSER.read(CFG_PATH)
except:
    raise ESMysql_Error('config file:%s is not exist!'%CFG_PATH)


#初始化一个池
#id 连接对象
#poolArgs 池配置参数
def initPool (id,**poolArgs):
    global _DB_POOL_
    if _DB_LOCK_.acquire(1):
        try:
            #不允许多次初始化同一个池
            if _DB_POOL_.has_key(id):
                raise ESMysql_Error('_DB_POOL_: key %s is already exist!'%id)

            if not CFG_PARSER.has_section(id):
                raise ESMysql_Error('mysql node [%s] is not exist!'%id)

            connArgs = { 'host':CFG_PARSER.get(id,'host'),
                         'port':CFG_PARSER.getint(id,'port'), 
                         'user':CFG_PARSER.get(id,'user'), 
                         'passwd':CFG_PARSER.get(id,'passwd'),
                         'db':CFG_PARSER.get(id,'db'),
                         'charset':CFG_PARSER.get(id,'charset'),
                         'use_unicode':CFG_PARSER.getboolean(id,'use_unicode'),
                         'auto_commit':CFG_PARSER.getboolean(id,'auto_commit'),
                         }
            if not poolArgs: #启动默认池设置
                pool_str = CFG_PARSER.get(id,'pooling')
                logging.info( 'esunmysql [db:%s] use default pool setup:[%s]'%(id,pool_str))
                arr = [ x.split(':') for x in pool_str.split(';')]
                poolArgs = dict([(i[0],eval(i[1])) for i in arr])
            _DB_POOL_[id] = QueuePool(lambda: _connect(**connArgs), **poolArgs)
        finally:
            _DB_LOCK_.release()

#根据ID连接库
def connect (id):
    global _DB_POOL_
    if _DB_LOCK_.acquire(1):
        try:
            #如果没有初始化 用默认池
            if not _DB_POOL_.has_key(id):
                initPool(id)

            pool = _DB_POOL_[id]
            for i in range(pool.size()+1):
                conn = pool.connect()
                try:
                    conn.ping()
                    break
                except MySQLdb.OperationalError:
                    conn.invalidate()    
            return Connection(conn,id)
        finally:
            _DB_LOCK_.release()

#是否存在某连接
def exist_db (id):
    return CFG_PARSER.has_section(id)

def dispose():
    '''释放连接池'''
    global _DB_POOL_
    if _DB_LOCK_.acquire(1):
        try:
            for id in _DB_POOL_:
                _DB_POOL_[id].dispose()
        finally:
            _DB_LOCK_.release()

import atexit
atexit.register(dispose)

if __name__ == '__main__':
    db = connect('sms')
    rows = db.query('select * from NS_City limit 0,10')
    for row in rows:
        print row
        

    



