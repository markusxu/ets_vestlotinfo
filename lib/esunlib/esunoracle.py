#coding=utf-8
'''
esunoracle oracle数据访问模块
author:kings
date:2015-11-19

(1)  配置默认路径 /etc/esun/oracle.conf 
     配置节点名：彩种id缩写
     格式：

     [jz]
     dsn = LOTTERY_JZ_BG
     user = lottery_jz
     password = jz
     threaded = True
     pooling = pool_size:21;timeout:16;use_threadlocal:True

(2)  对lib/Db/Oracle.py 的简单优化，函数和方法基本不变
     参考文档：
    DBAPI-2.0说明：
        http://www.python.org/topics/database/DatabaseAPI-2.0.html
    SQLAlchemy连接池的参数和用法：
        http://www.sqlalchemy.org/docs/core/pooling.html
    cx_Oracle的参数和用法：
        http://cx-oracle.sourceforge.net/html/index.html
(3) 使用方法
    3.1 自定义连接池
        import esunoracle
        poolArgs = {'pool_size':4,'timeout':16,'use_threadlocal':True}
        esunoracle.initPool('jz',**poolArgs)
        db = esunoracle.connect('jz')
        rows = db.query('select * from t_jzproject_info where rownum<2')
    3.2 使用默认连接池
        import esunoracle
        db = esunoracle.connect('jz')
        rows = db.query('select * from t_jzproject_info where rownum<2')


'''




import cx_Oracle
import os
import sys
import time
import thread

import traceback
import logging

##################### 服务可视化日志输出 ##############################  
def logCallback(args):
    """ 打印服务日志可视化的回调函数 """
    pass
    
def viewLog(func):
    """ 装饰函数，记录服务耗时 """
    def new_func( *args, **kwargs):
        begintime = time.time()
        #将异常抛出 
        ret = func(*args, **kwargs)
        try:
            costtime = (time.time() - begintime)*1000
            args = [str(args[0].__dict__.get('dbname', '')), costtime]
            logCallback(args)
        except:
            logging.error(traceback.format_exc())
        return ret
    return new_func
    
try:
    import logdecorationutil
except:
    pass
##################### 服务可视化日志输出 ##############################

#异常类
class ESOracle_Error(Exception):
    pass


# 初始化Oracle的环境变量
if not (os.environ.has_key("TNS_ADMIN") or os.environ.has_key("ORACLE_HOME")):
    os.environ["TNS_ADMIN"] = "/opt/oracle/instantclient/admin"
if not os.environ.has_key("NLS_DATE_FORMAT"):
    os.environ["NLS_DATE_FORMAT"] = "YYYY-MM-DD HH24:MI:SS"
if not os.environ.has_key("NLS_LANG"):
    os.environ["NLS_LANG"] = ".ZHS16GBK"


_VAR_TYPE_ = {
    '$STRING': cx_Oracle.STRING,
    '$NUMBER': cx_Oracle.NUMBER,
    '$CURSOR': cx_Oracle.CURSOR,
}

ROW_FACTORY_TYPE = 'default'


class ResultRow:
    ''' 记录行。可以当tuple或者dict使用，不区分大小写。
    '''

    def __init__(self, description, data):
        self.description = dict([(description[i[0]][0], i[0]) for i in enumerate(description)])
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.data[key]
        else:
            return self.data[self.description[key.upper()]]

    def __iter__(self):
        return iter(self.data)

    def keys(self):
        return self.description.keys()


class ResultSet:
    ''' 结果集。可以当tuple用，每个记录到了调用的时候才根据factory转化。
    '''

    def __init__(self, description, datas, factory):
        self.description = description
        self.datas = datas
        self.factory = factory
        self.index = 0
        self.length = len(datas)

    def __len__(self):
        return self.length

    def __getitem__(self, key):
        return self.factory(self.description, self.datas[key])

    def __iter__(self):
        return self

    def next(self):
        if self.index >= self.length:
            self.index = 0
            raise StopIteration
        self.index += 1
        return self.factory(self.description, self.datas[self.index - 1])


class DataCursor:
    """数据游标，对已经执行select的cursor进行封装
    """

    def __init__(self, cursor, factory):
        """构造函数
            cursor  oracle的游标
            factory rowFactory方法
        """
        self.cursor = cursor
        self.factory = factory

    def close(self):
        """关闭
        """
        self.cursor.close()

    def fetchmany(self, num):
        """获取数据，返回ResultSet对象
        """
        return ResultSet(self.cursor.description, self.cursor.fetchmany(num), self.factory)

#oracle 数据连接对象
class Connection:

    dbErr = cx_Oracle.DatabaseError

    def __init__ (self,conn,dbID):
        self.dbname = dbID
        self.conn = conn
        self.setRowFactory(ROW_FACTORY_TYPE)
        self.dbId = dbID


    def ping(self):
        try:
            self.conn.ping()
        except:
            return False
        return True

    def isAlive(self):
        '''检查是否存活'''
        try:
            self.conn.ping()
        except cx_Oracle.Error:
            return 0
        return 1

    def close(self):
        try:
            self.conn.close()
        except:
            pass

    def __del__(self):
        self.close()

    def setRowFactory(self, type):
        if type == 'upper':
            self._rowFactory = rowUpper
        elif type == 'lower':
            self._rowFactory = rowLower
        elif type == 'tuple':
            self._rowFactory = rowTuple
        else:
            self._rowFactory = ResultRow
        

    def list2arrayvar( self, LIST, TYPE='$STRING' ):
        cursor  = self.conn.cursor()
        return cursor.arrayvar(_VAR_TYPE_[ TYPE ], LIST)

    def setMaxRowSize(self, newMaxRowSize):
        self.maxRowSize = newMaxRowSize

    @viewLog
    def query(self, *args, **kwargs):
        cur = self.conn.cursor()
        try:
            cur.execute(*args, **kwargs)
            return self.fetchAll_(cur)
        finally:
            cur.close()

    # 功能：跳过前skip条数据，取出最多rows条数据
    # 说明：skip小于0表示不跳过，rows小于0表示取无限条
    @viewLog
    def query2(self, skip, rows, *args, **kwargs):
        cur = self.conn.cursor()
        try:
            cur.execute(*args, **kwargs)
            if skip > 0:
                cur.fetchmany(skip)
            return self.fetchMany_(cur, rows)
        finally:
            cur.close()

    # 功能：每页有pagelen行，取第pagenum页的数据
    # 说明：第一页pagenum则等于1，类推
    @viewLog
    def query3(self, pagenum, pagelen, *args, **kwargs):
        cur = self.conn.cursor()
        try:
            cur.execute(*args, **kwargs)
            if pagenum > 1 and pagelen > 0:
                skip = (pagenum-1)*pagelen
                cur.fetchmany(skip)
            return self.fetchMany_(cur, pagelen)
        finally:
            cur.close()

    @viewLog
    def queryx(self, *args, **kwargs):
        """查询，返回可以分批获取数据的DataCursor对象，一般在返回巨型记录集的时候使用。
            DataCursor在使用完以后最好主动调用一下close方法。
        """
        cur = self.conn.cursor()
        cur.execute(*args, **kwargs)
        return DataCursor(cur, self._rowFactory)

    @viewLog
    def execute(self, *args, **kwargs):
        cur = self.conn.cursor()
        try:
            cur.execute(*args, **kwargs)
            return cur.rowcount
        finally:
            cur.close()

    @viewLog
    def executemany(self, *args):
        cur = self.conn.cursor()
        try:
            return cur.executemany(*args)
        finally:
            cur.close()

    def commit(self):
        self.conn.commit()

    def rollback(self):
        self.conn.rollback()

    @viewLog
    def callproc(self, name, args=[], rows=0):
        cur = self.conn.cursor()
        argtypes = {}
        for i in range(len(args)):
            if args[i] in _VAR_TYPE_:
                argtypes[i] = args[i]
                args[i] = cur.var(_VAR_TYPE_[args[i]])
        try:
            ret = cur.callproc(name, args)
            for i in argtypes:
                args[i] = self.getVarValue_(args[i], argtypes[i], rows)
            return ret

        finally:
            cur.close()
            
    @viewLog
    def callproc2(self, name, skip, rows ,args=[]):
        cur = self.conn.cursor()
        argtypes = {}
        for i in range(len(args)):
            if args[i] in _VAR_TYPE_:
                argtypes[i] = args[i]
                args[i] = cur.var(_VAR_TYPE_[args[i]])
        try:
            ret = cur.callproc(name, args)
            for i in argtypes:
                args[i] = self.getVarValue2_(args[i], argtypes[i], skip, rows)
            return ret

        finally:
            cur.close()

    # 功能：每页有pagelen行，取第pagenum页的数据
    # 说明：第一页pagenum则等于1，类推
    @viewLog
    def callproc3(self, name, pagenum, pagelen, args=[]):
        cur = self.conn.cursor()
        argtypes = {}
        for i in range(len(args)):
            if args[i] in _VAR_TYPE_:
                argtypes[i] = args[i]
                args[i] = cur.var(_VAR_TYPE_[args[i]])
        try:
            ret = cur.callproc(name, args)
            for i in argtypes:
                args[i] = self.getVarValue3_(args[i], argtypes[i], pagenum, pagelen)
            return ret
        finally:
            cur.close()

    @viewLog
    def callfunc(self, name, retype, args=[], rows=0):
        cur = self.conn.cursor()
        argtypes = {}
        for i in range(len(args)):
            if args[i] in _VAR_TYPE_:
                argtypes[i] = args[i]
                args[i] = cur.var(_VAR_TYPE_[args[i]])
        try:
            ret = cur.callfunc(name, _VAR_TYPE_[retype], args)
            for i in argtypes:
                args[i] = self.getVarValue_(args[i], argtypes[i], rows)
            return ret
        finally:
            cur.close()

    def getCursor(self):
        return self.conn.cursor()

    def getVarValue_(self, v, t, rows=0):
        if t == '$STRING':
            return v.getvalue()
        elif t == '$CURSOR':
            cur = v.getvalue()
            rows = self.fetchMany_(cur, rows)
            cur.close()
            return rows
        else:
            return v

    def getVarValue2_(self, v, t, skip, row):
        if t == '$STRING':
            return v.getvalue()
        elif t == '$CURSOR':
            cur = v.getvalue()
            if skip >0:
                cur.fetchmany(skip)
                rows = self.fetchMany_(cur, row)
            else:
                rows = self.fetchMany_(cur, row)
            cur.close()
            return rows
        else:
            return v

    def getVarValue3_(self, v, t, pagenum, pagelen):
        if t == '$STRING':
            return v.getvalue()
        elif t == '$CURSOR':
            cur = v.getvalue()
            if pagenum > 1 and pagelen > 0:
                skip = (pagenum-1)*pagelen
                cur.fetchmany(skip)
            rows = self.fetchMany_(cur, pagelen)
            cur.close()
            return rows
        else:
            return v

    def fetchAll_(self, cur):
        return ResultSet(cur.description, cur.fetchall(), self._rowFactory)


    def fetchMany_(self, cur, rows):
        if rows > 0 :
            return ResultSet(cur.description, cur.fetchmany(rows), self._rowFactory)
        else:
            return ResultSet(cur.description, cur.fetchall(), self._rowFactory)


def rowUpper(description, data):
    # row factory: dict with upper key
    return dict([(i[1][0].upper(), data[i[0]]) for i in enumerate(description)])

def rowLower(description, data):
    # row factory: dict with lower key
    return dict([(i[1][0].lower(), data[i[0]]) for i in enumerate(description)])

def rowTuple(description, data):
    # row factory: tuple
    return data

import ConfigParser
import threading
from sqlalchemy.pool import QueuePool

_DB_LOCK_ = threading.RLock() # 锁
_DB_POOL_ = {} # 连接池

#默认配置 :/etc/esun/oracle.conf
CFG_PATH = '/etc/esun/oracle.conf'
CFG_PARSER = None
try:
    #自定义配置路径
    if os.getenv('ESUNORACLE_CONF'):
        CFG_PATH = os.getenv('ESUNORACLE_CONF')
    CFG_PARSER = ConfigParser.ConfigParser()
    CFG_PARSER.read(CFG_PATH)
except:
    raise ESOracle_Error('config file:%s is not exist!'%CFG_PATH)


#初始化一个池
#id 连接对象
#poolArgs 池配置参数
def initPool (id,**poolArgs):
    #poolArgs可选属性: 
    #  pool_size
    #  max_overflow
    #  timeout
    #  recycle
    #  echo
    #  use_threadlocal
    #  reset_on_return
    #  listeners
    global _DB_POOL_
    if _DB_LOCK_.acquire(1):
        try:
            #不允许多次初始化同一个池
            if _DB_POOL_.has_key(id):
                raise ESOracle_Error('_DB_POOL_: key %s is already exist!'%id)

            if not CFG_PARSER.has_section(id):
                raise ESOracle_Error('oracle node [%s] is not exist!'%id)

            conn_dict = { 'dsn':CFG_PARSER.get(id,'dsn'),
                          'user':CFG_PARSER.get(id,'user'), 
                          'password':CFG_PARSER.get(id,'password'), 
                          'threaded':CFG_PARSER.getboolean(id,'threaded')}
            if not poolArgs: #启动默认池设置
                pool_str = CFG_PARSER.get(id,'pooling')
                logging.info( 'esunoracle [db:%s] use default pool setup:[%s]'%(id,pool_str))
                arr = [ x.split(':') for x in pool_str.split(';')]
                poolArgs = dict([(i[0],eval(i[1])) for i in arr])
            _DB_POOL_[id] = QueuePool(lambda: cx_Oracle.connect(**conn_dict), **poolArgs)
        finally:
            _DB_LOCK_.release()

#根据ID连接库
def connect (id):
    global _DB_POOL_
    if _DB_LOCK_.acquire(1):
        try:
            #如果没有初始化 用默认池
            if not _DB_POOL_.has_key(id):
                initPool(id)

            pool = _DB_POOL_[id]
            for i in range(pool.size()+1):
                conn = pool.connect()
                try:
                    conn.ping()
                    break
                except cx_Oracle.Error:
                    conn.invalidate()    
            return Connection(conn,id)
        finally:
            _DB_LOCK_.release()
#是否存在某连接
def exist_db (id):
    return CFG_PARSER.has_section(id)

def dispose():
    '''释放连接池'''
    global _DB_POOL_
    if _DB_LOCK_.acquire(1):
        try:
            for id in _DB_POOL_:
                _DB_POOL_[id].dispose()
        finally:
            _DB_LOCK_.release()

import atexit
atexit.register(dispose)





if __name__== '__main__':
    poolArgs = {'pool_size':4,'timeout':16,'use_threadlocal':True}
    #initPool('jz',**poolArgs)
    db = connect('jz')
    
    rows = db.query('select * from t_jzproject_info where rownum<2')
    for row in rows:
        print row.data 

    
        
        
