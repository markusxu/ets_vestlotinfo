#coding=utf-8
"""
esunrabbit: rabbitmq 消息发送模块
author:kings
date:2015-11-16

(1) Config_Path 配置路径 默认 /etc/esun/rabbit.conf
    [rabbit1]
    conn = amqp://192.168.41.160:5672/ #可配置多个连接 用;隔开
    exchange_name = test_exchange
(2) 使用方法
    import esunrabbit
    rbt = esunrabbit.connect('rabbit1')
    rbt.send('test_topic','this is a test')
"""
import os
import sys
import time
import ConfigParser
import traceback
import threading
import random
import puka
import logging

##################### 服务可视化日志输出 ##############################  
def logCallback(args):
    """ 打印服务日志可视化的回调函数 """
    pass
    
def viewLog(func):
    """ 装饰函数，记录服务耗时 """
    def new_func( *args, **kwargs):
        begintime = time.time()
        #将异常抛出 
        ret = func(*args, **kwargs)
        try:
            costtime = (time.time() - begintime)*1000
            args = [str(args[1]), costtime]
            logCallback(args)
        except:
            logging.error(traceback.format_exc())
        return ret
    return new_func
    
try:
    import logdecorationutil
except:
    pass
##################### 服务可视化日志输出 ##############################

#异常类
class ESRbt_Error(Exception):
    pass


#默认配置 :/etc/esun/rabbit.conf
Config_Path = '/etc/esun/rabbit.conf'
Config_Parser = None
try:
    #自定义配置路径
    if os.getenv('ESUNRABBIT_CONF'):
        Config_Path = os.getenv('ESUNRABBIT_CONF')
    Config_Parser = ConfigParser.ConfigParser()
    Config_Parser.read(Config_Path)
except:
    raise ESCache_Error('config file:%s is not exist!'%Config_Path)


class Rabbit:

    def __init__ (self,url,exname):
        self.exname = exname
        self.url = url
        self.client = None
        

    def connect (self):
        srv_list = []
        for i in self.url.split(';'):
            if i:
                srv_list.append(i)
        while len(srv_list)>0:
            #如果多个连接 随机取一个
            idx = random.randint(0,len(srv_list)-1)
            server_url = srv_list[idx]
            self.client = puka.Client(server_url)
            promise = self.client.connect()
            res = self.client.wait(promise,3)
            if res and res.has_key('server_properties'): #success
                break
            else:
                self.client = None  #failed
                srv_list.remove(server_url)

        if not self.client:
            raise ESRbt_Error('rabbitmq connect server failed')

        promise_exchange = self.client.exchange_declare(exchange=self.exname,type='topic',durable=True,) 
        _exchange_res = self.client.wait(promise_exchange)

    @viewLog
    def send (self,topic, data, timeout=0.03):
        if SEND_LOCK.acquire(1):
            try:
                if not self.client:
                    self.connect()

                if timeout == 0:
                    timeout = None

                promise = self.client.basic_publish(exchange = self.exname,
                                           routing_key = topic,
                                           body = data)
                res = self.client.wait(promise, timeout)
                if res == {} :
                    return True
                else:
                    return False
            finally:
                SEND_LOCK.release()
                
    #获取队列消息积压数    
    def mqcount(self,queue_name,timeout=0.03):
        if SEND_LOCK.acquire(1):
            try:
                if not self.client:
                    self.connect()
                
                if timeout == 0:
                    timeout = None
                
                promise = self.client.queue_declare(                                    
                                    queue=queue_name,
                                    durable=True,
                                    arguments={'x-ha-policy':'all'}
                                  )        
                res = self.client.wait(promise, timeout)
                return res['message_count'] 
            finally:
                SEND_LOCK.release()      


#client实例
CLIENT_POOL = {} 
#实例锁
CLIENT_LOCK = threading.RLock()
#发送锁
SEND_LOCK = threading.RLock()

#初始化实例
def init_instance (name):
    global CLIENT_POOL
    if CLIENT_LOCK.acquire(1):
        try:
            if not CLIENT_POOL.has_key(name):
                conn = Config_Parser.get(name,'conn')
                exname = Config_Parser.get(name,'exchange_name')
                CLIENT_POOL[name] = Rabbit( conn, exname)
        except:
            raise ESRbt_Error('create client instance failed!')
        finally:
            CLIENT_LOCK.release()

#连接到消息对象
def connect (name):
    global CLIENT_POOL
    if CLIENT_LOCK.acquire(1):
        try:
            if not CLIENT_POOL.has_key(name):
                init_instance(name)
            instance = CLIENT_POOL[name]
            return instance
        finally:
            CLIENT_LOCK.release()

#直接发送
def send_topic (mqid , topic, data):
    return connect(mqid).send(topic,data)

#获取队列积压数
def get_messagecount(mqid,queue_name):
    lenth = connect(mqid).mqcount(queue_name)
    return lenth

if __name__=='__main__':
    # 1 直接发送
    send_topic('test','test_topic','hello world!')
    # 2 连续发送
    rbt = connect('test')
    arr = []
    for i in range(0,20):
        t = threading.Thread(target=rbt.send,args=('test_topic','this is a tst'))
        t.start()
        arr.append(t)

    for i in range(0,20):
        arr[i].join()
        
        


    
    