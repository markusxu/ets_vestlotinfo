#coding=utf-8
"""
esunacct账务接口客户端
author:kings
date:2015-11-05



(1)  配置默认路径 /etc/esun/ice.conf 
     配置节点名：acct
     格式：
     [acct]
     ice_file = /etc/esun/ice/ESunAcct.ice
     conn = ESunAcct:tcp -h 192.168.41.160 -p 60010
     Ice.MessageSizeMax = 20480
(2) 使用方法
    import esunacct
    req = {'username':'card100','coding':'gb2312','moneysources':'1'}
    resp = esunacct.getUserInfo(req)
   
"""
import os
import sys
import time
import logging
import ConfigParser
import traceback
import threading
import Ice



##################### 服务可视化日志输出 ##############################  
def logCallback(args):
    """ 打印服务日志可视化的回调函数 """
    pass

def viewLog(func):
    """ 装饰函数，记录服务耗时 """
    def new_func( *args, **kwargs):
        begintime = time.time()
        #将异常抛出 
        ret = func(*args, **kwargs)
        try:
            costtime = (time.time() - begintime)*1000
            #当出现异常一定会返回CODE并为负值
            result = 1
            if isinstance(ret, dict) and ret:
                result = ret.get('CODE', 1)
            elif isinstance(ret, list) and ret and isinstance(ret[0], dict):
                result = ret[0].get('CODE', 1)   
            args = [str(func.__name__), result, costtime]
            logCallback(args)
        except:
            logging.error(traceback.format_exc())
        return ret
    return new_func
    
try:
    import logdecorationutil
except:
    pass
##################### 服务可视化日志输出 ##############################


#异常类
class ESAcct_Error(Exception):
    pass




#配置文件路径 默认路径 /etc/esun/ice.conf
ICE_CONF = '/etc/esun/ice.conf'
#配置解析器
CONFIG_PARSER = None
#节点名字
SECTION_NAME = 'acct'
#Ice文件路径
ICE_FILE = '/etc/esun/ice/ESunAcct.ice'
try:
    #自定义配置路径
    if os.getenv('ESUNICE_CONF'):
        ICE_CONF = os.getenv('ESUNICE_CONF')
    CONFIG_PARSER = ConfigParser.ConfigParser()
    CONFIG_PARSER.read(ICE_CONF)
    if CONFIG_PARSER.has_option(SECTION_NAME,'ice_file'):
        ICE_FILE = CONFIG_PARSER.get(SECTION_NAME,'ice_file')
except:
    raise ESAcct_Error('config file:%s is not exist!'%ICE_CONF)

if not os.path.exists(ICE_FILE):
    raise ESAcct_Error('ice file:%s is not exist!'%ICE_FILE)


try:
    Ice.loadSlice(ICE_FILE)
    #导入模块
    import ESUN
except:
    raise ESAcct_Error('load module error!')


#acct实例
ACCT_INSTANCE = None
#锁
ACCT_LOCK = threading.Lock()

#初始化acct实例
def initInstance ():
    global ACCT_INSTANCE
    if ACCT_LOCK.acquire(1):
        try:
            if not ACCT_INSTANCE:
                conn = CONFIG_PARSER.get(SECTION_NAME,'conn')
                prop = Ice.createProperties()
                for k,v in CONFIG_PARSER.items(SECTION_NAME):
                    if 'Ice.' in k:
                        prop.setProperty(k,v)
                data = Ice.InitializationData()
                data.properties = prop
                #创建通讯器
                ic = Ice.initialize(data)
                #适配器
                adapter = ic.stringToProxy(conn)
                #acct实例
                ACCT_INSTANCE = ESUN.ESunAcctPrx.checkedCast(adapter)
        except:
            raise ESAcct_Error('create ACCT_INSTANCE failed')
        finally:
            ACCT_LOCK.release()

#调用接口
def _exe_cmd (command,*args):
    global ACCT_INSTANCE
    resp = {}
    try:
        if not ACCT_INSTANCE:
            initInstance ()
    except ESAcct_Error:
        resp["CODE"] = "-10000"
        resp["MSG"]  = "取账务服务连接失败"
        return resp

    try:
        if command == 'BatchDeposit2':
            func = getattr(ACCT_INSTANCE,'BatchDeposit','None')
            ret = func(*args)
            return ret
        else:
            func = getattr(ACCT_INSTANCE,command,'None')
            ret = func(*args)
            if isinstance(ret[1],dict):
                resp = ret[1]
                if resp.has_key('CODE'):
                    #有code要判断是否与ret一致
                    if int(ret[0])<0 and int(resp['CODE'])>0:
                        resp['CODE'] = ret[0]
                else:
                    #没有code的直接用ret[0]
                    resp['CODE'] = ret[0]
                return resp
            else:
                return ret[1]
    except:
        resp["CODE"] = "-10000"
        resp["MSG"]  =  'execute %s error!'%command
        resp["ERRTRACE"] = traceback.format_exc()
        return resp

#取用户信息
@viewLog
def getUserInfo(req):
    return _exe_cmd('getUserInfo',req)

#扣款
@viewLog
def Deduct(req):
    return _exe_cmd('Deduct',req)

#批量扣款
@viewLog
def BatchDeduct(reqlist):
    return _exe_cmd('BatchDeduct',reqlist)

#批量存款
@viewLog
def BatchDeposit(reqlist):
    return _exe_cmd('BatchDeposit',reqlist)

#批量存款 更多返回值
@viewLog
def BatchDeposit2(reqlist):
    return _exe_cmd('BatchDeposit2',reqlist)

#存款
@viewLog
def Deposit(req):
    return _exe_cmd('Deposit',req)


#取账务流水信息
@viewLog
def getUserTransLogInfoByID(req):
    return _exe_cmd('getUserTransLogInfoByID',req)


#取用户充值信息
@viewLog
def getUserChargeState(req):
    return _exe_cmd('getUserChargeState',req)

#冻结
@viewLog
def Freeze(req):
    return _exe_cmd('Freeze',req)

#冻结
@viewLog
def tryFreezeBJmoney(req):
    return _exe_cmd('tryFreezeBJmoney',req)

#解冻扣款
@viewLog
def RealDeduct(req):
    return _exe_cmd('RealDeduct',req)

#解冻
@viewLog
def unFreeze(req):
    return _exe_cmd('unFreeze',req)

# 查usertranslog表
@viewLog
def getUserTransLogInfo(req):
    return _exe_cmd('getUserTransLogInfo',req)

# 查usertranslog表
@viewLog
def getUserTransLogInfo2(req):
    return _exe_cmd('getUserTransLogInfo2',req)

# 查usertmppay表
@viewLog
def getUserTmpPayInfo(req):
    return _exe_cmd('getUserTmpPayInfo',req)

# 兑换红包
@viewLog
def assignHB(req):
    return _exe_cmd('assignHB',req)

# 赠送红包
@viewLog
def gifthb(req):
    return _exe_cmd('gifthb',req)

# 取用户可用红包
@viewLog
def getUsefullAccount(req):
    return _exe_cmd('getUsefullAccount',req)

# 加钱
@viewLog
def Recharge(req):
    return _exe_cmd('Recharge',req)

#提款
@viewLog
def userDrawMoney(req):
    return _exe_cmd('userDrawMoney',req)

# 提款成功扣款
@viewLog
def Withdrawn(req):
    return _exe_cmd('Withdrawn',req)

# 转账
@viewLog
def transfer(reqlist):
    return _exe_cmd('transfer',reqlist)
   
# 提款失败撤单返款
@viewLog
def CancelWithdrawn (req):
    return _exe_cmd('CancelWithdrawn',req)
    
def ping():
    data ={'from':'python','resource':get_ip_address()}
    return _exe_cmd('ping', data)


#获取IP地址
def get_ip_address(ifname='eth0'):
    try:
        import socket
        import fcntl
        import struct
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
    except:
        ifname = "bond0"
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])




if __name__ == '__main__':
    #获取用户信息
    
    req = {'username':'card100','coding':'gb2312','moneysources':'1'}
    resp = getUserInfo(req)
    print resp
    print 'CODE=%s,MSG=%s'%(resp['CODE'],resp['MSG']) 

    # 扣款
    req = {'username':'card100','coding':'gb2312','moneysources':'1'}
    resp = Deduct(req)
    print isinstance(resp,dict)
    print 'CODE=%s,MSG=%s'%(resp['CODE'],resp['MSG'])

        
