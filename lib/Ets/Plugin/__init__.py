#coding=utf-8

# 【插件开发模块】
# 
#【已注册hook】#：
###############################################################################
#         插件名称        ||                        描述                      #
#=============================================================================#
# run_start               || server.py中_run函数的开始处                      #
# ice_interface_init      || server.py中EasI.__init__方法的结束处             #
# server_init_over        || server.py中Server.__init__方法的开始处           #
# run_stop                || server.py中_run函数的结束处                      #
###############################################################################

#【说明】#
# 开启插件需要在项目的eas_interface.xml配置文件中配置，具体可看demo
# 开发插件，需要在提供的插件中申明setup方法，在内注册相应方法，可参考Alert

#【联系】#
## tuyl@500wan.com ##

import logging
import traceback

_ACTIONS_ = {}

def _get_unique_id(tag, func, priority):
    return '%s_%s_%s' % (tag, func.__hash__(), priority)
    

def add_action(tag, func, priority=10):
    '''添加插件'''
    global _ACTIONS_
    idx = _get_unique_id(tag, func, priority)
    if not _ACTIONS_.has_key(tag):
        _ACTIONS_[tag] = {}
    if not _ACTIONS_[tag].has_key(priority):
        _ACTIONS_[tag][priority] = {}
    
    _ACTIONS_[tag][priority][idx] = {'function' : func}
    

def do_action(tag, *args, **kwargs):
    '''注册钩子'''
    actions = _ACTIONS_.get(tag, None)
    if not actions:
        return None
    
    prikey = actions.keys()
    prikey.sort() #按优先级排序
    for k in prikey:
        acs = actions[k]
        for ac in acs:
            a = acs[ac]['function']
            try:
                a(*args, **kwargs)
            except:
                logging.error('some error in %s-do_action:%s', tag, traceback.format_exc())


def hook_exist(*tags):
    '''检查钩子是否有注册'''
    etags = set(tags) & set(_ACTIONS_.keys())
    return etags




