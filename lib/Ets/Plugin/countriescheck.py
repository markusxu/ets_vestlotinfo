# coding=utf-8

import os
import sys
import logging
import traceback

import Common
import Ets.Plugin
import JsonUtil

################################
sys.path.append(os.environ['_BASIC_PATH_'] + '/libs')

import geoip
import retcode
import functions
from baseconfig import Environment
import env

nonallowed_countries = ['CN', 'HK', 'MO', 'US', 'FR', 'PL', 'ES', 'LV', 'PT', 'IT', 'DK']

def _deal_countriescheck(*args, **kwargs):
    '''if user's country is in nonallowed_countries'''
    global nonallowed_countries
    try:
        req        = kwargs.get('req', {})
        language   = kwargs.get('language','en').upper()
        languageid = env.Language.get_languageid(language)

        eret = {
            'CODE': str(retcode.NOT_ALLOWED_COUTRIES),
            'MSG' : functions.t('NOT_ALLOWED_COUTRIES',[Environment.SERVICE_PAGECODE],0,languageid),
            'info': {},
            'list': []
        }

        # check user's real ip
        resp        =   kwargs.get('resp')
        remote_addr =   req.get('remote_addr', '')

        # can't get the user's ip
        if not remote_addr:
            resp['direct_ret'] = 1
            resp['ret'] = eret
            return 
        # get the iso code by ip
        iso_code = geoip.find_country_iso_code(remote_addr)
        if not iso_code or iso_code in nonallowed_countries:
            resp['direct_ret'] = 1
            resp['ret'] = eret
            return

    except:
        logging.error(traceback.format_exc())

################################


def setup(appid, *args, **kwargs):
    '''插件启动'''
    logging.info('start countriescheck deal')
    Ets.Plugin.add_action('invoke_begin', _deal_countriescheck)
