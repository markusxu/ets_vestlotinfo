#coding=utf-8
'''MonitorService的Basic模块'''
import sys
import os
import time
import logging
import traceback
import exceptions
import __main__

import Eas.Plugin
from Eas import Plugin
import JsonUtil
import WatchDog
import thread
import Common

def IceMsg2IP( current ):
    if not current:
        return ''
    
    str_ip = current.con.toString()
    str_find = "remote address = "
    position = str_ip.find( str_find ) + len( str_find )

    return str_ip[position:]


def original_eas_invoke( _main, _self ):

    class original_EasI( _main.EasI ):

        def _invoke(self, method, req):
            '''处理请求方法'''
            result = method(**req)
            code        =  result.get('CODE', 0)
            resp        =  result.get('info', {})
            resp['CODE']=  str(code)
            resp['MSG'] =  result.get('MSG', '')
            data        = result.get('list', [])
            return (int(code), resp, data)
        
        def do_invoke(self, cmdId, req, res, current=None):
            '''处理主程序'''
            watchman = WatchDog.autoFeeder( thread.get_ident(), cmdId )
            startime = time.time()
            argv = {'cmdId':cmdId, 'req':req, 'res':res}
            IP = IceMsg2IP( current )
            IP = IP.split(':')[0]
            if not self._methods.has_key(cmdId): #方法不存在
                cmdId = cmdId.lower()
            Common.threadSpace().set('data', res)
            try: #相信用户传参规范
                if not self._methods.has_key(cmdId): #方法不存在
                    err = self._error('invoke', 'Method('+cmdId+' can not found!)')
                    logging.info('[ID:%s][return]:%s', cmdId, JsonUtil.write(err))
                    return err
                result, req = self.auto_match_args(cmdId,req)
                if result == False:
                    return self._error('invoke', 'Method('+cmdId+') parameter not enough:%s!'%req.keys())
                ret = self._invoke(self._methods[cmdId]['function'], req)
                Plugin.do_action('invoke_end2', cmdId,
                    req=req,
                    res=res,
                    current=current,
                    resp=ret,
                )

                costime = time.time() - startime
                if not ret[1].has_key("__type") or ret[1]["__type"] != "json":
                    logging.info(' %s 耗时 %s 毫秒 [IP:%s] [ID:%s] 入参[%s] 附属数据[%s] 出参[CODE:%s][INFO:%s][LEN_OF_LIST:%s]',
                        cmdId, float(int(costime*1000000))/1000, IP, cmdId, JsonUtil.write(req), JsonUtil.write(res),
                        ret[0], JsonUtil.write(ret[1]), len(ret[2])
                    )
                else:
                    logging.info(' %s 耗时 %s 毫秒 [IP:%s] [ID:%s] 入参[%s] 附属数据[%s] 出参[CODE:%s][INFO:%s][LEN_OF_LIST:%s]',
                        cmdId, float(int(costime*1000000))/1000, IP, cmdId, JsonUtil.write(req), JsonUtil.write(res),
                        ret[0], JsonUtil.write({"MSG":ret[1].get("MSG", ""), "__type":"json", "__data":"HIDEN"}), len(ret[2])
                    )
                return ret
            except:
                logging.error('invoke error:%s', traceback.format_exc())
                return self._error('invoke', 'Server method('+cmdId+') error')

                
    _self.obj = original_EasI()

def setup(appid, *args, **kwargs):
    '''兼容旧版eas插件启动脚本'''

    Eas.Plugin.add_action('server_init_over', original_eas_invoke)
