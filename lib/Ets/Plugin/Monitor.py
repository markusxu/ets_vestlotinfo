#coding=utf-8

import os
import sys
import socket
import getpass
import logging
import traceback
import __main__

import Common
import MiniXml
import Ets.Plugin

from MonitorClient.Basic import Client as McClient

_MBC_ = None        #Monitor客户端
_REGINFO_ = ''      #服务信息
_CHECK_TIME = 60    #心跳检测间隔时间s
#获取框架名
_PRO_ID_ = sys.argv[0]
_PRO_ID_ = _PRO_ID_[:_PRO_ID_.find('.')]
_PRO_ID_ = _PRO_ID_[_PRO_ID_.rfind('/')+1:]
################################

#发送心跳
def inform(data=None):
    global _MBC_
    global _REGINFO_

    if not data:
        data = _REGINFO_
    _MBC_._inform(data)

#启动自动发送心跳线程
def start_loop(data=None):
    global _MBC_
    global _CHECK_TIME
    global _REGINFO_

    if not data:
        data = _REGINFO_
    _MBC_.start_loop(data, _CHECK_TIME)

#停止自动发送心跳线程
def stop_loop():
    global _MBC_
    _MBC_.stop_loop()

def _register(*args, **kwargs):
    try:
        main = args[0]
        app_id = main._APP_ID_
        logging.info('_register')

        ins = MiniXml.parseFile(os.environ['_BASIC_PATH_'] + '/etc/%s.xml'%(_PRO_ID_))
        conf = {}
        if _PRO_ID_.lower() in ['cos']:
            conf = ins.get('/%s/%s' % (_PRO_ID_, app_id))
        elif _PRO_ID_.lower() in ['rms']:
            conf = ins.list('/%s/applications/%s/' % (_PRO_ID_, app_id))
        else: #['eas','ets','ems']
            conf = ins.get('/%s/applications/%s' % (_PRO_ID_, app_id))

        commname = conf.get('commname','')
        server_info = conf.get('content','FIND JIAOYI')
        workteam = ins.get('/%s/monitoring/team'%_PRO_ID_,{})
        tname = workteam.get('tname','')
        mbc_addr = ins.get('/%s/monitoring/server'%_PRO_ID_)

        mbc = McClient('%s.'%_PRO_ID_  + app_id, (mbc_addr['host'], int(mbc_addr['port'])))
        __main__.alert = mbc.alert # 把空的告警函数改为使用监控的

        #注册服务接口
        INTERFACES = __main__.INTERFACES
        interfacelist = []
        logging.info(INTERFACES)
        for i in range(0,len(INTERFACES)):
            for key in INTERFACES[i]:    
                interface = INTERFACES[i][key].get('function')
                interfacename = str(interface).split(' ')[1]
                if interfacename == "method":
                    interfacename = str(interface).split(' ')[2]  
                interfaceargs = INTERFACES[i][key].get('funcinfo')
                interfaceargs = interfaceargs.args
                interfacedict = {"args":interfaceargs,"interface":interfacename}   
                interfacelist.append(interfacedict)                
        logging.info("^^^^^^^^^^^^^^^^^^^^%s" %(interfacelist))         

        global _REGINFO_
        _REGINFO_ = {
            'host_name'             : socket.gethostname(),                                  #机器名
            "service_name"          : app_id,                                                #服务名
            "server_type"           : _PRO_ID_,                                              #服务类型
            "server_path"           : os.environ['_BASIC_PATH_'],                            #服务路径
            "start_script"          : os.environ['_BASIC_PATH_'] + '/bin/' + _PRO_ID_ +'.sh',#服务启动脚本路径
            "server_info"           : server_info,                                           #描述信息
            "server_pid"            : os.getpid(),                                           #服务进程号
            "host_ip"               : Common.get_ip_address(),                               #ip地址
            "server_port"           : conf.get('listen', ''),                                #端口号
            "server_team_name"      : 'team_'+tname,                                         #所属工作团队
            "server_comm_name"      : commname,                                              #服务大类名称
            "server_owner_user"     : getpass.getuser(),                                     #所属用户
            "server_log"            : os.environ['_BASIC_PATH_'] + '/var/log/' + _PRO_ID_ + '.' + app_id + '.log', #服务日志
            "interfacename"         : interfacelist    
        }

        logging.info(_REGINFO_)
        global _MBC_
        _MBC_ = mbc

        mbc.register(_REGINFO_)
        start_loop()
    except:
        logging.error(traceback.format_exc())

#注销app 
def _unregister(*args, **kwargs ):
    global _MBC_
    global _REGINFO_

    try:
        logging.info(_REGINFO_)
        stop_loop()
        _MBC_.unregister(_REGINFO_)
    except:
        logging.error(traceback.format_exc())

################################
def setup(appid, *args, **kwargs):
    '''插件启动'''
    logging.info('start Monitor')
    Ets.Plugin.add_action('ets_start', _register)
    Ets.Plugin.add_action('ets_end', _unregister)
