# coding=utf-8

import os
import sys
import logging
import traceback

import Common
import JsonUtil
import esuncache
import Ets.Plugin

if sys.path.count(os.environ['_BASIC_PATH_'] + '/libs') == 0:
    sys.path.insert(0, os.environ['_BASIC_PATH_'] + '/libs')

from baseconfig import Environment
################################

def _deal_usercheck(*args, **kwargs):
    ''' update user's ck expire time '''
    try:
        logging.info('_deal_usercheck')
        params = kwargs.get('req')
        cmdId  = args

        usercheck = params.get('usercheck')
        if usercheck and cmdId != 'login':
            con   = esuncache.connect(Environment.REDIS_ML_DB_CONFIG)
            rval  = con.get(usercheck)
            if rval:
                uinfo = JsonUtil.read(rval)
                userid   = uinfo.get('userid')
                # update the usercheck's expire time 
                con.expire(usercheck, Environment.CK_VALID_TIME)
                con.expire('multilotto|usercheck|%s'%str(userid), Environment.CK_VALID_TIME)
                logging.info('_deal_usercheck done')
        else:
            logging.info('_deal_usercheck no usercheck, pass')

    except:
        logging.error(traceback.format_exc())

################################


def setup(appid, *args, **kwargs):
    ''' Plugin start '''
    logging.info('start usercheck deal')
    Ets.Plugin.add_action('invoke_end', _deal_usercheck)
